unit fcalc;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls;

type
  tform_calc = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    Memo1: TMemo;
    procedure edit1_returnpressed(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var  form_calc: tform_calc;

implementation

uses iglobvar,iutil,isyntax,ieval{,Registry,Windows};

{$R *.lfm}

procedure tform_calc.FormCreate(Sender: TObject);
begin
  Left   := 610;
  Top    := 205;
  Height := 396;
  Width  := 310;
  adjust(self);
  memo1.Clear;
end;

procedure tform_calc.edit1_returnpressed(Sender: TObject);
label 1;
var s : string;
    v,tv : integer;
    val : extended;
begin
  s := propre(minuscule(edit1.Text));
  if ( s = '' ) then exit;
  lirexp(s,v,tv);
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  val := eval(v,tv);
  if err_eval then
    begin
      err_eval := false;
      exit;
    end;
  memo1.Lines.Append(s);
  memo1.Lines.Append('-> ' + FloatToStr(val));
  edit1.Clear;
end;

{function  cputype: string;
var reg: TRegistry;
begin
  cputype := '';
  reg := TRegistry.Create;
  try
    reg.RootKey := HKEY_LOCAL_MACHINE;
    if reg.OpenKey('\Hardware\Description\System\CentralProcessor\0', False) then
      cputype := reg.ReadString('Identifier');
  finally
    reg.Free;
  end;
end; }

procedure tform_calc.FormActivate(Sender: TObject);
begin
  {status(cputype); }
end;

procedure tform_calc.FormShow(Sender: TObject);
begin
  edit1.Clear;
  memo1.Clear;
end;

end.
