unit fviewall;

{$MODE Delphi}

interface

uses {SysUtils,Types,Classes,Variants,}Graphics,Controls,Forms,Dialogs,
     ComCtrls,Menus,ExtCtrls,StdCtrls;

type
  tform_view_all = class(TForm)
    TreeView1: TTreeView;
    StatusBar1: TStatusBar;
    Memo1: TMemo;
    procedure tree_group(g : integer; tnode : TTreeNode);
    procedure tree;
    procedure display;
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_viewall;
    procedure StatusBar1PanelClick(Sender: TObject; Panel: TStatusPanel);
  private
    plus_date : boolean;
    plus_nb   : boolean;
    plus_val  : boolean;
    procedure status_ordre;
  public
  end;

var  form_view_all: tform_view_all;

implementation

uses iglobvar,iutil,isymb,isyntax;

{$R *.lfm}

function carboo(boo : boolean) : char;
begin
  if boo then carboo := '+' else carboo := '-';
end;

procedure tform_view_all.status_ordre;
begin
  statusbar1.Panels[1].Text := 'Date '   + carboo(plus_date);
  statusbar1.Panels[2].Text := 'Number ' + carboo(plus_nb);
  statusbar1.Panels[3].Text := 'Value '  + carboo(plus_val);
end;

procedure tform_view_all.tree_group(g : integer; tnode : TTreeNode);
var tn : TTreeNode;
    i : integer;
begin
  with group[g],treeview1.Items do
    begin
      AddChild(tnode,'Group relations');
      tn := tnode.GetLastChild;
      for i := 1 to size do
        AddChild(tn,s_ecri_rel(xrel[i]));
      AddChild(tnode,'Other group relations');
      tn := tnode.GetLastChild;
      for i := 1 to sizei do
        AddChild(tn,s_ecri_rel(xreli[i]));
      AddChild(tnode,'Mutations');
      tn := tnode.GetLastChild;
      for i := 1 to sizemut do
        AddChild(tn,s_ecri_mut(xvarmut[i]));
      AddChild(tnode,'Group variables');
      tn := tnode.GetLastChild;
      for i := 1 to size do with rel[xrel[i]] do
        AddChild(tn,s_ecri_var(xvar));
      for i := 1 to sizei do with rel[xreli[i]] do
        AddChild(tn,s_ecri_var(xvar));
      for i := 1 to sizeloc do
        AddChild(tn,s_ecri_var(xvarloc[i]));
      AddChild(tnode,'Phenotypes');
      AddChild(tnode,'Phylogeny');
    end;
end;

procedure tform_view_all.tree;
var treenode,tn : TTreeNode;
    z,x,i : integer;
begin
  with treeview1.Items do
    begin
      treenode := nil;
      for z := 1 to modele_nb do with modele[z] do
        begin
          treenode := Add(treenode,s_ecri_model(z));
          AddChild(treenode,'Relations');
          tn := treenode.GetLastChild;
          for i := 1 to size do
            AddChild(tn,s_ecri_rel(xrel[i]));
          AddChild(treenode,'Other relations');
          tn := treenode.GetLastChild;
          for i := 1 to sizei do
            AddChild(tn,s_ecri_rel(xreli[i]));
          for x := 1 to group_nb do
            begin
              AddChild(treenode,s_ecri_group(x));
              tn := treenode.GetLastChild;
              tree_group(x,tn);
              tn.Expanded := true;
            end;
        end;
      if ( fun_nb - fun_nb_predef > 0 ) then
        begin
          treenode := Add(treenode,'Functions');
          for x := fun_nb_predef+1 to fun_nb do
            AddChild(treenode,s_ecri_fun(x));
        end;
      treenode := Add(treenode,'Variables');
      for x := 1 to variable_nb do with variable[x] do
        if ( xgroup = 0 ) then AddChild(treenode,s_ecri_var(x));
    end;
end;

procedure tform_view_all.display;
var x,tx,z,tz : integer;
    s : string;
begin
  with treeview1 do
    if Visible and Assigned(Selected) then
      begin
        s := Selected.Text;
        if ( s = 'Variables' ) then
          b_ecri_list_variable
        else
        if ( s = 'Relations' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_rel_model(z);
          end
        else
        if ( s = 'Functions' ) then
          b_ecri_list_fun
        else
        if ( s = 'Other relations') then
          b_ecri_list_reli_model(z)
        else
        if ( s = 'Group relations' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_rel_group(z);
          end
        else
        if ( s = 'Other group relations' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_reli_group(z);
          end
        else
        if ( s = 'Mutations') then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_mut_group(z);
          end
        else
        if ( s = 'Group variables' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_variable_group(z);
          end
        else
        if ( s = 'Phenotypes' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_pheno_group(z);
          end
        else
        if ( s = 'Phylogeny' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_philogeny_group(z);
          end
        else
          begin
            trouve_obj(s,x,tx);
            b_ecri(x,tx);
            {status(s_ecri2(x,tx)); }
          end;
        memo1.Clear;
        memo1.Lines := lines_syntax;
      end;
end;

procedure tform_view_all.TreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  display;
end;

procedure tform_view_all.FormCreate(Sender: TObject);
begin
  Left   := 298;
  Top    := 28;
  Height := 532;
  Width  := 706;
  adjust(self);
  memo1.ReadOnly := true;
  memo1.Clear;
  type_ordre := -ordre_date;
  plus_date := false;
  plus_nb   := true;
  plus_val  := true;
end;

procedure tform_view_all.FormActivate(Sender: TObject);
begin
  display;
  if ( group_nb > 0 ) then status_ordre;
end;

procedure tform_view_all.init_viewall;
begin
  treeview1.Items.Clear;
  tree; {voir a reconstruire l'arbre si changevar, newvar }{iiiii}
  with treeview1 do Selected := Items[1];
  display;
end;

procedure tform_view_all.StatusBar1PanelClick(Sender: TObject;
          Panel: TStatusPanel);
begin
  if ( group_nb = 0 ) then exit;
  case panel.index of
  0 :;
  1 : begin
        plus_date := not plus_date;
        type_ordre := ordre_date;
        if not plus_date then type_ordre := -type_ordre;
      end;
  2 : begin
        plus_nb := not plus_nb;
        type_ordre := ordre_nb;
        if not plus_nb then type_ordre := -type_ordre;
      end;
  3 : begin
        plus_val := not plus_val;
        type_ordre := ordre_val;
        if not plus_val then type_ordre := -type_ordre;
      end;
  else
  end;
  status_ordre;
  display;
end;

end.
