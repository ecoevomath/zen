unit isyntax;

{$MODE Delphi}

{  @@@@@@   analyse des formules   @@@@@@  }

interface

uses iglobvar;

function  lowcase(car : char) : char;
function  minuscule(s : string) : string;
function  position(s : string; c : char) : integer;
function  sous_chaine(s : string;i1,long : integer) : string;
function  propre(s : string) : string;
function  tronque(s : string) : string;
procedure coupe(s : string;pos : integer;var s1,s2 : string);
procedure separe(s : string; sep : char;var ns : integer;var tabs : svec_type);

function  est_reserve(s : string) : boolean;
function  est_nom(s : string) : boolean;
function  est_entier(s : string;var n : integer) : boolean;
function  est_reel(s : string;var a : extended) : boolean;
function  lir_variable(s : string) : integer;
procedure lirexp(s : string;var x,tx : integer);

function  s_ecri_dic(x : integer) : string;
function  s_ecri_var(x : integer) : string;
function  s_ecri_val(a : extended) : string;
procedure xwrite9(a : extended);
function  s_ecri_val_variable(x : integer) : string;
function  s_ecri_variable(x : integer) : string;
procedure b_ecri_variable(x : integer);
procedure b_ecri_list_variable;
procedure b_ecri_list_variable_group(z : integer);
function  s_ecri_fun(x : integer) : string;
function  s_ecri_fonction(x : integer) : string;
procedure b_ecri_list_fun;
function  s_ecri_rel(x : integer) : string;
function  s_ecri_relation(x : integer) : string;
procedure b_ecri_relation(x : integer);
procedure b_ecri_list_reli_model(x : integer);
procedure b_ecri_list_rel_model(x : integer);
procedure b_ecri_list_rel_group(x : integer);
procedure b_ecri_list_reli_group(x : integer);
procedure b_ecri_list_rel_indep;
function  s_ecri_model(x : integer) : string;
function  s_ecri_modele(x : integer) : string;
procedure b_ecri_modele(x : integer);
procedure b_ecri_list_modele;

procedure b_ecri_list_group;
procedure b_ecri_groupe(x : integer);
procedure b_ecri_list_pheno_group(x : integer);
procedure b_ecri_philogeny_group(x : integer);
function  s_ecri_group(x : integer) : string;
function  s_ecri_groupe(x : integer) : string;
function  s_ecri_mut(x : integer) : string;
procedure b_ecri_mutation(x : integer);
procedure b_ecri_list_mut_group(x : integer);
function  s_ecri_pheno(x : integer) : string;
procedure b_ecri_pheno(x : integer);

function  s_ecri(x,tx : integer) : string;
function  s_ecri2(x,tx : integer) : string;
procedure b_ecri(x,tx : integer);

procedure s_ecri_lis2(x : integer);

const ordre_num  = 0;
      ordre_date = 1;
      ordre_nb   = 2;
      ordre_val  = 3;

var   fun_compil : integer;
      crea_var   : boolean;
      crea_fun   : boolean;
      crea_group : boolean;

      {lines_syntax : TStrings;
      lines_separe : TStrings;}

      lines_separe : svec_type;
      err_syntax : boolean;

      type_ordre : integer;

implementation

uses  SysUtils,
      isymb;

var   group_expected : boolean;
      aaa : array[1..lis2_nb_max] of integer;

procedure erreur_syntaxe(s : string);
begin
  writeln('Syntax - ',s);
  err_syntax := true;
end;

function  lowcase(car : char) : char;
var icar : integer;
begin
  icar := ord(car);
  if ( ( icar <= ord('Z') ) and ( icar >= ord('A') ) ) then 
    begin
      icar := icar + ord('a') - ord('A');
      car  := chr(icar);
    end;
  lowcase := car;
end;

function  minuscule(s : string) : string;
var i : integer;
    t : string;
begin
  t := '';
  for i := 1 to length(s) do t := t + lowcase(s[i]);
  minuscule := t;
end;
 
function  position(s : string; c : char) : integer;
begin
  position := pos(c,s);
end;

function  sous_chaine(s : string;i1,long : integer) : string;
begin
  sous_chaine := copy(s,i1,long);
end; 

function  propre(s : string) : string;
var i : integer;
    t : string;
begin
  t := '';
  for i := 1 to length(s) do
    if ( s[i] <> ' ' ) then  t := t + s[i];
  propre := t;
end; 

function  tronque(s : string) : string;
var i,pos1,pos2,ls : integer;
    t : string;
begin
  ls  := length(s);
  if ( ls = 0 ) then
    begin
      tronque := '';
      exit;
    end;
  pos1 := ls;
  for i := 1 to ls do
    if ( s[i] <> ' ' ) then 
      begin
        pos1 := i;
        break;
      end;
   pos2 := 0;
   for i := ls downto 1 do
    if ( s[i] <> ' ' ) then 
      begin
        pos2 := i;
        break;
      end;
   t := '';
   for i := pos1 to pos2 do t := t + s[i];
   tronque := t;
end; 

procedure coupe(s : string;pos : integer;var s1,s2 : string);
var ls : integer;
begin
  ls := length(s);
  s1 := sous_chaine(s,1,pos-1);
  s2 := sous_chaine(s,pos+1,ls-pos);
end;

procedure separe(s : string; sep : char;var ns : integer;var tabs : svec_type);
var pos : integer;
    s1  : string;
begin
  ns := 0;
  s := tronque(s);
  repeat
    pos := position(s,sep);
    if ( pos = 0 ) then
      begin
        ns := ns + 1;
        if ( ns > svec_max ) then
          begin
            erreur_syntaxe(s);
            exit;
          end;
        tabs[ns] := tronque(s);
        exit
      end;
    coupe(s,pos,s1,s);
    ns := ns + 1;
    if ( ns > svec_max ) then
      begin
        erreur_syntaxe(s);
        exit;
      end;
    tabs[ns] := tronque(s1);
  until false;
end;

function  est_chiffre(car : char;var val : integer) : boolean;
begin
  val := ord(car) - ord('0');
  est_chiffre := ( val >= 0 ) and ( val <= 9 );
end;  

function  est_lettre(car : char) : boolean;
begin
  est_lettre := car in ['a'..'z'];
end;  

function  est_nom(s : string) : boolean;
var c : char;
    i : integer;
    bool : boolean;
begin
  if ( s = '' ) then
    begin
      est_nom := false;
      exit;
    end;
  bool := est_lettre(s[1]);
  for i := 2 to length(s) do
    begin
      c := s[i];
      bool := bool and 
       ( est_lettre(c) or
       ( c in ['_','$','!','#','%','&'] ) or
       ( c in ['0'..'9'] ) );
    end;
  est_nom := bool;
end;

function  est_entier(s : string;var n : integer) : boolean;
var i,j,l : integer;
begin
  est_entier := false;
  l := length(s);
  if ( l = 0 ) then exit;
  n := 0;
  for i := 1 to l do
    begin
      if not est_chiffre(s[i],j) then exit;      n := 10*n + j;
    end;
  est_entier := true;
end;

function  est_nombre(s : string;var r : extended) : boolean;
var i,j,l : integer;
begin
  est_nombre := false;
  l := length(s);
  if ( l = 0 ) then exit;  r := 0;
  for i := 1 to l do
    begin
      if not est_chiffre(s[i],j) then exit;      r := 10.0*r + j;
    end;
  est_nombre := true;
end;

function  est_reel0(s : string;var a : extended) : boolean;
var pos,i : integer;
    s1,s2 : string;
    a1,a2 : extended;
begin
  est_reel0 := false;
  if ( length(s) = 0 ) then exit;  pos := position(s,'.');
  if ( pos = 0 ) then
    begin
      if not est_nombre(s,a1) then exit
      else
        begin
          a := a1;
          est_reel0 := true;
          exit;
        end;
    end;
  coupe(s,pos,s1,s2);
  if ( s1 <> '' ) then
    if not est_nombre(s1,a1) then exit
    else
  else
    a1 := 0.0;
  if ( s2 <> '' ) then
    if not est_nombre(s2,a2) then
      exit
    else
      for i := 1 to length(s2) do a2 := a2/10.0
  else
    a2 := 0.0;
  a := a1 + a2;
  est_reel0 := true;
end;

function  est_reel(s : string;var a : extended) : boolean;
var ls : integer;
begin
  est_reel := false;
  ls := length(s);
  if ( ls = 0 ) then exit;
  if ( s[1] = '-' ) then
    begin
      s := sous_chaine(s,2,ls-1);
      est_reel := est_reel0(s,a);
      a := -a;
    end 
  else
    est_reel := est_reel0(s,a);
end;

function  est_op2(c : char;var op2 : integer) : boolean;
begin
  est_op2 := true;
  case c of
    '+'  : op2  := op2_plus;
    '-'  : op2  := op2_moins;
    '*'  : op2  := op2_mult;
    '/'  : op2  := op2_div;
    '^'  : op2  := op2_puis;
    '<'  : op2  := op2_infe;
    '>'  : op2  := op2_supe;
    '\'  : op2  := op2_mod;
    '@'  : op2  := op2_convol;
    else est_op2 := false;
  end;
end;   

function  prio_op2(op2 : integer) : integer;
begin
  case op2 of
    op2_plus   : prio_op2 := 4;
    op2_moins  : prio_op2 := 4;
    op2_mult   : prio_op2 := 2;
    op2_div    : prio_op2 := 2;
    op2_puis   : prio_op2 := 1;
    op2_infe   : prio_op2 := 3;
    op2_supe   : prio_op2 := 3;
    op2_mod    : prio_op2 := 2;
    op2_convol : prio_op2 := 3;
    else;
  end;
end;   

function  est_op1(var s : string;var op1 : integer) : boolean;
begin
  est_op1 := true;
  if ( s = '-' )     then op1 := op1_moins else
  if ( s = 'cos' )   then op1 := op1_cos   else
  if ( s = 'sin' )   then op1 := op1_sin   else
  if ( s = 'tan' )   then op1 := op1_tan   else
  if ( s = 'acos' )  then op1 := op1_acos  else
  if ( s = 'asin' )  then op1 := op1_asin  else
  if ( s = 'atan' )  then op1 := op1_atan  else
  if ( s = 'ln' )    then op1 := op1_ln    else
  if ( s = 'log' )   then op1 := op1_log   else
  if ( s = 'exp' )   then op1 := op1_exp   else
  if ( s = 'fact' )  then op1 := op1_fact  else
  if ( s = 'sqrt' )  then op1 := op1_sqrt  else
  if ( s = 'abs' )   then op1 := op1_abs   else
  if ( s = 'trunc' ) then op1 := op1_trunc else
  if ( s = 'round' ) then op1 := op1_round else
  if ( s = 'gauss' ) then op1 := op1_gauss else
  if ( s = 'rand' )  then op1 := op1_rand  else
  if ( s = 'ber' )   then op1 := op1_ber   else
  if ( s = 'gamm' )  then op1 := op1_gamm  else
  if ( s = 'poisson' ) then op1 := op1_poisson else
  if ( s = 'geom' )  then op1 := op1_geom  else
  if ( s = 'expo' )  then op1 := op1_expo  else
  if ( s = 'ln0' )   then op1 := op1_ln0   else
    est_op1 := false;
end;  

function  est_reserve(s : string) : boolean;
begin
  est_reserve :=
    ( s = 't'     ) or
    ( s = '-'     ) or 
    ( s = 'cos'   ) or ( s = 'sin' )  or ( s = 'tan' )  or
    ( s = 'acos'  ) or ( s = 'asin' ) or ( s = 'atan' ) or
    ( s = 'ln'    ) or ( s = 'ln0' )  or
    ( s = 'log'   ) or ( s = 'exp'  ) or
    ( s = 'sqrt'  ) or ( s = 'fact' ) or
    ( s = 'abs'   ) or ( s = 'trunc') or ( s = 'round' ) or
    ( s = 'gauss' ) or ( s = 'rand' ) or ( s = 'ber' )   or
    ( s = 'gamm'  ) or ( s = 'poisson' ) or
    ( s = 'geom'  ) or ( s = 'expo' ) or
    ( s = 'if'    ) or ( s = 'min' ) or ( s = 'max' ) or
    ( s = 'gaussf' )  or ( s = 'lognormf' ) or
    ( s = 'binomf' )  or ( s = 'poissonf' ) or
    ( s = 'nbinomf')  or ( s = 'nbinom1f' ) or
    ( s = 'tabf' ) or
    ( s = 'beta1f' )  or ( s = 'betaf' ) or
    ( s = 'gratef' )  or ( s = 'bdf' ) or
    ( s = 'groupsumf' ) or ( s = 'groupsum1f') or 
    ( s = 'groupmeanf') or
    ( s = 'groupmaxf' ) or ( s = 'groupminf' ) or 
    ( s = 'groupcardf') or ( s = 'grouppopf' ) or
    ( s = 'groupmultif') or
    ( s = 'groupgrowthf') or ( s = 'grouplifetimef' ) or
    ( s = 'datef' ) or ( s = 'magicf' ) or ( s = 'focalf' );
end;  

function  lir_variable(s : string) : integer;
var nom : integer;
begin
  nom := trouve_dic(s);
  if ( nom = 0 ) then
    begin
      if est_reserve(s) then
        begin
          erreur_syntaxe(s+' reserved word');
          exit;
        end;
      if crea_var then
        begin
          nom := cre_dic(s);
          lir_variable := cre_variable(nom);
        end
      else
        lir_variable := 0;
    end
  else
    lir_variable := trouve_variable(nom);
end;

function  lir_group(s : string) : integer; 
var nom : integer; 
begin 
  nom := trouve_dic(s); 
  if ( nom = 0 ) then
    begin 
      if est_reserve(s) then 
        begin 
          erreur_syntaxe(s + ' reserved word');
          exit; 
        end; 
      if crea_group then 
        begin 
          nom := cre_dic(s); 
          lir_group := cre_group(nom); 
        end
      else
        lir_group := 0;
    end
  else 
    lir_group := trouve_group(nom); 
end; 
 
function lir_arg(s : string;f : integer) : integer;
var nam,i : integer;
begin
  nam := trouve_dic('_'+s);
  if ( nam = 0 ) then
    begin
      lir_arg := 0;
      exit;
    end;
  with fun[f] do
    for i := 1 to nb_arg do
      if ( nam = arg[xarg[i]].nom ) then
        begin
          lir_arg := xarg[i];
          exit;
        end;
  lir_arg := 0;
end;

function  lir_fun(s : string) : integer;
var nom : integer;
begin
  nom := trouve_dic(s);
  if ( nom = 0 ) then
    begin
      if est_reserve(s) then
        begin
          erreur_syntaxe(s + ' reserved word');
          exit;
        end;
      if crea_fun then
        begin
          nom := cre_dic(s);
          lir_fun := cre_fun(nom);
        end
      else
        lir_fun := 0;
    end
  else
    lir_fun := trouve_fun(nom);
end;

function  test_fun_group(f,narg : integer) : boolean;
{ fonctions qui ont un nom de groupe en argument }
begin
  test_fun_group := false;
  if ( narg <> fun[f].nb_arg ) then exit;
  test_fun_group := ( f = fun_groupsumf )  or
                    ( f = fun_groupsum1f ) or
                    ( f = fun_groupcardf ) or
                    ( f = fun_grouppopf )  or
                    ( f = fun_groupmultif ) or
                    ( f = fun_groupgrowthf ) or
                    ( f = fun_grouplifetimef );
end;

function lirlarg(s : string; f,narg : integer) : integer;
label 10;
var x,tx,i,n,ls : integer;
    c : char;
    s1,s2,niv : string;
begin
  if ( s = '' ) then
    begin
      erreur_syntaxe('missing arguments in function ' + s_ecri_fun(f));
      exit;
    end;
  ls := length(s);
  niv := '';
  for i := 1 to ls do niv := niv + ' ';
  n  := 0;
  for i := 1 to ls do
    begin
      c := s[i];
      if ( c = '(' ) then n := n + 1;
      if ( c = ')' ) then n := n - 1;
      if ( n = 0 ) then niv[i] := '0';
    end;
  if ( n > 0 ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  for i := 1 to ls do
    if ( niv[i] = '0' ) and ( s[i] = ',' ) then goto 10;
  if ( narg > 1 ) then
    begin
      erreur_syntaxe('missing arguments in function ' + s_ecri_fun(f));
      exit;
    end;
  group_expected := test_fun_group(f,narg);
  lirexp(s,x,tx); 
  group_expected := false;
  lirlarg := cons(x,tx,0);
  exit;
10 :
  if ( narg <= 1 ) and ( fun[f].nb_arg <> 0 ) then
    begin
      erreur_syntaxe('too many arguments in function ' + s_ecri_fun(f));
      exit;
    end;
  coupe(s,i,s1,s2);
  if ( s1 = '' ) or ( s2 = '' ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  group_expected := test_fun_group(f,narg);
  lirexp(s1,x,tx);
  group_expected := false;
  if ( narg > 0 ) then
    lirlarg := cons(x,tx,lirlarg(s2,f,narg-1))
  else
    lirlarg := cons(x,tx,lirlarg(s2,f,0));
end;

procedure lirterm(s : string;var x,tx : integer);
var x2,tx2,op1,f,pos,ls : integer;
    a : extended;
    s1,s2 : string;
begin
  ls := length(s);
  if (s[ls] <> ')') then
    if est_nom(s) then
      begin
        if ( fun_compil <> 0 ) then
          begin
            x := lir_arg(s,fun_compil);
            if ( x <> 0 ) then
              begin
                tx := type_arg;
                exit;
              end;
          end;
        if group_expected then {iiiii}
          begin
            x  := lir_group(s); 
            tx := type_group; 
            if ( x = 0 ) then erreur_syntaxe(s); 
            exit;
          end;
        x  := lir_variable(s);
        tx := type_variable;
        if ( x = 0 ) then erreur_syntaxe(s);
        exit;
      end
    else
      begin
        if group_expected then
          begin
            erreur_syntaxe(s);
            exit;
          end;
        if est_reel(s,a) then
          begin
            tx := type_ree;
            x  := cre_ree(a);
            exit;
          end 
        else
          begin
            erreur_syntaxe(s);
            exit;
          end;
      end;
  pos := position(s,'(');
  if ( pos = 0 ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  coupe(sous_chaine(s,1,ls-1),pos,s1,s2);
  if ( pos > 1 ) then
    begin
      if not est_op1(s1,op1) then { fun }
        begin
          if not est_nom(s1) then
            begin
              erreur_syntaxe(s);
              exit;
            end;
          f := lir_fun(s1);
          if ( f = 0 ) then
             begin
               erreur_syntaxe(s);
               exit;
             end;
          tx := type_lis;
          x  := cons(f,type_fun,lirlarg(s2,f,fun[f].nb_arg));
        end
      else { op1 }
        begin
          lirexp(s2,x2,tx2);
          tx := type_lis;
          x  := cons(op1,type_op1,cons(x2,tx2,0));
        end;
    end
  else   {  cas (...)   }
    begin
      s1 := sous_chaine(s,2,ls-2);
      lirexp(s1,x,tx);
    end;
end;

procedure lirexp(s : string;var x,tx : integer);
label 10;
var op2,op1,x1,x2,tx1,tx2,prio,i,ls,n : integer;
    c : char;
    s1,s2,niv,pri : string;
begin
  ls := length(s);
  if ( ls = 0 ) then
    begin
      x  := 0;
      tx := type_lis ;
      exit;
    end;
  niv := '';
  pri := '';
  for i := 1 to ls do
    begin
      niv := niv + ' ';
      pri := pri + ' ';
    end;
  n := 0;
  for i := 1 to ls do
    begin
      c := s[i];
      if est_op2(c,op2) then pri[i] := chr(prio_op2(op2));
      if ( c = '(' ) then n := n + 1;
      if ( c = ')' ) then n := n - 1;
      if ( n = 0 ) then niv[i] := '0';
    end;
  if ( n > 0 ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  for prio := 4 downto 1 do
    for i := ls downto 1 do
      if ( niv[i] = '0' ) and ( pri[i] = chr(prio) ) then goto 10;
  lirterm(s,x,tx);
  exit;
10 :
  if est_op2(s[i],op2) then;
  coupe(s,i,s1,s2);
  if ( s1 = '' ) then
    if ( op2 = op2_moins ) then
      begin
        op1 := op1_moins;
        if ( ls <= 1 ) then
          begin
            erreur_syntaxe(s);
            exit;
          end;
        lirexp(s2,x2,tx2);
        if ( tx2 = type_ree ) then
          begin
            tx := type_ree;
            x  := cre_ree(-ree[x2].val);
            exit;
          end;
        tx := type_lis;
        x  := cons(op1,type_op1,cons(x2,tx2,0));
      end
    else
      begin
        erreur_syntaxe(s);
        exit;
      end
  else
    begin
      lirexp(s1,x1,tx1);
      if ( s2 = '' ) then
        begin
          erreur_syntaxe(s);
          exit;
        end;
      lirexp(s2,x2,tx2);
      tx := type_lis;
      x  := cons(op2,type_op2,cons(x1,tx1,cons(x2,tx2,0)));
    end;
end;


{  @@@@@@   ecriture des objets  @@@@@@  }

function  s_ecri_dic(x : integer) : string;
var s : string;
begin
  s := '';
  while ( x <> 0 ) do with lis[x] do
    begin
      s := s + chr(car);
      x := cdr;
    end;
  s_ecri_dic := s;
end;

function  s_ecri_val(a : extended) : string;
var s : string;
begin
  s := FloatToStr(a);
  if ( length(s) > 10 ) then s := Format('%1.4g',[a]);
  s_ecri_val := s;
end;

function  s_ecri_ree(x : integer) : string;
begin
  s_ecri_ree := s_ecri_val(ree[x].val);
end;

procedure xwrite9(a : extended);
var i : integer;
begin
  i := round(a);
  if ( a - i = 0.0 ) then
    begin
      write(' ',a:9:0);
      exit;
    end;
  if ( abs(a) > 10.0) then
    write(' ',a:9:1)
  else
    if ( abs(a) > 1.0 ) then
      write(' ',a:9:2)
    else
      if ( abs(a) > 0.0001 ) then
        write(' ',a:9:4)
      else
        write(' ',a:9:9);
end;

function  s_ecri_var(x : integer) : string;
begin
  with variable[x] do s_ecri_var := s_ecri_dic(nom);
end;

{ ------ ordre sur les phenotypes ------ }

procedure ordre_pheno(n,type_ordre : integer);
label 1;
var x,y,z,u,v,w,h,ln2 : integer;

function  compare(i1,i2 : integer) : boolean;
var b : boolean;
begin
  case type_ordre of
   ordre_num  : b := lis2[aaa[i1]].atom.num  > lis2[aaa[i2]].atom.num;
  -ordre_date : b := lis2[aaa[i1]].atom.datc < lis2[aaa[i2]].atom.datc;
   ordre_date : b := lis2[aaa[i1]].atom.datc > lis2[aaa[i2]].atom.datc;
  -ordre_nb   : b := lis2[aaa[i1]].atom.nb   < lis2[aaa[i2]].atom.nb;
   ordre_nb   : b := lis2[aaa[i1]].atom.nb   > lis2[aaa[i2]].atom.nb;
  -ordre_val  : b := false;
   ordre_val  : b := false;
  else b := false;
  end;
  compare := b;
end;

begin
  ln2 := trunc(ln(n)*1.442695022 + 0.00001);
  z := n;
  for x := 1 to ln2 do
    begin
      z := z div 2;
      u := n - z;
      for y := 1 to u do
        begin
          v := y;
1:
          w := v + z;
          if compare(v,w) then
            begin
              h := aaa[v];
              aaa[v] := aaa[w];
              aaa[w] := h;
              v := v - z;
              if ( v >= 1 ) then goto 1;
            end;
        end;
    end;
end;

procedure ordre_pheno_val(n,ival,ityp : integer);
label 1;
var x,y,z,u,v,w,h,ln2 : integer;

function  compare(i1,i2 : integer) : boolean;
var b : boolean;
begin
  case ityp of
  -1 :  b := lis2[aaa[i1]].atom.lmut[ival] < lis2[aaa[i2]].atom.lmut[ival];
   1 :  b := lis2[aaa[i1]].atom.lmut[ival] > lis2[aaa[i2]].atom.lmut[ival];
  -2 :  b := lis2[aaa[i1]].atom.lval[ival] < lis2[aaa[i2]].atom.lval[ival];
   2 :  b := lis2[aaa[i1]].atom.lval[ival] > lis2[aaa[i2]].atom.lval[ival];
  -3 :  b := lis2[aaa[i1]].atom.lloc[ival] < lis2[aaa[i2]].atom.lloc[ival];
   3 :  b := lis2[aaa[i1]].atom.lloc[ival] > lis2[aaa[i2]].atom.lloc[ival];
  else  b := false;
  end;
  compare := b;
end;

begin
  ln2 := trunc(ln(n)*1.442695022 + 0.00001);
  z := n;
  for x := 1 to ln2 do
    begin
      z := z div 2;
      u := n - z;
      for y := 1 to u do
        begin
          v := y;
1:
          w := v + z;
          if compare(v,w) then
            begin
              h := aaa[v];
              aaa[v] := aaa[w];
              aaa[w] := h;
              v := v - z;
              if ( v >= 1 ) then goto 1;
            end;
        end;
    end;
end;

procedure list_pheno_group(x : integer);
var y,i : integer;
begin
  with group[x] do
    begin
      y := x_deb;
      i := 0;
      while ( y <> 0 ) do with lis2[y] do
        begin
          i := i + 1;
          aaa[i] := y;
          y := suiv;
        end;
    end;
end;

{ ------ ecriture des variables ------ }

procedure b_ecri_varrel_group(x : integer);
var y,i,ityp : integer;
begin
  with variable[x] do with group[xgroup] do if ( longlis2 > 0 ) then
    begin
      list_pheno_group(xgroup);
      ityp := 2;
      if ( type_ordre < 0 ) then ityp := -ityp;
      case abs(type_ordre) of
      ordre_date,ordre_nb : ordre_pheno(longlis2,type_ordre);
      ordre_val : ordre_pheno_val(longlis2,irel,ityp);
      else;
      end;
      for i := 1 to longlis2 do
        begin
          y := aaa[i];
          writeln(s_ecri_pheno(aaa[i]));
          writeln('   ',s_ecri_var(x),' = ',s_ecri_val(lis2[y].atom.lval[irel]));
        end;
    end;
end;

procedure b_ecri_varreli_group(x : integer);
var y,i,ityp : integer;
begin
  with variable[x] do with group[xgroup] do if ( longlis2 > 0 ) then
    begin
      list_pheno_group(xgroup);
      ityp := 2;
      if ( type_ordre < 0 ) then ityp := -ityp;
      case abs(type_ordre) of
      ordre_date,ordre_nb : ordre_pheno(longlis2,type_ordre);
      ordre_val : ordre_pheno_val(longlis2,ireli,ityp);
      else;
      end;
      for i := 1 to longlis2 do
        begin
          y := aaa[i];
          writeln(s_ecri_pheno(aaa[i]));
          writeln('   ',s_ecri_var(x),' = ',s_ecri_val(lis2[y].atom.lvali[ireli]));
        end;
    end;
end;

procedure b_ecri_varmut_group(x: integer);
var y,i,ityp : integer;
begin
  with variable[x] do with group[xgroup] do if ( longlis2 > 0 ) then
    begin
      list_pheno_group(xgroup);
      ityp := 1;
      if ( type_ordre < 0 ) then ityp := -ityp;
      case abs(type_ordre) of
      ordre_date,ordre_nb : ordre_pheno(longlis2,type_ordre);
      ordre_val : ordre_pheno_val(longlis2,imut,ityp);
      else;
      end;
      for i := 1 to longlis2 do
        begin
          y := aaa[i];
          writeln(s_ecri_pheno(y));
          writeln('   ',s_ecri_var(x),' = ',s_ecri_val(lis2[y].atom.lmut[imut]));
        end;
    end;
end;

procedure b_ecri_varloc_group(x : integer);
var y,i,ityp : integer;
begin
  with variable[x] do with group[xgroup] do if ( longlis2 > 0 ) then
    begin
      list_pheno_group(xgroup);
      ityp := 3;
      if ( type_ordre < 0 ) then ityp := -ityp;
      case abs(type_ordre) of
      ordre_date,ordre_nb : ordre_pheno(longlis2,type_ordre);
      ordre_val : ordre_pheno_val(longlis2,iloc,ityp);
      else;
      end;
      for i := 1 to longlis2 do
        begin
          y := aaa[i];
          writeln(s_ecri_pheno(y));
          writeln('   ',s_ecri_var(x),' = ' + s_ecri_val(lis2[y].atom.lloc[iloc]));
        end;
    end;
end;

procedure  b_ecri_variable(x : integer);
begin
  with variable[x] do
    begin
      writeln(s_ecri_var(x),' = ',s_ecri(exp,exp_type));
      writeln(s_ecri_val(val0),' -> ',s_ecri_val_variable(x));
      if ( xgroup <> 0 ) then
        case typvargroup of
          typvargroup_rel  : b_ecri_varrel_group(x);
          typvargroup_reli : b_ecri_varreli_group(x);
          typvargroup_mut  : begin
                               b_ecri_mutation(xmut);
                               b_ecri_varmut_group(x);
                             end;
          typvargroup_loc  :  b_ecri_varloc_group(x);
          else;
        end;
    end;
end;

function  s_ecri_variable(x : integer) : string;
begin
  s_ecri_variable := s_ecri_var(x) + ' -> ' + s_ecri_val_variable(x);
end;

function  s_ecri_val_variable(x : integer) : string;
begin
  with variable[x] do
    if ( xgroup = 0 ) then
      s_ecri_val_variable := s_ecri_val(val)
    else
      s_ecri_val_variable := 'group ' + s_ecri_group(xgroup);
end;

function  s_ecri_exp_variable(x : integer) : string;
begin
  with variable[x] do
    if ( xgroup = 0 ) then
      s_ecri_exp_variable := s_ecri_var(x) + ' = ' + s_ecri(exp,exp_type) + ' -> ' + s_ecri_val_variable(x)
    else
      s_ecri_exp_variable := s_ecri_var(x) + ' = ' + s_ecri(exp,exp_type) + ' -> ' + 'group ' + s_ecri_group(xgroup);
end;

procedure b_ecri_list_variable;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( xgroup = 0 ) then
      writeln(s_ecri_exp_variable(x));
end;

procedure b_ecri_list_variable_group(z : integer);
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( xgroup = z ) then writeln(s_ecri_exp_variable(x));
end;

{ ------ ecriture des fonctions ------ }

function  s_ecri_dic2(x : integer) : string;
begin
  s_ecri_dic2 := s_ecri_dic(lis[x].cdr);
end;

function s_ecri_arg(x : integer) : string;
begin
  with arg[x] do s_ecri_arg := s_ecri_dic2(nom);
end;

function  s_ecri_fun(x : integer) : string;
begin
  with fun[x] do s_ecri_fun := s_ecri_dic(nom);
end;

function  s_ecri_fonction(x : integer) : string;
var i : integer;
    s : string;
begin
  with fun[x] do
    begin
      s := s_ecri_fun(x) + '(';
      for i := 1 to nb_arg-1 do s := s + s_ecri_arg(xarg[i]) + ',';
      s := s + s_ecri_arg(xarg[nb_arg]) + ') = ';
      s := s + s_ecri(exp,exp_type);
    end;
  s_ecri_fonction := s;
end;

procedure b_ecri_fonction(x : integer);
begin
  writeln(s_ecri_fonction(x));
end;

procedure b_ecri_list_fun;
var x : integer;
begin
  for x := fun_nb_predef+1 to fun_nb do writeln(s_ecri_fonction(x));
end;

{ ------ ecriture des modeles ------ }

function  s_ecri_model(x : integer) : string;
begin
  with modele[x] do s_ecri_model := s_ecri_dic(nom);
end;

function  s_ecri_modele(x : integer) : string;
begin
  with modele[x] do
    s_ecri_modele := 'MODEL ' + s_ecri_model(x) + ' -> ' +
                     Format('pop = %1.1f',[pop]);
end;

procedure b_ecri_modele(x : integer);
var i : integer;
    s : string;
begin
  with modele[x] do
    begin
      writeln(s_ecri_modele(x));
      s := '  rel: ';
      for i := 1 to size-1 do s := s + s_ecri_rel(xrel[i]) + ', ';
      s := s + s_ecri_rel(xrel[size]);
      writeln(s);
    end;
end;

procedure b_ecri_list_modele;
var x,i : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      b_ecri_modele(x);
      for i := 1 to size do b_ecri_relation(xrel[i]);
    end;
end;

{ ------ ecriture des groupes et des phenotypes ------ }

function  s_ecri_pheno(x : integer) : string;
var s : string;
begin
  with lis2[x].atom do
    begin
      s := 'Pheno#' + IntToStr(num) +
           ' created from #' + IntToStr(anc) +
           ' at t = ' + IntToStr(datc) +
           '   nb = ' + IntToStr(nb);
      if ( group[xgroup].pop > 0 ) then
        s_ecri_pheno := s +
                    ' -> ' + Format('%2.1g',[100.0*nb/group[xgroup].pop])+'%';
    end;
  s_ecri_pheno := s;
end;

function  s_ecri_pheno1(x : integer) : string;
var s : string;
begin
  with lis2[x].atom do
    begin
      s := 'Pheno#' + IntToStr(num) + ' t = ' + IntToStr(datc);
      if ( nb > 0 ) and ( group[xgroup].pop > 0 ) then
        s := s + ' -> ' + Format('%2.1g',[100.0*nb/group[xgroup].pop])+'%'
      else
        s := s + '-' + IntToStr(datd);
    end;
  s_ecri_pheno1 := s;
end;

procedure b_ecri_pheno(x : integer);
var i : integer;
begin
  with lis2[x].atom do
    begin
      writeln(s_ecri_pheno(x));
      with group[xgroup] do
        begin
          for i := 1 to size do with rel[xrel[i]] do
            writeln('    ',s_ecri_var(xvar),' = ',s_ecri_val(lval[i]));
          for i := 1 to sizei do with rel[xreli[i]] do
            writeln('    ',s_ecri_var(xvar),' = ',s_ecri_val(lvali[i]));
          for i := 1 to sizemut do with mut[xvarmut[i]] do
            writeln('    ',s_ecri_var(xvar),' = ',s_ecri_val(lmut[i]));
          for i := 1 to sizeloc do
            writeln('    ',s_ecri_var(xvarloc[i]),' = ' + s_ecri_val(lloc[i]));
        end;
    end;
end;

function s_ecri_group(x : integer) : string;
begin
  with group[x] do s_ecri_group := s_ecri_dic(nom);
end;

function s_ecri_groupe(x : integer) : string;
begin
  with group[x] do
    s_ecri_groupe := 'GROUP ' + s_ecri_group(x) + ' -> ' +
                     'pop = ' + IntToStr(pop) +
                     '  nb_pheno = ' + IntToStr(longlis2);
end;

procedure b_ecri_groupe0(x : integer);
var i : integer;
    s : string;
begin
  with group[x] do
    begin
      writeln(s_ecri_groupe(x));
      s := '  rel: ';
      for i := 1 to size-1 do s := s + s_ecri_rel(xrel[i]) + ', ';
      s := s + s_ecri_rel(xrel[size]);
      writeln(s);
      s := '  mut: ';
      for i := 1 to sizemut-1 do s := s + s_ecri_mut(xvarmut[i]) + ', ';
      s := s + s_ecri_mut(xvarmut[sizemut]);
      writeln(s);
      for i := 1 to size do b_ecri_relation(xrel[i]);
      writeln('GROUP VARIABLES:');
      b_ecri_list_variable_group(x);
    end;
end;

procedure b_ecri_groupe(x : integer);
begin
  b_ecri_groupe0(x);
  writeln;
  writeln('PHENOTYPES:');
  b_ecri_list_pheno_group(x);
end;

procedure b_ecri_list_group;
var x : integer;
begin
  for x := 1 to group_nb do with group[x] do b_ecri_groupe0(x);
end;

procedure b_ecri_list_pheno_group(x: integer);
var i : integer;
begin
  with group[x] do if ( pop > 0 ) then
    begin
      list_pheno_group(x);
      case abs(type_ordre) of
        ordre_date,ordre_nb : ordre_pheno(longlis2,type_ordre);
        ordre_val : ;
        else;
      end;
      for i := 1 to longlis2 do b_ecri_pheno(aaa[i]);
    end;
end;

procedure b_ecri_philogeny_group(x : integer);
var y,i,n : integer;
    bb : string;

procedure descendants(i,n,niv : integer);
var y,z,j,k : integer;
begin
  niv := niv + 1;
  bb := '';
  for k := 1 to niv-1 do bb := bb + '    ';
  y := aaa[i];
  if ( y < 0 ) then exit;
  with lis2[y].atom do
    begin
      writeln(bb,s_ecri_pheno1(y));
      with group[xgroup] do
        for k := 1 to sizemut do with mut[xvarmut[k]] do
          writeln(bb,s_ecri_var(xvar),' = ',s_ecri_val(lmut[k]));
    end;
  aaa[i] := -y;
  for j := i+1 to n do
    begin
      z := aaa[j];
      if ( z > 0 ) then
        if ( lis2[z].atom.anc = lis2[y].atom.num ) then descendants(j,n,niv);
    end;
  niv := niv - 1;
end;

begin
  with group[x] do
    begin
      i := 0;
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do
        begin
          i := i + 1;
          aaa[i] := y;
          y := suiv;
        end;
      n := i; { = longlis2 }
      if ( n = 0 ) then exit;
      ordre_pheno(n,ordre_num);
      for i := 1 to n do descendants(i,n,0);
    end;
end;

procedure b_ecri_philogeny_group1(x : integer);
var y,i,n : integer;
    bb : string;

procedure descendants(i,n,niv : integer);
var y,z,j,k : integer;
begin
  niv := niv + 1;
  bb := '';
  for k := 1 to niv-1 do bb := bb + '    ';
  y := aaa[i];
  if ( y < 0 ) then exit;
  with lis2[y].atom do
    begin
      writeln(bb,s_ecri_pheno1(y));
      with group[xgroup] do
        for k := 1 to sizemut do with mut[xvarmut[k]] do
          writeln(bb,s_ecri_var(xvar),' = ',s_ecri_val(lmut[k]));
    end;
  aaa[i] := -y;
  for j := i+1 to n do
    begin
      z := aaa[j];
      if ( z > 0 ) then
        if ( lis2[z].atom.anc = lis2[y].atom.num ) then descendants(j,n,niv);
    end;
  niv := niv - 1;
end;

begin
  with group[x] do
    begin
      i := 0;
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do
        begin
          i := i + 1;
          aaa[i] := y;
          y := suiv;
        end;
      y := lis2_lib_deb;
      { if faut verifier que cette liste contient des ancetres }
      { les atomes sont mis à 0 au lancement, sinon... }
      while ( y <> 0 ) do with lis2[y] do
        begin
          with atom do
            if ( xgroup = x ) and ( datd > datc ) and ( num < lis2_num ) then
              begin
                i := i + 1;
                aaa[i] := y;
              end;
          y := suiv;
        end;
      n := i;
      ordre_pheno(n,ordre_num);
      for i := 1 to n do descendants(i,n,0);
    end;
end;

{ ------ ecriture des mutations ------ }

procedure b_ecri_mutation(x : integer);
begin
  with mut[x] do
    begin
      with variable[xvar] do
        writeln('Mutation ',s_ecri_var(xvar),' = ',s_ecri_val(val0));
      writeln('  trigger : ',s_ecri_var(declench_var));
      writeln('  occur   : ',s_ecri(occur_exp,occur_exp_type));
      writeln('  number  : ',s_ecri(nbmut_exp,nbmut_exp_type));
      writeln('  distrib : ',s_ecri(distrib_exp,distrib_exp_type));
      writeln('  replace : ',s_ecri_val(replace));
      writeln('  concern : ' + s_ecri_var(concern_var));
      b_ecri_varmut_group(xvar);
    end;
end;

function  s_ecri_mut(x : integer) : string;
begin
  with mut[x] do s_ecri_mut := s_ecri_var(xvar);
end;


function  s_ecri_mutation(x : integer) : string;
begin
  with mut[x] do s_ecri_mutation := s_ecri_variable(xvar);
end;

procedure b_ecri_list_mut_group(x : integer);
var i : integer;
begin
  with group[x] do
    for i := 1 to sizemut do writeln(s_ecri_mut(xvarmut[i]));
end;

{ ------ ecriture des relations ------ }

function  s_ecri_rel(x : integer) : string;
begin
  with rel[x] do s_ecri_rel := s_ecri_dic(nom);
end;

function  s_ecri_relation(x : integer) : string;
begin
  with rel[x] do s_ecri_relation :=
    s_ecri_var(xvar) + ' = ' + s_ecri(exp,exp_type) +
                       ' -> ' + s_ecri_val_variable(xvar);
end;

procedure  b_ecri_relation(x : integer);
begin
  with rel[x] do
    begin
      writeln('Relation ',s_ecri_rel(x));
      writeln('  ',s_ecri_relation(x));
    end;
end;
 
procedure b_ecri_list_reli_model(x : integer);
{ relations independantes du modele x }
var i : integer;
begin
  with modele[x] do
    for i := 1 to sizei do writeln(s_ecri_rel(xreli[i]));
end;

procedure b_ecri_list_rel_model(x : integer);
var i : integer;
begin
  with modele[x] do
    for i := 1 to size do writeln(s_ecri_rel(xrel[i]));
end;

procedure b_ecri_list_rel_group(x : integer);
var i : integer;
begin
  with group[x] do
    for i := 1 to size do writeln(s_ecri_rel(xrel[i]));
end;

procedure b_ecri_list_reli_group(x : integer);
{ relations independantes du groupe x }
var i : integer;
begin
  with group[x] do
    for i := 1 to sizei do b_ecri_relation(xreli[i]);
end;

procedure b_ecri_list_rel_indep;
{ relations independantes modeles ou groupes }
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do
    if ( xmodele = 0) and ( xgroup = 0 ) then b_ecri_relation(x);
end;

{ ------ ecritures generales ------ }

function  s_ecri_op1(x : integer) : string;
begin
  case x of
    op1_moins   : s_ecri_op1 := '-';
    op1_cos     : s_ecri_op1 := 'cos';
    op1_sin     : s_ecri_op1 := 'sin';
    op1_tan     : s_ecri_op1 := 'tan';
    op1_acos    : s_ecri_op1 := 'acos';
    op1_asin    : s_ecri_op1 := 'asin';
    op1_atan    : s_ecri_op1 := 'atan';
    op1_ln      : s_ecri_op1 := 'ln';
    op1_ln0     : s_ecri_op1 := 'ln0';
    op1_log     : s_ecri_op1 := 'log';
    op1_exp     : s_ecri_op1 := 'exp';
    op1_fact    : s_ecri_op1 := 'fact';
    op1_sqrt    : s_ecri_op1 := 'sqrt';
    op1_abs     : s_ecri_op1 := 'abs';
    op1_trunc   : s_ecri_op1 := 'trunc';
    op1_round   : s_ecri_op1 := 'round';
    op1_gauss   : s_ecri_op1 := 'gauss';
    op1_rand    : s_ecri_op1 := 'rand';
    op1_ber     : s_ecri_op1 := 'ber';
    op1_gamm    : s_ecri_op1 := 'gamm';
    op1_poisson : s_ecri_op1 := 'poisson';
    op1_geom    : s_ecri_op1 := 'geom';
    op1_expo    : s_ecri_op1 := 'expo';
    else s_ecri_op1 := 'ecri_op1?';
  end;
end;

function  s_ecri_op2(x : integer) : string;
begin
  case x of
    op2_plus   : s_ecri_op2 := ' + ';
    op2_mult   : s_ecri_op2 := ' * ';
    op2_moins  : s_ecri_op2 := ' - ';
    op2_div    : s_ecri_op2 := ' / ';
    op2_puis   : s_ecri_op2 := '^';
    op2_infe   : s_ecri_op2 := ' < ';
    op2_supe   : s_ecri_op2 := ' > ';
    op2_mod    : s_ecri_op2 := ' \ ';
    op2_convol : s_ecri_op2 := ' @ ';
    else s_ecri_op2 := 'ecri_op2?';
  end;
end;

function  s_ecri_lis(x : integer) : string;
var y,ty,z,prio : integer;
    s : string;
begin
  s := '';
  if ( x = 0 ) then
    begin
      s_ecri_lis := '';
      exit;
    end;
  with lis[x] do
    begin
      case car_type of
      type_op1 : begin
                   s  := s + s_ecri_op1(car);
                   z  := cdr;
                   y  := lis[z].car;
                   ty := lis[z].car_type;
                   s  := s + '(';
                   s  := s + s_ecri(y,ty);
                   s  := s + ')';
                 end;
      type_op2 : begin
                   z  := cdr;
                   y  := lis[z].car;
                   ty := lis[z].car_type;
                   prio := prio_op2(car);
                   if ( ( prio <= 3 ) and ( ty = type_lis ) ) then s := s + '(';
                   s := s + s_ecri(y,ty);
                   if ( ( prio <= 3 ) and ( ty = type_lis ) ) then s := s + ')';
                   s := s + s_ecri_op2(car);
                   z  := lis[z].cdr;
                   y  := lis[z].car;
                   ty := lis[z].car_type;
                   if {( ( prio <= 3 ) and} ( ty = type_lis ) then s := s + '(';
                   s := s + s_ecri(y,ty);
                   if {( ( prio <= 3 ) and} ( ty = type_lis ) then s := s + ')';
                 end;
      type_fun : begin
                   with fun[car] do
                     begin
                       s := s + s_ecri_fun(car);
                       s := s + '(';
                       z  := lis[x].cdr;
                       while z <> 0 do with lis[z] do
                         begin
                           y  := car;
                           ty := car_type;
                           s := s + s_ecri(y,ty);
                           z := cdr;
                           if ( z <> 0 ) then s := s + ',';
                         end;
                       s := s + ')';
                     end;
                 end;
      else
        begin
          s := s + '(';
          while x <> 0 do with lis[x] do
            begin
              s := s + s_ecri(car,car_type);
              x := cdr;
              if ( x <> 0 ) then s := s + ' ';
            end;
          s := s + ')';
        end
      end;
    end;
  s_ecri_lis := s;
end;

function  s_ecri(x,tx : integer) : string;
begin
  case tx of
    type_lis       : s_ecri := s_ecri_lis(x);
    type_ree       : s_ecri := s_ecri_ree(x);
    type_variable  : s_ecri := s_ecri_var(x);
    type_fun       : s_ecri := s_ecri_fun(x);
    type_arg       : s_ecri := s_ecri_arg(x);
    type_rel       : s_ecri := s_ecri_rel(x);
    type_modele    : s_ecri := s_ecri_model(x);
    type_group     : s_ecri := s_ecri_group(x);
    type_mut       : s_ecri := s_ecri_mut(x);
    type_inconnu   : s_ecri := ' ? ';
    else
      s_ecri := 'ecri? ';
  end;
end;

function  s_ecri2(x,tx : integer) : string;
begin
  case tx of
    type_lis       : s_ecri2 := s_ecri_lis(x);
    type_ree       : s_ecri2 := s_ecri_ree(x);
    type_variable  : s_ecri2 := s_ecri_variable(x);
    type_fun       : s_ecri2 := s_ecri_fun(x);
    type_arg       : s_ecri2 := s_ecri_arg(x);
    type_rel       : s_ecri2 := s_ecri_rel(x);
    type_modele    : s_ecri2 := s_ecri_modele(x);
    type_group     : s_ecri2 := s_ecri_groupe(x);
    type_mut       : s_ecri2 := s_ecri_mutation(x);
    type_inconnu   : s_ecri2 := ' ? ';
    else
      s_ecri2 := 'ecri2? ';
  end;
end;

procedure b_ecri(x,tx : integer);
begin
  case tx of
    type_lis       : ;
    type_ree       : ;
    type_variable  : b_ecri_variable(x);
    type_fun       : b_ecri_fonction(x);
    type_arg       : ;
    type_rel       : b_ecri_relation(x);
    type_modele    : b_ecri_modele(x);
    type_group     : b_ecri_groupe(x);
    type_mut       : b_ecri_mutation(x);
    type_inconnu   : ;
    else;
  end;
end;

procedure s_ecri_lis2(x : integer);
var n : integer;
begin
  n := 0;
  while ( x <> 0 ) do with lis2[x] do
    begin
      n := n + 1;
      with atom do
        writeln(IntToStr(n) +
                 ' num =' + IntToStr(num) +
                 ' datc = ' + IntToStr(datc) +
                 ' datd = ' + IntToStr(datd) +
                 ' anc = ' + IntToStr(anc));
      x := suiv;
    end;
end;
 
end.
