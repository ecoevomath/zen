program zenc;

{$MODE Delphi}

{$APPTYPE CONSOLE}

uses  iglobvar,isymb,isyntax,imath,icompil,ieval,iinterp,irun,icarlo;

procedure erreur_zen(s : string);
begin
  writeln('Error: ',s);
end;

procedure init_defaults;
begin
  nb_cycle := 100;
  dt_texte_interp := 10;
  nb_cycle_carlo := 100;
  nb_run_carlo := 10;
  seuil_ext := 1.0;
  seuil_div := 10000000.0;
end;

procedure start_model;
begin
  init_math; { seed = 1 }
  init_symb;
  compilation;
  if compiled then
    begin
      init_eval;
      init_texte_interp;
      {with form_view_all do init_viewall;
      set_actions1;}
    end;
end;

procedure init_fic_param;
begin
  modelfileopened  := false;
  inputfileopened  := false;
  outputfile := false;
  nomficmodele := '';
  nomficin     := '';
  nomficout    := '';
  if ( ParamCount > 0 ) then
    nomficmodele := ParamStr(1)
  else
    begin
      write('File name ? ');
      readln(nomficmodele);
      nomficmodele := tronque(nomficmodele);
      if ( nomficmodele = '' ) then halt;
    end;
  assign(ficmodele,nomficmodele);
{$I-}
  reset(ficmodele);
  if ( IOresult  <> 0 ) then
    begin
      erreur_zen(nomficmodele +' file not found');
      readln;
      halt;
    end;
{$I+}
  modelfileopened := true;
  if ( ParamCount > 1 ) then
    begin
      nomficin := ParamStr(2);
      assign(ficin,nomficin);
{$I-}
      reset(ficin);
      if ( IOresult  <> 0 ) then
        begin
          erreur_zen(nomficin +' file not found');
          readln;
          exit;
        end;
{$I+}
      inputfileopened := true;
      if ( Paramcount > 2 ) then
        begin
          nomficout := ParamStr(3);
          assign(output,nomficout);
{$I-}
          rewrite(output);
          if ( IOresult <> 0 ) then
            begin
              erreur_zen(nomficout +' could not open file');
              readln;
              exit;
            end;
{$I+}
          outputfile := true;
        end;
    end;
end;

procedure init_zen;
begin
  init_math;
  init_symb;
  init_defaults;
  init_fic_param;
end;

procedure fin_zen;
var i : integer;
begin
  for i := 1 to fic_nb do CloseFile(fic[i].f);
end;

procedure interp1;
var s : string;
begin
  if inputfileopened then
    if eof(ficin) then
      fini := true
    else
      begin
        readln(ficin,s);
        s := tronque(minuscule(s));
        writeln('> ',s);
        interp(s);
      end
  else
    begin
      write('? ');
      readln(s);
      interp(s);
    end
end;

begin
  writeln('          ZEN');
  writeln('Eco-evolutionary software');
  writeln('');
  init_zen;
  compilation;
  init_eval;
  init_texte_interp;
  fini := false;
  while not fini do interp1;
  fin_zen;
end.

