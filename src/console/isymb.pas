unit isymb;

{$MODE Delphi}

{  @@@@@@   gestion symbolique   @@@@@@  }

interface

uses iglobvar;

function  cons(ca,typ,cd : integer) : integer;

procedure cre_lis2(var x_deb,x_fin : integer);
function  long_lis2(x : integer) : integer; 
procedure cre_atom(var x_deb,x_fin : integer);
procedure destruc_atom(var x_deb,x_fin : integer; z : integer);
procedure destruc_lis2(var x_deb,x_fin : integer);
procedure concat_lis2(var x_deb,x_fin : integer; y_deb,y_fin : integer); 

function  cre_dic(mot : string) : integer; 
function  s_get_dic(x : integer) : string;
function  trouve_dic(s : string) : integer; 

function  cre_ree(val1 : extended) : integer;

function  trouve_variable(nom1 : integer) : integer;
function  cre_variable(nom1 : integer) : integer;
procedure set_variable(x,exp1,exp_type1: integer);

function  trouve_fun(nom1 : integer) : integer;
function  cre_fun(nom1 : integer) : integer;
procedure set_fun(x,nb_arg1 : integer;xarg1 : larg_type);
function  cre_arg(nom1 : integer) : integer;

function  trouve_rel(nom1 : integer) : integer;
function  cre_rel(nom1 : integer) : integer;
procedure set_rel(x : integer;exp1,exp_type1,var1 : integer);

function  trouve_modele(nom1 : integer) : integer;
function  cre_modele(nom1 : integer) : integer;

function  trouve_group(nom1 : integer) : integer; 
function  cre_group(nom1 : integer) : integer; 

var pheno0 : pheno_type;

function  trouve_mut(nom1 : integer) : integer;
function  cre_mut(xvar1 : integer) : integer; 
{procedure set_mut(x,oexp1,oexp_type1,rexp1,rexp_type1,dexp1,dexp_type1: integer);}

function  trouve_fic(nam1 : string) : integer;
function  cre_fic(nam1 : string) : integer;

procedure trouve_obj(s : string;var x,tx : integer);
procedure init_symb;

var  err_symb : boolean;

implementation

procedure erreur_symb(s : string);
begin
  writeln('Symb - ',s);
end;

{ ------  listes  ------ } 
 
procedure init_lis; 
var x : integer; 
begin
  for x := 1 to lis_nb_max do lis[x].cdr := x + 1;
  lis[lis_nb_max].cdr := 0;
  lis_lib := 1; 
  lis_nb  := 0; 
end; 
 
procedure mark_lis(x : integer); 
var z : integer; 
begin 
  while ( x <> 0 ) do with lis[x] do
    begin 
      z := cdr; 
      if ( z < 0 ) then exit; 
      cdr := -z - 1; 
      if ( car_type = type_lis ) then mark_lis(car); 
      x := z; 
    end;  
end; 
 
procedure gc(ca,typ,cd : integer); 
var x,n : integer; 
begin 
  if ( typ = type_lis ) then mark_lis(ca); 
  if ( cd <> 0 ) then mark_lis(cd); 
  mark_lis(dic);
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_lis ) then mark_lis(exp); 
  for x := 1 to fun_nb do with fun[x] do
    if ( exp_type = type_lis ) then mark_lis(exp);
  for x := 1 to rel_nb do with rel[x] do
    if ( exp_type = type_lis ) then mark_lis(exp); 
  for x := 1 to mut_nb do with mut[x] do
    begin 
      if ( occur_exp_type   = type_lis ) then mark_lis(occur_exp); 
      if ( nbmut_exp_type   = type_lis ) then mark_lis(nbmut_exp); 
      if ( distrib_exp_type = type_lis ) then mark_lis(distrib_exp); 
    end;
  n := 0; 
  for x := 1 to lis_nb_max do with lis[x] do
    if ( cdr >= 0 ) then 
      begin 
        cdr := lis_lib; 
        lis_lib := x; 
        n := n + 1; 
      end 
    else 
      cdr := -cdr - 1; 
  lis_nb := lis_nb - n; 
  {iwriteln('-> gc : ' + IntToStr(n) + ' cells free');}
end; 
 
function  cons(ca,typ,cd : integer) : integer; 
var x : integer; 
begin 
  if ( lis_lib = 0 ) then gc(ca,typ,cd); 
  if ( lis_lib = 0 ) then 
    begin 
      erreur_symb('no more room for lists');
      halt;
    end; 
  x := lis_lib; 
  with lis[x] do
    begin 
      lis_lib  := cdr; 
      car_type := typ; 
      car := ca; 
      cdr := cd; 
    end; 
  lis_nb := lis_nb + 1; 
  cons := x; 
end; 

{ ------ listes doubles ------ }

procedure init_lis2; 
var x : integer;
begin
  for x := 1 to lis2_nb_max do lis2[x].suiv := x + 1;
  lis2_lib_fin := lis2_nb_max;
  lis2[lis2_lib_fin].suiv := 0;
  for x := 1 to lis2_nb_max do lis2[x].prec := x - 1;
  lis2_lib_deb := 1;
  lis2[lis2_lib_deb].prec := 0;
  lis2_nb  := 0;
  lis2_num := 0;
end;

procedure cre_lis2(var x_deb,x_fin : integer);
begin
  x_deb := 0;
  x_fin := 0;
end;
 
function  long_lis2(x : integer) : integer; 
var n : integer; 
begin 
  n := 0; 
  while ( x <> 0 ) do with lis2[x] do
    begin 
      n := n + 1; 
      x := suiv; 
    end; 
  long_lis2 := n; 
end; 
 
procedure concat_lis2(var x_deb,x_fin : integer; y_deb,y_fin : integer);
{ x_deb est la tete de liste cree, y_fin est la queue }
begin 
  if ( x_fin = 0 ) then 
    begin 
      x_deb := y_deb; 
      x_fin := y_fin; 
    end 
  else 
    begin 
      lis2[x_fin].suiv := y_deb;
      if ( y_deb <> 0 ) then 
        begin 
          lis2[y_deb].prec := x_fin;
          x_fin := y_fin; 
        end; 
    end; 
end; 
 
procedure cre_atom(var x_deb,x_fin : integer);
{ atome mis en tete de liste x_deb }
var y : integer;
begin 
  if ( lis2_lib_deb = 0 ) then
    begin
      erreur_symb('too many phenotypes');
      halt;
    end;
  y := lis2_lib_deb;
  if ( x_deb <> 0 ) then 
    lis2[x_deb].prec := y
  else
    x_fin := y;
  lis2_nb  := lis2_nb  + 1;
  lis2_num := lis2_num + 1;
  with lis2[y] do
    begin
      prec := 0;
      lis2_lib_deb := suiv;
      atom := pheno0;
      with atom do
        begin
          num  := lis2_num;
          datc := t__;
        end;
      suiv := x_deb;
    end;
  x_deb := y;
end;

procedure destruc_atom(var x_deb,x_fin : integer; z : integer);
{ z mis en queue de liste libre }
var x,y : integer; 
begin 
  x := lis2[z].prec;
  y := lis2[z].suiv;
  if ( x = 0 ) then
    if ( y = 0 ) then
      begin
        x_deb := 0;
        x_fin := 0;
      end
    else
      begin
        x_deb := y;
        lis2[y].prec := 0;
      end
  else
    if ( y = 0 ) then
      begin
        x_fin := x;
        lis2[x].suiv := 0;
      end
    else
      begin
        lis2[x].suiv := y;
        lis2[y].prec := x;
      end;
  lis2[z].prec := lis2_lib_fin;
  lis2[z].suiv := 0;
  lis2[lis2_lib_fin].suiv := z;
  lis2_lib_fin := z;
  lis2_nb := lis2_nb - 1;
end;

procedure destruc_lis2(var x_deb,x_fin : integer);
{ liste mise en queue de liste libre }
var n : integer;
begin
  n := long_lis2(x_deb);
  if ( n = 0 ) then exit;
  lis2[x_deb].prec := lis2_lib_fin;
  lis2[lis2_lib_fin].suiv := x_deb;
  lis2_lib_fin := x_fin;
  x_deb := 0;
  x_fin := 0;
  lis2_nb := lis2_nb - n;
end;

{ ------  dictionnaire  ------ }

procedure init_dic; 
begin 
  dic     := 0; 
  dic_nb  := 0; 
  char_nb := 0;
end; 
 
function  cre_chaine(mot : string) : integer; 
var x : integer; 
    n,i : byte;
begin 
  n := length(mot); 
  x := 0;
  for i := n downto 1 do x := cons(ord(mot[i]),type_char,x);
  cre_chaine := x; 
end; 
 
function  cre_dic(mot : string) : integer;
var n : byte; 
    x : integer;
begin 
  n := length(mot);
  x := cre_chaine(mot);
  dic := cons(x,type_lis,dic); 
  dic_nb  := dic_nb + 1; 
  char_nb := char_nb + n;
  cre_dic := x; 
end; 
 
function  s_get_dic(x : integer): string;
var s : string;
begin 
  s := '';
  while ( x <> 0 ) do with lis[x] do
    begin 
      s := s + chr(car);
      x := cdr; 
    end;
  s_get_dic := s;
end; 
 
function  trouve_dic(s : string) : integer; 
var x : integer; 
    mot : string; 
begin 
  x := dic;
  while ( x <> 0 ) do with lis[x] do
    begin
      mot := s_get_dic(car);
      if ( mot = s ) then 
        begin
          trouve_dic := car;
          exit;
        end;  
      x := cdr;   
    end;
  trouve_dic := 0;
end; 

{ ------  reels  ------ }

procedure mark_ree_lis(x : integer);
begin
  while ( x <> 0 ) do with lis[x] do
    begin
      if ( car_type = type_lis ) then mark_ree_lis(car);
      if ( car_type = type_ree ) then ree[car].mark := 1;
      x := cdr;
    end;
end;

procedure mark_ree;
var x : integer;
begin
  for x := 1 to ree_nb_max do ree[x].mark := 0;
  ree[ree_zero].mark := 1;
  for x := 1 to variable_nb do with variable[x] do
    begin
      if ( exp_type = type_ree ) then ree[exp].mark := 1;
      if ( exp_type = type_lis ) then mark_ree_lis(exp);
    end;
  for x := 1 to fun_nb do with fun[x] do
    begin
      if ( exp_type = type_ree ) then ree[exp].mark := 1;
      if ( exp_type = type_lis ) then mark_ree_lis(exp);
    end;
end;        
        
procedure libere_ree;
var x,y,n: integer;
begin
  mark_ree;
  n := 0;
  x := ree_deb;
  while ( x <> 0 ) do
    begin
      y := ree[x].suiv;
      if ( y <> 0 ) then
        if ( ree[y].mark = 0 ) then
          begin
            ree[x].suiv := ree[y].suiv;
            ree[y].suiv := ree_lib;
            ree_lib := y;
            n := n + 1;
          end
        else
          x := y
      else
       x := y;
    end;
  ree_nb := ree_nb - n;
  writeln('-> ',n:1,' reals free');
end;

function  cre_ree(val1 : extended) : integer;
var x : integer;
begin
  if ( ree_lib = 0 ) then libere_ree;
  if ( ree_lib = 0 ) then
    begin
      erreur_symb('too many real constants');
      halt;
    end;
  if ( val1 = 0.0 ) then 
    begin
      cre_ree := ree_zero;
      exit;
    end;
  x := ree_lib;
  with ree[x] do
    begin
      val     := val1;
      ree_lib := suiv;
      suiv    := ree_deb;
    end;
  ree_deb := x;
  ree_nb  := ree_nb + 1;
  cre_ree := x;
end;

procedure init_ree;
var x : integer;
begin
  for x := 1 to ree_nb_max do ree[x].suiv := x + 1;
  ree[ree_nb_max].suiv := 0;
  ree_zero := 1;
  ree[ree_zero].val  := 0.0;
  ree[ree_zero].suiv := 0;
  ree_lib := 2; 
  ree_deb := 1;
  ree_nb  := 1;
end;

{ ------  variables  ------ }
 
function  trouve_variable(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_variable := x;
        exit;
      end;
  trouve_variable := 0;
end;

function  cre_variable(nom1 : integer) : integer;
begin
  variable_nb := variable_nb + 1;
  if ( variable_nb > variable_nb_max ) then
    begin
      erreur_symb('too many variables');
      halt;
    end;
  with variable[variable_nb] do
    begin
      nom      := nom1;
      xrel     := 0;
      xgroup   := 0;
      typvargroup := typvargroup_inconnu;
      irel     := 0;
      ireli    := 0;
      xmut     := 0;
      imut     := 0;
      iloc     := 0;
      mut_declench := 0;
      exp      := 0;
      exp_type := type_inconnu;
      val0     := 0.0;
      val      := 0.0;
    end;
  cre_variable := variable_nb;
end;

procedure set_variable(x,exp1,exp_type1: integer);
begin
  with variable[x] do
    begin
      exp_type := exp_type1;
      exp := exp1;
    end;
end;

function  cre_variable_0(s : string;a : extended) : integer;
var nom,x : integer;
begin
  nom := cre_dic(s);
  x   := cre_variable(nom);
  set_variable(x,cre_ree(a),type_ree);
  variable_nb_predef := variable_nb_predef + 1;
  cre_variable_0 := x;
end;

procedure init_variable; 
begin
  variable_nb := 0; 
  variable_nb_predef := 0; 
  xtime := cre_variable_0('t',0.0); 
end; 

{ ------  fonctions  ------ }

function  trouve_arg(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to arg_nb do with arg[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_arg := x;
        exit;
      end;
  trouve_arg := 0;
end;

function  cre_arg(nom1 : integer) : integer;
begin
  arg_nb := arg_nb + 1;
  if ( arg_nb > arg_nb_max ) then
    begin
      erreur_symb('too many function arguments');
      halt;
    end;
  with arg[arg_nb] do
    begin
      nom := nom1;
      val := 0.0;
    end;
  cre_arg := arg_nb;
end;

procedure init_arg;
begin
  arg_nb := 0;
end;

function  trouve_fun(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to fun_nb do with fun[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_fun := x;
        exit;
      end;
  trouve_fun := 0;
end;

function  cre_fun(nom1 : integer) : integer;
var i : integer;
begin
  fun_nb := fun_nb + 1;
  if ( fun_nb > fun_nb_max ) then
    begin
      erreur_symb('too many functions');
      halt;
    end;
  with fun[fun_nb] do
    begin
      nom    := nom1;
      nb_arg := 0;
      for i := 1 to larg_nb_max do xarg[i] := 0;
      exp := 0;
      exp_type := type_inconnu;
    end;
  cre_fun := fun_nb;
end;

procedure set_fun(x,nb_arg1 : integer;xarg1 : larg_type);
begin
  with fun[x] do
    begin
      nb_arg := nb_arg1;
      xarg   := xarg1;
    end;
end;

function  cre_fun_0(s : string;nb_arg : integer) : integer;
var nom,x,i : integer;
    xarg : larg_type;
begin
  nom := cre_dic(s);
  x := cre_fun(nom);
  for i := 1 to nb_arg do xarg[i] := cre_arg(1);
  set_fun(x,nb_arg,xarg);
  fun[x].exp := 0;
  fun[x].exp_type := type_lis;
  fun_nb_predef := fun_nb_predef + 1;
  cre_fun_0 := x;
end;

procedure init_fun;
begin
  fun_nb := 0;
  fun_nb_predef := 0;
  fun_if        := cre_fun_0('if'       ,3);
  fun_min       := cre_fun_0('min'      ,0);
  fun_max       := cre_fun_0('max'      ,0);
  fun_gaussf    := cre_fun_0('gaussf'   ,2);
  fun_lognormf  := cre_fun_0('lognormf' ,2);
  fun_binomf    := cre_fun_0('binomf'   ,2);
  fun_poissonf  := cre_fun_0('poissonf' ,2); 
  fun_nbinomf   := cre_fun_0('nbinomf'  ,2);
  fun_nbinom1f  := cre_fun_0('nbinom1f' ,2);
  fun_betaf     := cre_fun_0('betaf'    ,2);
  fun_beta1f    := cre_fun_0('beta1f'   ,2);
  fun_tabf      := cre_fun_0('tabf'     ,0);
  fun_bicof     := cre_fun_0('bicof'    ,2);
  fun_gratef    := cre_fun_0('gratef'   ,1);
  fun_bdf       := cre_fun_0('bdf'      ,4);

  fun_groupsumf   := cre_fun_0('groupsumf',  2);
  fun_groupsum1f  := cre_fun_0('groupsum1f', 2);
  fun_groupmeanf  := cre_fun_0('groupmeanf', 1);
  fun_groupmaxf   := cre_fun_0('groupmaxf',  1);
  fun_groupminf   := cre_fun_0('groupminf',  1);
  fun_groupcardf  := cre_fun_0('groupcardf', 1);
  fun_grouppopf   := cre_fun_0('grouppopf',  1);
  fun_groupmultif := cre_fun_0('groupmultif',3);
  fun_groupgrowthf   := cre_fun_0('groupgrowthf',  1);
  fun_grouplifetimef := cre_fun_0('grouplifetimef',1);

  fun_datef       := cre_fun_0('datef',      1);
  fun_magicf      := cre_fun_0('magicf',     0);
  fun_focalf      := cre_fun_0('focalf',     1);
end;

{ ------  relations  ------ }

procedure init_rel;
begin
  rel_nb := 0;
end;

function  trouve_rel(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do
    if ( nom = nom1 ) then
      begin
        trouve_rel := x;
        exit;
      end;
  trouve_rel := 0;
end;

function  cre_rel(nom1 : integer) : integer;
begin
  rel_nb := rel_nb + 1;
  if ( rel_nb > rel_nb_max ) then
    begin
      erreur_symb('too many relations');
      halt;
    end;
  with rel[rel_nb] do
    begin
      nom := nom1;
      xmodele := 0;
      xgroup  := 0;
      indep   := 0;
      exp_type := type_inconnu;
      exp  := 0;
      val0 := 0.0;
      val  := 0.0;
    end;
  cre_rel := rel_nb;
end;

procedure set_rel(x : integer;exp1,exp_type1,var1 : integer); 
begin 
  with rel[x] do
    begin 
      exp  := exp1; 
      exp_type := exp_type1; 
      xvar := var1; 
    end; 
end; 

{ ------  modeles  ------ }

procedure init_modele;
begin
  modele_nb := 0;
end;

function  trouve_modele(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    if ( nom = nom1 ) then
      begin
        trouve_modele := x;
        exit;
      end;
  trouve_modele := 0;
end;

function  cre_modele(nom1 : integer) : integer;
var i : integer;
begin
  modele_nb := modele_nb + 1;
  if ( modele_nb > modele_nb_max ) then
    begin
      erreur_symb('too many models');
      halt;
    end;
  with modele[modele_nb] do
    begin
      nom   := nom1;
      size  := 0;
      for i := 1 to mod_rel_nb_max do xrel[i] := 0;
      sizei := 0;
      for i := 1 to mod_rel_nb_max do xreli[i] := 0;
    end;
  cre_modele := modele_nb;
end;

{ ------  groupes  ------ } 
 
procedure init_group;
var i : integer;
begin
  group_nb := 0;
  with pheno0 do
    begin
      num    := 0;
      datc   := 0;
      datd   := 0;
      anc    := 0;
      xgroup := 0;
      for i := 1 to group_rel_nb_max    do lval[i] := 0.0;
      for i := 1 to group_rel_nb_max    do lval_sav[i] := 0.0;
      for i := 1 to group_rel_nb_max    do lvali[i] := 0.0;
      for i := 1 to group_varmut_nb_max do lmut[i] := 0.0;
      for i := 1 to group_varmut_nb_max do ldat[i] := 0;
      for i := 1 to group_varloc_nb_max do lloc[i] := 0.0;
      nb0 := 0;
      nb  := 0;
    end;
end; 
 
function  trouve_group(nom1 : integer) : integer; 
var x : integer; 
begin 
  for x := 1 to group_nb do with group[x] do
    if ( nom = nom1 ) then 
      begin 
        trouve_group := x; 
        exit; 
      end; 
  trouve_group := 0; 
end; 
 
function  cre_group(nom1 : integer) : integer;
var i : integer;
begin
  group_nb := group_nb + 1;
  if ( group_nb > group_nb_max ) then
    begin
      erreur_symb('too many groups');
      halt;
    end;
  with group[group_nb] do
    begin 
      nom   := nom1; 
      size  := 0;
      sizei := 0;
      sizemut := 0;
      sizeloc := 0;
      for i := 1 to group_rel_nb_max    do xrel[i]    := 0;
      for i := 1 to group_rel_nb_max    do xreli[i]   := 0;
      for i := 1 to group_varmut_nb_max do xvarmut[i] := 0;
      for i := 1 to group_varloc_nb_max do xvarloc[i] := 0;
      cre_lis2(x_deb0,x_fin0);
      cre_atom(x_deb0,x_fin0);
      cre_lis2(x_deb,x_fin);
      cre_lis2(m_deb,m_fin);
      pop0 := 0;
      pop  := 0;
      tvie := 0;
      cre  := 0;
      des  := 0;
      longlis2 := 0;
    end; 
  cre_group := group_nb; 
end;

{ ------  mutations  ------ } 
  
function  trouve_mut(nom1 : integer) : integer; 
var x,y : integer; 
begin 
  for x := 1 to mut_nb do 
    begin
      y := mut[x].xvar;
      with variable[y] do
        if ( nom  = nom1 ) then 
          begin 
            trouve_mut := x;
            exit; 
          end; 
    end;
  trouve_mut := 0; 
end; 
 
function  cre_mut(xvar1 : integer) : integer;
begin
  mut_nb := mut_nb + 1;
  if ( mut_nb > mut_nb_max ) then 
    begin
      erreur_symb('too many mutations');
      halt; 
    end;
  with mut[mut_nb] do
    begin
      xvar             := xvar1;
      declench_var     := 0;
      occur_exp        := 0;
      occur_exp_type   := type_inconnu; 
      {dat              := 0; }
      nbmut_exp        := 0;
      nbmut_exp_type   := type_inconnu;
      distrib_exp      := 0;
      distrib_exp_type := type_inconnu;
      replace          := 0;
      concern_var      := 0;
    end;
  cre_mut := mut_nb;
end;
 
procedure set_mut(x,oexp1,oexp_type1,nexp1,nexp_type1,dexp1,dexp_type1: integer);
begin 
  with mut[x] do
    begin 
      occur_exp_type := oexp_type1; 
      occur_exp := oexp1; 
      nbmut_exp_type := nexp_type1; 
      nbmut_exp := nexp1; 
      distrib_exp_type := dexp_type1; 
      distrib_exp := dexp1; 
    end; 
end; 
 
procedure init_mut;
begin
  mut_nb := 0;
end;

{ ------  fichiers de sortie  ------ }

procedure init_fic;
begin
  fic_nb := 0;
end;

function  trouve_fic(nam1 : string) : integer;
var i : integer;
begin
  for i := 1 to fic_nb do with fic[i] do
    if ( nam = nam1 ) then
      begin
        trouve_fic := i;
        exit;
      end;
  trouve_fic := 0;
end;

function  cre_fic(nam1 : string) : integer;
var i : integer;
begin
  fic_nb := fic_nb + 1;
  if ( fic_nb > fic_nb_max ) then
    begin
      erreur_symb('too many files');
      exit;
    end;
  with fic[fic_nb] do
    begin
      nam := nam1;
      xgroup := 0;
      var_fic_nb := 0;
      for i := 1 to var_fic_nb_max do
        begin
          var_fic[i] := 0;
          precis[i]  := 4;
        end;
    end;
  cre_fic := fic_nb;
end;

{ ------ }

procedure trouve_obj(s : string;var x,tx : integer);
var nom : integer;
begin
  x  := 0;
  tx := 0;
  nom := trouve_dic(s);
  if ( nom = 0 ) then exit;
  x := trouve_mut(nom);
  if ( x <> 0 ) then
    begin
      tx := type_mut;
      exit
    end;
  x := trouve_variable(nom);
  if ( x <> 0 ) then
    begin
      tx := type_variable;
      exit
    end;
  x := trouve_fun(nom);
  if ( x <> 0 ) then
    begin
      tx := type_fun;
      exit
    end;
  x := trouve_rel(nom);
  if ( x <> 0 ) then
    begin
      tx := type_rel;
      exit
    end;
  x := trouve_modele(nom);
  if ( x <> 0 ) then
    begin
      tx := type_modele;
      exit
    end;
  x := trouve_group(nom);
  if ( x <> 0 ) then
    begin
      tx := type_group;
      exit
    end; 
end;

{ ------  init  ------ }

procedure init_symb;
begin
  init_lis;
  init_lis2;
  init_dic; 
  init_ree;
  init_variable;
  init_arg;
  init_fun;
  init_rel;
  init_modele;
  init_group;
  init_mut;
  init_fic;
end;

end.
