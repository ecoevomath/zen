unit irun;

{$MODE Delphi}

{  @@@@@@   run modele   @@@@@@  }

interface

procedure run(nb_cycle : integer);

implementation

uses iglobvar,isymb,isyntax,imath,ieval,iinterp;

{ ------  texte fic  ------ }

procedure run_fic(icyc : integer);
var i,k,x,y : integer;
begin
  if ( icyc mod dt_texte_interp <> 0 ) then exit;
  for i := 1 to fic_nb do with fic[i] do
    if ( xgroup <> 0 ) then with group[xgroup] do
      begin
        y := x_deb;
        while ( y <> 0 ) do with lis2[y] do
          begin
            write(f,t__:6,' ',atom.nb:6);
            for k := 1 to var_fic_nb do
              begin
                x := var_fic[k];
                write(f,' ',eval_var_pheno(x,y):1:precis[k]);
              end;
            writeln(f);
            y := suiv;
          end;
      end
    else
      begin
        write(f,t__:6);
        for k := 1 to var_fic_nb do with variable[var_fic[k]] do
          write(f,' ',val:1:precis[k]);
        writeln(f);
      end;
end;

procedure run_init_fic;
begin
  run_fic(0);
end;

procedure run_fin_fic;
var i : integer;
begin
  for i := 1 to fic_nb do Flush(fic[i].f);
end;

{ ------  texte interp  ------ }

procedure run_init_texte_interp;
var j : integer;
begin
  if texte_interp then
    begin
      write('t':5);
      for j := 1 to nb_vartexte_interp do
        with variable[vartexte_interp[j]] do write(s_ecri_dic(nom):10);
      writeln;
      write(t__:5);
      for j := 1 to nb_vartexte_interp do
        with variable[vartexte_interp[j]] do
          if ( xgroup = 0 ) then
            xwrite9(val)
          else
            write(s_ecri_dic(group[xgroup].nom));
      writeln;
    end;
end;

procedure run_texte_interp(icyc : integer);
var j : integer;
begin
  if texte_interp then
    if ( icyc mod dt_texte_interp = 0 ) then
      begin
        write(t__:5);
        for j := 1 to nb_vartexte_interp do
          with variable[vartexte_interp[j]] do
            if ( xgroup = 0 ) then
              xwrite9(val)
            else
              write(s_ecri_dic(group[xgroup].nom));
        writeln;
      end;
end;

procedure run_fin_texte_interp;
var j : integer;
begin
  write('t = ',t__:1);
  for j := 1 to nb_vartexte_interp do
    with variable[vartexte_interp[j]] do
      if ( xgroup = 0 ) then
        write(' ',s_ecri_dic(nom),' = ',s_ecri_val(val))
      else
        writeln(' ',s_ecri_dic(nom),' -> group',s_ecri_dic(group[xgroup].nom));
  writeln;
end;

{ ------  procedure run  ------ }

procedure run(nb_cycle : integer);
var icyc,t1 : integer;
    p1 : array[1..modele_nb_max] of extended;
    pop1,cre1,des1,tvie1 : array[1..group_nb_max] of integer;

procedure init_run;
var x : integer;
begin
  t1 := t__;
  for x := 1 to modele_nb do with modele[x] do p1[x] := pop;
  for x := 1 to group_nb do with group[x] do
    begin
      pop1[x]  := pop;
      cre1[x]  := cre;
      des1[x]  := des;
      tvie1[x] := tvie;
    end;
end;

procedure fin_run;
var x,b,d,h : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      writeln(s_ecri_modele(x));
      if ( t__ > 0 ) then
        begin
          a := exp((ln0(pop) - ln0(pop0))/t__);
          writeln('  growth rate from [t = 0] -> ',a:1:6);
        end;
      if ( t1 > 0 ) then
        if ( t__ > t1 ) then
          begin
            a := exp((ln0(pop) - ln0(p1[x]))/(t__ - t1));
            writeln('  growth rate from [t = ',t1:1,'] -> ',a:1:6);
          end;
    end;
  for x := 1 to group_nb do with group[x] do
    begin
      writeln(s_ecri_groupe(x));
      if ( t__ > 0 ) then
        begin
          a := exp((ln0(pop) - ln0(pop0))/t__);
          writeln('  growth of group from [t = 0] -> ',a:1:6);
        end;
      if ( t1 > 0 ) then
        if ( t__ > t1 ) then
          begin
            a := exp((ln0(pop) - ln0(pop1[x]))/(t__ - t1));
            writeln('  growth of group from [t = ',t1:1,'] -> ',a:1:6);
          end;
      b := cre - cre1[x];
      d := des - des1[x];
      h := tvie - tvie1[x];
      if ( cre > 0 ) then
        a := tvie/cre
      else
        a := 0.0;
      writeln('  life expectancy of phenotypes from [t = 0] -> ',a:1:4);
      if ( b > 0 ) then
        a := h/b
      else
        a := 0.0;
      if ( t1 > 0 ) then
        writeln('  life expectancy of phenotypes from [t = ',t1:1,'] -> ',a:1:4);
      if ( t__ > t1 ) then
        begin
          if ( b >= d ) then
            a := exp(ln0(b-d)/(t__ - t1))
          else
            a := exp(-ln(d-b)/(t__ - t1));
          writeln('  growth rate of phenotypes from [t = ',t1:1,'] -> ',a:1:4);
        end;
    end; 
end;

begin
  init_run;
  run_init_texte_interp;
  run_init_fic;
  for icyc := 1 to nb_cycle do
    begin
      run_t;
      run_texte_interp(icyc);
      run_fic(icyc);
    end;
  if not texte_interp then run_fin_texte_interp;
  run_fin_fic;
  fin_run;
end;

end.

