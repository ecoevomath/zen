unit fgraph;

{$MODE Delphi}

{  @@@@@@   form for graphics   @@@@@@  }

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, Menus, ActnList, ImgList;

  
const  maxvargraph  = 4;      { maximum number of trajectories on the paintbox }
       maxgraph     = 10000;  { maximum number of cycles that can be displayed }
       { e.g., to display 100000 time steps, trajectories are sampled every 10 time steps }
       maxcolors    = 16;     { number of basic colors }
       max_arc_en_ciel = 255; { number of rainbow colors }
       imaxtab2     = 600;
       jmaxtab2     = imaxtab2;

       { codes for graphic procedures: }
       graf_efface  = 0; { erase graphics }
       graf_traj    = 1; { trajectory (run command) }
       graf_distrib = 2; { distribution }
       graf_carlo   = 3; { trajectory (montecarlo command) }
       graf_group   = 4;

type   tab_graph_type = array[0..maxgraph] of extended;
       tab2_graph_type = array[0..imaxtab2,0..jmaxtab2] of integer;

type

  { tform_graph }

  tform_graph = class(TForm)
    N1: TMenuItem;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    file_save: TAction;
    file_saveas: TAction;
    fileexit: TAction;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Save1: TMenuItem;
    Save2: TMenuItem;
    Exit1: TMenuItem;
    graphset: TAction;
    ToolBar1: TToolBar;
    file_save_button: TToolButton;
    ToolButton4: TToolButton;
    graph_settings_button: TToolButton;
    Settings1: TMenuItem;
    Settings2: TMenuItem;
    PaintBox1: TPaintBox;
    Clear1: TMenuItem;
    Clear2: TMenuItem;
    clear: TAction;
    ToolButton6: TToolButton;
    graph_clear_button: TToolButton;
    ImageList1: TImageList;
    SaveDialog1: TSaveDialog;
    procedure init_graph;
    procedure tab_col;
    procedure efface(Sender: TObject);
    procedure efface_notgplus(Sender: TObject);
    procedure gtraj(nb_steps : integer);
    procedure gspec(nb_steps : integer);
    procedure gdistrib(nb_steps : integer);
    procedure gcarlo(nb_steps : integer);
    procedure gtraj_group(nb_j,nb_i : integer);
    function  test_group : boolean;
    function  get_col_aec(a : extended) : integer;
    function  var_color(x : integer) : integer;
    procedure repaint1(Sender: TObject);
    procedure fileexitExecute(Sender: TObject);
    procedure graphsetExecute(Sender: TObject);
    procedure status_run(nb_steps : integer);
    procedure status_spec(nb_steps : integer);
    procedure status_carlo(nb_steps,nb_traj : integer);
    procedure status_var;
    procedure status_distrib;
    procedure status_dt_graph;
    procedure status_gplus;
    procedure status;
    procedure gjoli;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure file_saveExecute(Sender: TObject);
    procedure file_saveExecute2(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure graph_clear_buttonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    iax,ibx,iay,iby,imargex,imargey : integer; { bornes cadrage }
    iax_bbb,ibx_bbb,iay_bbb,iby_bbb,imargex_bbb,imargey_bbb : integer; { bornes cadrage bbb }
    vx : tab_graph_type; { vecteur graphique courant x }
    vy : tab_graph_type; { vecteur graphique courant y }
    p  : array[0..maxgraph] of TPoint;
    nb_steps_sav : integer; { memorisation du nb de points courant }
    nb_i_sav : integer;
    nb_j_sav : integer;
    grafgraf : integer; { memorisation du graphique courant }
    bbb : TBitmap;      { bitmap pour superposition des graphiques }
    procedure ligne(x1,y1,x2,y2 : extended;col : integer);
    procedure coord(x,y : extended;var ix,iy : integer);
    procedure coord2(x,y : extended;var ix,iy : integer);
    procedure coord_bbb(x,y : extended;var ix,iy : integer);
    procedure coord2_bbb(x,y : extended;var ix,iy : integer);
    procedure cercle(x,y,r : extended;col : integer);
    procedure points(n: integer; vx,vy: tab_graph_type;col : integer);
    procedure vect(n: integer;vx,vy : tab_graph_type;col : integer);
    procedure rectangle(x1,y1,x2,y2 : extended;col : integer);
    procedure rect2(x1,y1,x2,y2 : extended;col : integer);
    procedure rect2_bbb(x1,y1,x2,y2 : extended;col : integer);
    procedure rectfull(x1,y1,x2,y2 : extended;col : integer);
    procedure textenum(x,y : extended;a : extended;col : integer);
    procedure axes(xmi,xma,ymi,yma : extended);
    function  get_color(k : integer) : integer;
    procedure calcul_bornes(nb_steps : integer);
  public
    nomficgraph: string;
    colors : array[0..maxcolors] of integer;    { table des couleurs }
    xmin,xmax,
    ymin,ymax   : extended; { bornes courantes du graphique }
    xming,xmaxg,
    yming,ymaxg : extended; { bornes courantes du graphique groupe }
    spec        : boolean;        { indicateur spectre }
    distrib     : boolean;        { indicateur mode distribution }
    distrib0    : boolean;        { indicateur 0 inclus dans distribution }
    d_distrib   : extended;       { intervalle en mode distribution }
    {ln10x,ln10y : boolean;}        { echelle logarithmique x,y }
    coeff_aec   : extended;       { coefficient couleurs gtraj_group }
    dt_update   : integer;        { intervalle de mise a jour graphique }
    gplus       : boolean;        { superposer les graphiques }
    bord        : boolean;        { axes on/off }
    grid        : boolean;        { grid on/off }
    xscale      : boolean;        { echelle definie en x on/off }
    yscale      : boolean;        { echelle definie en y on/off}
    line0       : boolean;        { traits on/off }
    black_and_white : boolean;    { indicateur mode noir sur blanc }
    white_and_black : boolean;    { indicateur mode blanc sur noir }
    gminmax     : boolean;        { indicateur representation min et max montecarlo }
    gsigma      : boolean;        { indicateur representation sigma montecarlo }
    groupgroup  : boolean;        { indicateur variables group }
    dt_graph    : integer;        { intervalle d'echantillonnage en temps }
    t_graph_group    : integer;   { pas en temps pour graphique }
    t_graph_traj     : integer;   { pas en temps pour graphique groupe }
    vargraph_x       : integer;   { variable en x }
    nb_vargraph_y    : integer;   { nombre de variables en y }
    vargraph_y       : array[1..maxvargraph] of integer; { variables en y }
    vargraph_y_col   : array[1..maxvargraph] of integer;
                       { couleurs des variables en y }
    valgraph_x       : tab_graph_type; { tableau des valeurs x }
    valgraph_x_min   : tab_graph_type;
    valgraph_x_max   : tab_graph_type;
    valgraph_x_sigma : tab_graph_type;
    valgraph_y       : array[1..maxvargraph] of tab_graph_type;
                       { tableau des valeurs y }
    valgraph_y_sigma : array[1..maxvargraph] of tab_graph_type;
                       { tableau des ecart-types des valeurs y (montcarlo) }
    valgraph_y_min   : array[1..maxvargraph] of tab_graph_type;
                       { tableau des min des valeurs y (montecarlo) }
    valgraph_y_max   : array[1..maxvargraph] of tab_graph_type;
                       { tableau des max des valeurs y (montecarlo) }
    tab2_graph       : tab2_graph_type;
    ifg : integer; { numero de la forme }
  end;

var   form_graph : tform_graph;

implementation

uses  iglobvar,iutil,imath,isyntax,fgraphset,fzen;

{$R *.lfm}

var   arc_en_ciel : array[0..max_arc_en_ciel] of integer;
      { table des couleurs arc-en-ciel }
      aec_nb : integer; {nombre de couleurs arc_en_ciel }
      gris_en_ciel : array[0..max_arc_en_ciel] of integer;
      { la meme chose, en gris }

function rgb(r,g,b : integer) : integer;
begin
  rgb := r + 256*(g + 256*b);
end;

procedure tform_graph.tab_col;
begin
  colors[0]  := rgb(  0,  0, 80);     { bleu fonce } {fond}
  colors[1]  := rgb(255,  0,  0);     { rouge }
  colors[2]  := rgb(  0,255,  0);     { vert }
  colors[3]  := rgb(  0,  0,255);     { bleu }
  colors[4]  := rgb(255,255,  0);     { jaune }
  colors[5]  := rgb(255,  0,255);     { magenta }
  colors[6]  := rgb(  0,255,255);     { cyan }
  colors[7]  := rgb(255,255,255);     { blanc }
  colors[8]  := rgb(191,  0,  0);     { rouge fonce }
  colors[9]  := rgb(  0,191,  0);     { vert fonce }
  colors[10] := rgb(  0,  0,191);     { bleu fonce }
  colors[11] := rgb(191,191,  0);     { jaune fonce }
  colors[12] := rgb(191,  0,191);     { magenta fonce }
  colors[13] := rgb(  0,191,191);     { cyan fonce }
  colors[14] := rgb(191,191,191);     { gris }
  colors[15] := rgb(128,128,255);     { violet }
  colors[16] := rgb(255,128,  0);     { orange }
end;

procedure tab_arc_en_ciel;
var i,k : integer;
begin
  arc_en_ciel[0] := rgb(0,0,80); { bleu fonce } {fond}
  i := 0;
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i] := rgb(255,k,0);
  until ( k = 255 );
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i] := rgb(255-k,255,0);
  until ( k = 255 );
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i] := rgb(0,255,k);
  until ( k = 255 );
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i]:= rgb(0,255-k,255);
  until ( k = 255 );
  k := 0;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i]:= rgb(0,0,255-k);
  until ( 255-k = 80 );  
  aec_nb := i-1;
  for i := aec_nb + 1 to 255 do arc_en_ciel[i] := clWhite;
  k := 0;
  gris_en_ciel[0] := clWhite;
  for i := 1 to 255 do
    begin
      gris_en_ciel[i] := rgb(255-k,255-k,255-k);
      k := k + 1;
    end;
end;

function  tform_graph.var_color(x : integer) : integer;
var col : integer;
begin
  if ( x > maxcolors ) then x := x mod maxcolors + 1;
  col := colors[x];
  if ( x = 0 ) then col := clBackground;
  if black_and_white then col := clBlack;
  if white_and_black then col := clWhite;
  var_color := col;
end;

function  tform_graph.get_color(k : integer) : integer;
var col : integer;
begin
  col := vargraph_y_col[k];
  if black_and_white then col := clBlack;
  if white_and_black then col := clWhite;
  get_color := col;
end;

procedure tform_graph.FormCreate(Sender: TObject);
begin
  Left   := 518;
  Top    := 143;
  Height := 571;
  Width  := 500; { paintbox1 = 625x625 }
  adjust(self);
  tab_col;
  tab_arc_en_ciel;
  nomficgraph := '';
  with paintbox1 do
    begin
      ClientWidth  := form_graph.Width;
      ClientHeight := ClientWidth;
      iax := round(ClientWidth/10.0);
      ibx := round(ClientWidth/12.0);
      iay := round(ClientHeight/15.0);
      iby := round(ClientHeight/12.0);
      imargex := iax + ibx;
      imargey := iay + iby;
    end;
  bbb := TBitmap.Create;
  with bbb do
    begin
      Height  := paintbox1.ClientHeight;
      Width   := paintbox1.ClientWidth;
      iax_bbb := iax;
      ibx_bbb := ibx;
      iay_bbb := iay;
      iby_bbb := iby;
      imargex_bbb := imargex;
      imargey_bbb := imargey;
    end;
  file_save.Enabled := false;
  file_saveas.Enabled := false;
  graphset.Enabled := false;
  efface(nil);
end;

procedure tform_graph.init_graph;
var i,k,x,y,j : integer;
begin
  line0  := true;
  bord   := true;
  grid   := false;
  gplus  := false;
  black_and_white := false;
  white_and_black := false;
  xmin := 0.0;
  xmax := 1.0;
  ymin := 0.0;
  ymax := 1.0;
  xscale := false;
  yscale := false;
  spec   := false;
  distrib  := false;
  distrib0 := false;
  d_distrib := 1.0;
  {ln10x := false;
  ln10y := false;}
  coeff_aec := 2.718;
  dt_update := 100;
  gminmax := false;
  gsigma  := false;
  groupgroup := false;
  dt_graph := 1;
  for i := 0 to maxgraph do
    begin
      vx[i] := 0.0;
      vy[i] := 0.0;
      valgraph_x[i]       := 0.0;
      valgraph_x_sigma[i] := 0.0;
      valgraph_x_min[i]   := 0.0;
      valgraph_x_max[i]   := 0.0;
      for k := 1 to maxvargraph do
        begin
          valgraph_y[k][i] := 0.0;
          valgraph_y_sigma[k][i] := 0.0;
          valgraph_y_min[k][i]   := 0.0;
          valgraph_y_max[k][i]   := 0.0;
        end;
    end;
  vargraph_x := xtime;
  k := 0;
  for x := 1 to modele_nb do
    for i := 1 to modele[x].size do
      begin
        y := modele[x].xrel[i];
        with rel[y] do
          if ( k <= maxvargraph-1 ) then
            begin
              k := k + 1;
              vargraph_y[k] := xvar;
            end;
      end;
  nb_vargraph_y := k;
  for k := 1 to nb_vargraph_y do
    begin
      j := vargraph_y[k];
      if ( j > maxcolors ) then j := j mod maxcolors + 1;
      vargraph_y_col[k] := colors[j];
    end;
  for k := nb_vargraph_y+1 to maxvargraph do vargraph_y[k] := 0;
  for k := nb_vargraph_y+1 to maxvargraph do vargraph_y_col[k] := clBackground;
  nb_steps_sav := maxgraph;
  nb_i_sav := imaxtab2;
  nb_j_sav := jmaxtab2;
  efface(nil);
  status;
  grafgraf := graf_efface;
  file_save.Enabled := true;
  file_saveas.Enabled := true;
  graphset.Enabled := true;
end;

procedure tform_graph.status_run(nb_steps : integer);
begin
  statusbar1.Panels[0].Text := 'Run ' + IntToStr(nb_steps);
end;

procedure tform_graph.status_spec(nb_steps : integer);
begin
  statusbar1.Panels[0].Text := 'Log power spec ' + IntToStr(nb_steps);
end;

procedure tform_graph.status_carlo(nb_steps,nb_traj : integer);
begin
  statusbar1.Panels[0].Text :=
   'MonteCarlo ' + IntToStr(nb_steps)+ ' ' + IntToStr(nb_traj);
end;

procedure tform_graph.status_var;
var k : integer;
    s : string;
begin
  if spec then
    s := 'freq'
  else
    s := s_ecri_var(vargraph_x);
  s := 'X: ' + s;
  statusbar1.Panels[1].Text := s;
  s := '';
  for k := 1 to nb_vargraph_y do s := s + ' ' + s_ecri_var(vargraph_y[k]);
  s := 'Y:' + s;
  statusbar1.Panels[2].Text := s;
end;

procedure tform_graph.status_distrib;
var s : string;
begin
  if distrib then
    begin
      s := FloatToStr(d_distrib);
      if distrib0 then
        s := 'Dist0 ' + s
      else
        s := 'Dist ' + s;
    end
  else
    s := '';
  statusbar1.Panels[3].Text := s;
end;

procedure tform_graph.status_dt_graph;
begin
  statusbar1.Panels[4].Text := 'Dt = ' + IntToStr(dt_graph);
end;

procedure tform_graph.status_gplus;
begin
  if gplus then
    statusbar1.Panels[5].Text := '+'
  else
    statusbar1.Panels[5].Text := '';
end;

procedure tform_graph.status;
begin
  statusbar1.Panels[0].Text := '';
  status_var;
  status_distrib;
  status_dt_graph;
  status_gplus;
end;

procedure tform_graph.efface(Sender: TObject);
var col : integer;
begin
  col := colors[0];
  if black_and_white then col := clWhite;
  if white_and_black then col := clBlack;
  rect2(0.0,0.0,1.0,1.0,col);
  rect2_bbb(0.0,0.0,1.0,1.0,col);
end;

procedure tform_graph.efface_notgplus(Sender: TObject);
var col : integer;
begin
  col := colors[0];
  if black_and_white then col := clWhite;
  if white_and_black then col := clBlack;
  rect2(0.0,0.0,1.0,1.0,col);
  if not gplus then rect2_bbb(0.0,0.0,1.0,1.0,col);
end;

procedure tform_graph.coord(x,y : extended;var ix,iy : integer);
begin
  with paintbox1 do
    begin
      ix := round(iax + x*(ClientWidth-imargex));
      iy := round(iby + (1.0-y)*(ClientHeight-imargey));
    end;
end;

procedure tform_graph.coord_bbb(x,y : extended;var ix,iy : integer);
begin
  with bbb do
    begin
      ix := round(iax_bbb + x*(Width-imargex_bbb));
      iy := round(iby_bbb + (1.0-y)*(Height-imargey_bbb));
    end;
end;

procedure tform_graph.coord2(x,y : extended;var ix,iy : integer);
begin
  with paintbox1 do
    begin
      ix := round(x*ClientWidth);
      iy := round((1.0-y)*ClientHeight);
    end;
end;

procedure tform_graph.coord2_bbb(x,y : extended;var ix,iy : integer);
begin
  with bbb do
    begin
      ix := round(x*Width);
      iy := round((1.0-y)*Height);
    end;
end;

procedure tform_graph.ligne(x1,y1,x2,y2 : extended;col : integer);
var ix1,iy1,ix2,iy2 : integer;
begin
  coord(x1,y1,ix1,iy1);
  coord(x2,y2,ix2,iy2);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      MoveTo(ix1,iy1);
      LineTo(ix2,iy2);
    end;
  coord_bbb(x1,y1,ix1,iy1);
  coord_bbb(x2,y2,ix2,iy2);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      MoveTo(ix1,iy1);
      LineTo(ix2,iy2);
    end;
   paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.cercle(x,y,r : extended;col : integer);
var ix1,iy1,ix2,iy2 : integer;
begin
  coord(x-r,y-r,ix1,iy1);
  coord(x+r,y+r,ix2,iy2);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      Ellipse(ix1,iy1,ix2,iy2);
    end;
  coord_bbb(x-r,y-r,ix1,iy1);
  coord_bbb(x+r,y+r,ix2,iy2);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      Ellipse(ix1,iy1,ix2,iy2);
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.points(n: integer; vx,vy: tab_graph_type;col : integer);
var i,ix,iy : integer;
begin
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      for i := 0 to n do
        begin
          coord(vx[i],vy[i],ix,iy);
          Pixels[ix,iy]:=col;
        end;
    end;
  with bbb.Canvas do
    begin
      Pen.Color := col;
      for i := 0 to n do
        begin
          coord_bbb(vx[i],vy[i],ix,iy);
          Pixels[ix,iy]:=col;
        end;
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.vect(n: integer;vx,vy : tab_graph_type;col : integer);
var i : integer;
begin
  for i := 0 to n do coord(vx[i],vy[i],p[i].x,p[i].y);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,n+1);
    end;
  for i := 0 to n do coord_bbb(vx[i],vy[i],p[i].x,p[i].y);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,n+1);
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.rectangle(x1,y1,x2,y2 : extended;col : integer);
begin
  coord(x1,y1,p[0].x,p[0].y);
  coord(x2,y1,p[1].x,p[1].y);
  coord(x2,y2,p[2].x,p[2].y);
  coord(x1,y2,p[3].x,p[3].y);
  coord(x1,y1,p[4].x,p[4].y);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,5);
    end;
  coord_bbb(x1,y1,p[0].x,p[0].y);
  coord_bbb(x2,y1,p[1].x,p[1].y);
  coord_bbb(x2,y2,p[2].x,p[2].y);
  coord_bbb(x1,y2,p[3].x,p[3].y);
  coord_bbb(x1,y1,p[4].x,p[4].y);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,5);
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.rect2(x1,y1,x2,y2 : extended;col : integer);
var ix1,iy1,ix2,iy2 : integer;
begin
  coord2(x1,y1,ix1,iy1);
  coord2(x2,y2,ix2,iy2);
  with paintbox1.Canvas do
    begin
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.rect2_bbb(x1,y1,x2,y2 : extended;col : integer);
var ix1,iy1,ix2,iy2 : integer;
begin
  with bbb.Canvas do
    begin
      coord2_bbb(x1,y1,ix1,iy1);
      coord2_bbb(x2,y2,ix2,iy2);
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.rectfull(x1,y1,x2,y2 : extended;col : integer);
var ix1,iy1,ix2,iy2 : integer;
begin
  coord(x1,y1,ix1,iy1);
  coord(x2,y2,ix2,iy2);
  with paintbox1.Canvas do
    begin
      Pen.Color   := col;
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
  coord_bbb(x1,y1,ix1,iy1);
  coord_bbb(x2,y2,ix2,iy2);
  with bbb.Canvas do
    begin
      Pen.Color   := col;
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.textenum(x,y : extended;a : extended;col : integer);
var ix,iy : integer;
    s : string;
begin
  s := Format('%1.6g',[a]);
  with paintbox1.Canvas do
    begin
      coord(x,y,ix,iy);
      Font.Size := round(72*imax(ClientHeight,ClientWidth)/PixelsPerInch/60.0);
      Font.Name := 'Times New Roman';
      Font.Color := col;
      TextOut(ix,iy,s);
    end;
  with bbb.Canvas do
    begin
      coord_bbb(x,y,ix,iy);
      Font.Size := round(72*imax(Height,Width)/PixelsPerInch/60.0);
      Font.Name := 'Times New Roman';
      Font.Color := col;
      TextOut(ix,iy,s);
    end;
  paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

function  expo(x : extended) : integer;
var n : integer;
begin
  x := abs(x);
  if ( x >= 1.0 ) then
    begin
      n := -1;
      repeat
        x := x/10.0;
        n := n + 1;
      until x < 1.0;
    end
  else
    begin
      n := 0;
      repeat
        x := x*10.0;
        n := n - 1;
      until x > 1.0;
    end;
  expo := n;
end;

function  puis10(n : integer) : extended;
var i : integer;
    a,d : extended;
begin
  if ( n >= 0 ) then
    d := 10.0
  else
    begin
      n := -n;
      d := 0.1;
    end;
  a := 1.0;
  for i := 1 to n do a := a*d;
  puis10 := a;
end;

procedure calcul_echelle(amin,amax : extended;var pas,a1 : extended);
var  k,id : integer;
     d,coeff : extended;
begin
  d := amax - amin;
  k := expo(d)-1;
  coeff := puis10(k);
  d  := d/coeff;
  id := trunc(d);
  id := (id div 10 + 1)*10;
  if ( id > 50 ) then
    pas := 10.0
  else
    if ( id > 20 ) then
      pas := 5.0
    else
      pas := 2.0;
  pas := pas*coeff;
  a1  := amin/pas;
  if ( abs(a1) < bigint ) then
    begin
      id := trunc(a1);
      a1 := id*pas;
    end
  else
    a1 := amin;
  if ( a1 > amin ) then a1 := a1 - pas;
end;

procedure tform_graph.axes(xmi,xma,ymi,yma : extended);
const dd = 0.01;
var col,i : integer;
    x1,xpas,y1,ypas,x,x0,y,y0,xx,yy,ex,ey,dx,dy,xa1,ya1,xa2,ya2 : extended;
begin
  {if ln10x then
    begin
      xmi := log0(xmi);
      xma := log0(xma);
    end;}
  calcul_echelle(xmi,xma,xpas,x1);
  {if ln10y then
    begin
      ymi := log0(ymi);
      yma := log0(yma);
    end;}
  calcul_echelle(ymi,yma,ypas,y1);
  col := clWhite;
  if black_and_white then col := clBlack;
  rectangle(0.0,0.0,1.0,1.0,col);
  ex := xma - xmi;
  xx := x1;
  x0 := (x1 - xmi)/ex;
  x  := x0;
  dx := xpas/ex;
  ya1 := 0.0;
  ya2 := dd;
  if grid then ya2 := 1.0;
  i := 0;
  repeat
    if ( x >= 0.0 ) and ( x <= 1.0 ) then
      begin
        ligne(x,ya1,x,ya2,col);
        textenum(x,-dd,xx,col);
      end;
    i := i + 1;
    x  := x0 + i*dx;
    xx := x1 + i*xpas;
  until ( x > 1.0 );
  ey := yma - ymi;
  yy := y1;
  y0 := (y1 - ymi)/ey;
  y  := y0;
  dy := ypas/ey;
  xa1 := 0.0;
  xa2 := dd;
  if grid then xa2 := 1.0;
  i := 0;
  repeat
    if ( y >= 0.0 ) and ( y <= 1.0 ) then
      begin
        ligne(xa1,y,xa2,y,col);
        textenum(-8*dd,y + 2*dd,yy,col);
      end;
    i  := i + 1;
    y  := y0 + i*dy;
    yy := y1 + i*ypas;
  until ( y > 1.0 );
end;

procedure tform_graph.calcul_bornes(nb_steps : integer);
var i,i0,k : integer;
begin
  i0 := 0;
  if ( grafgraf = graf_distrib ) and not distrib0 then i0 := 1;
  if not xscale then
    begin
      xmin :=  maxextended;
      xmax := -maxextended;
      for i := i0 to nb_steps do
        begin
          xmin := min(xmin,valgraph_x[i]);
          xmax := max(xmax,valgraph_x[i]);
        end;
      if ( grafgraf = graf_carlo ) then
        begin
          if gminmax then
            for i := 0 to nb_steps do
              begin
                xmin := min(xmin,valgraph_x_min[i]);
                xmax := max(xmax,valgraph_x_max[i]);
              end;
          if gsigma then
            for i := 0 to nb_steps do
              begin
                xmin := min(xmin,valgraph_x[i] - valgraph_x_sigma[i]);
                xmax := max(xmax,valgraph_x[i] + valgraph_x_sigma[i]);
              end;
        end;
    end;
  if not yscale then
    begin
      ymin :=  maxextended;
      ymax := -maxextended;
      for k := 1 to nb_vargraph_y do
        for i := i0 to nb_steps do
          begin
            ymin := min(ymin,valgraph_y[k][i]);
            ymax := max(ymax,valgraph_y[k][i]);
          end;
      if ( grafgraf = graf_carlo ) then
        begin
          if gminmax then
            for k := 1 to nb_vargraph_y do
              for i := 0 to nb_steps do
                begin
                  ymin := min(ymin,valgraph_y_min[k][i]);
                  ymax := max(ymax,valgraph_y_max[k][i]);
                end;
          if gsigma then
            for k := 1 to nb_vargraph_y do
              for i := 0 to nb_steps do
                begin
                  ymin := min(ymin,valgraph_y[k][i] - valgraph_y_sigma[k][i]);
                  ymax := max(ymax,valgraph_y[k][i] + valgraph_y_sigma[k][i]);
                end;
        end;
    end;
  if ( xmax = xmin ) then
    begin
      xmax := xmin + 1.0;
      xmin := xmin - 1.0;
    end;
  if ( ymax = ymin ) then
    begin
      ymax := ymin + 1.0;
      ymin := ymin - 1.0;
    end;
end;

procedure tform_graph.gtraj(nb_steps : integer);
var  i,k,col : integer;
     ex,ey : extended;
begin
  nb_steps_sav := nb_steps;
  grafgraf := graf_traj;
  calcul_bornes(nb_steps);
  ex := xmax - xmin;
  ey := ymax - ymin;
  {if ln10x then
    for i := 0 to nb_steps do vx[i] := (log0(valgraph_x[i])-log0(xmin))/log0(ex)
  else}
    for i := 0 to nb_steps do vx[i] := (valgraph_x[i]-xmin)/ex;
  for k := 1 to nb_vargraph_y do
    begin
      {if ln10y then
        for i := 0 to nb_steps do vy[i] := (log0(valgraph_y[k][i])-log0(ymin))/log0(ey)
      else}
        for i := 0 to nb_steps do vy[i] := (valgraph_y[k][i]-ymin)/ey;
      col := get_color(k);
      if line0 then vect(nb_steps,vx,vy,col);
      col := clWhite;
      if black_and_white then col := clBlack;
      points(nb_steps,vx,vy,col);
    end;
  if bord then axes(xmin,xmax,ymin,ymax);
end;

procedure tform_graph.gspec(nb_steps : integer);
var i,k,expo,n : integer;
    p : vecvec_type;
begin
  expo := trunc(ln(nb_steps+1)/ln(2.0));
  if ( expo < 2 ) then exit;
  if ( expo > 13 ) then expo := 13;
  n := round(exp(expo*ln(2.0)));
  SetLength(p,n);
  for i := 0 to n-1 do valgraph_x[i] := i/(n+n);
  for k := 1 to nb_vargraph_y do
    begin
      for i := 0 to n-1 do p[i] := valgraph_y[k][i];
      spectre(n,p);
      for i := 0 to n-1 do
        begin
          p[i] := ln0(p[i]);
          valgraph_y[k][i] := p[i]/ln(10.0);
          { p[i] correspond a la frequence i/2*nb_val }
        end;
    end;
  gtraj(n-1);
  status_spec(n);
end;

procedure tform_graph.gdistrib(nb_steps : integer);
var  i,k,col,i0 : integer;
     ex,ey,y : extended;
begin
  nb_steps_sav := nb_steps;
  grafgraf := graf_distrib;
  if distrib0 then i0 := 0 else i0 := 1;
  calcul_bornes(nb_steps);
  ex := xmax - xmin;
  ey := ymax - ymin;
  for i := i0 to nb_steps do vx[i] := (valgraph_x[i]-xmin)/ex;
  for k := 1 to nb_vargraph_y do
    begin
      col := get_color(k);
      for i := i0 to nb_steps-1 do
        begin
          y := (valgraph_y[k][i]-ymin)/ey;
          if ( y > 0 ) then rectangle(vx[i],0.0,vx[i+1],y,col);
        end;
    end;
  if bord then axes(xmin,xmax,ymin,ymax);
end;

procedure tform_graph.gcarlo(nb_steps : integer);
var i,k,delta,col : integer;
    ex,ey,x,xa,xb,y,ya,yb : extended;
begin
  nb_steps_sav := nb_steps;
  grafgraf := graf_carlo;
  calcul_bornes(nb_steps);
  ex := xmax - xmin;
  ey := ymax - ymin;
  delta := 1*dt_graph;
  if ( nb_steps > 100 )  then delta := 10*dt_graph;
  if ( nb_steps > 1000 ) then delta := 100*dt_graph;
  for i := 0 to nb_steps do vx[i] := (valgraph_x[i]-xmin)/ex;
  for k := 1 to nb_vargraph_y do
    begin
      for i := 0 to nb_steps do vy[i] := (valgraph_y[k][i]-ymin)/ey;
      col := get_color(k);
      if line0 then vect(nb_steps,vx,vy,col);
      points(nb_steps,vx,vy,clWhite);
      if gsigma then
        for i := 1 to nb_steps do
          if ( i mod delta = 0 ) then
            begin
              x  := vx[i];
              ya := (valgraph_y[k][i]-valgraph_y_sigma[k][i]-ymin)/ey;
              yb := (valgraph_y[k][i]+valgraph_y_sigma[k][i]-ymin)/ey;
              ligne(x,ya,x,yb,col);
              y  := vy[i];
              xa := (valgraph_x[i]-valgraph_x_sigma[i]-xmin)/ex;
              xb := (valgraph_x[i]+valgraph_x_sigma[i]-xmin)/ex;
              ligne(xa,y,xb,y,col);
            end;
    end;
  if gminmax then
    begin
      for i := 1 to nb_steps do
        if ( i mod delta = 0 ) then
          begin
            x := (valgraph_x_min[i]-xmin)/ex;
            for k := 1 to nb_vargraph_y do
              begin
                y := (valgraph_y_min[k][i]-ymin)/ey;
                col := get_color(k);
                cercle(x,y,0.004,col);
              end;
          end;
      for i := 1 to nb_steps do
        if ( i mod delta = 0 ) then
          begin
            x := (valgraph_x_max[i]-xmin)/ex;
            for k := 1 to nb_vargraph_y do
              begin
                y := (valgraph_y_max[k][i]-ymin)/ey;
                col := get_color(k);
                cercle(x,y,0.004,col);
              end;
          end;
    end;
  if bord then axes(xmin,xmax,ymin,ymax);
end;

function  tform_graph.get_col_aec(a : extended) : integer;
var k : integer;
begin
  if black_and_white or white_and_black then
    begin
      k := trunc(255*a) + 1;
      if black_and_white then
        get_col_aec := gris_en_ciel[k]
      else
        get_col_aec := gris_en_ciel[255-k+1]
    end
  else
    begin
      k := trunc(aec_nb*(1.0 - a)) + 1;
      get_col_aec := arc_en_ciel[k];
    end;
end;

procedure tform_graph.gtraj_group(nb_j,nb_i : integer);
var  i,j,maxh,minh : integer;
     x1,y1,x2,y2,dx,dy,a,eh : extended;
begin
  nb_i_sav := nb_i;
  nb_j_sav := nb_j;
  grafgraf := graf_group;
  maxh := -bigint;
  minh := bigint;
  for i := 0 to nb_i do
    for j := 0 to nb_j do
      begin
        maxh := imax(tab2_graph[i,j],maxh);
        minh := imin(tab2_graph[i,j],minh);
      end;
  eh := maxh - minh;
  if ( eh = 0.0 ) then exit; {iiiii}
  dx := 1.0/nb_j;
  dy := 1.0/nb_i;
  for j := 0 to nb_j-1 do
    begin
      x1 := j*dx;
      x2 := x1 + dx;
      for i := 0 to nb_i-1 do
        begin
          y1 := i*dy;
          y2 := y1 + dy;
          a := (tab2_graph[i,j] - minh)/eh;
         { a := ln(1.0+a)/ln(2.0); }
          if ( a > 0.0 ) then
            begin
              a := exp(ln(a)/coeff_aec);
              rectfull(x1,y1,x2,y2,get_col_aec(a));
            end;
        end;
    end;
  if bord then axes(xming,xmaxg,yming,ymaxg);
end;

function  tform_graph.test_group : boolean;
var k : integer;
    b : boolean;
begin
  b := variable[vargraph_x].xgroup <> 0;
  for k := 1 to nb_vargraph_y do
    if ( vargraph_y[k] <> 0 ) then
      b := b or ( variable[vargraph_y[k]].xgroup <> 0 );
  test_group := b;
end;

procedure tform_graph.gjoli;
var  i,j,k,col : integer;
     x1,y1,x2,y2,dx,dy : extended;
begin
  dx := 1.0/16;
  dy := 1.0/16;
  k := 0;
  for i := 0 to 15 do
    begin
      y1 := i*dy;
      y2 := y1 + dx;
      for j := 0 to 15 do
        begin
          x1 := j*dx;
          x2 := x1 + dx;
          col := arc_en_ciel[k];
          rectfull(x1,y1,x2,y2,col);
          k := k + 1;
        end;
    end;
end;

procedure tform_graph.repaint1(Sender: TObject);
begin
  with paintbox1 do
    begin
      iax := round(ClientWidth/10.0);
      ibx := round(ClientWidth/12.0);
      iay := round(ClientHeight/15.0);
      iby := round(ClientHeight/12.0);
      imargex := iax + ibx;
      imargey := iay + iby;
      if ( ClientWidth = bbb.Width ) and ( ClientHeight = bbb.Height ) then
        begin
          Canvas.Draw(0,0,bbb);
          exit;
        end;
    end;
  efface_notgplus(nil);
  case grafgraf of
    graf_efface  :;
    graf_traj    : gtraj(nb_steps_sav);
    graf_distrib : gdistrib(nb_steps_sav);
    graf_carlo   : gcarlo(nb_steps_sav);
    graf_group   : gtraj_group(nb_j_sav,nb_i_sav);
    else;
  end;
   paintbox1.Invalidate; { Mark the paintbox to be redrawn }
end;

procedure tform_graph.fileexitExecute(Sender: TObject);
begin
  nb_form_graph := nb_form_graph - 1;
  Visible := false;
end;

procedure tform_graph.graphsetExecute(Sender: TObject);
begin
  with form_graphset do
    begin
      fg := Self;
      Show;
    end;
end;

procedure tform_graph.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fileexitExecute(nil);
end;

procedure tform_graph.file_saveExecute(Sender: TObject);
label 1;
var r1,r2 : TRect;
    bm : TBitmap;
    w1,h1 : integer;
begin
  if gplus then
    begin
      file_saveExecute2(nil);
      exit;
    end;
  if ( nomficgraph = '' ) then
    nomficgraph := s_ecri_model(1) + '.bmp';
  bm := TBitmap.Create;
  try
    with paintbox1 do
      begin
        w1 := ClientWidth;
        h1 := ClientHeight;
        r1 := Rect(Left,Top,ClientWidth+Left,ClientHeight+Top);
      end;
    r2 := Rect(0,0,w1,h1);
    bm.Width  := w1;
    bm.Height := h1;
    bm.Canvas.CopyRect(r2,paintbox1.Canvas,r1);
    if inputfileopened then
      bm.SaveToFile(nomficgraph)
    else
      with savedialog1 do
        begin
          FileName := nomficgraph;
          Filter := 'Bitmap file (*.bmp)|All files(*)';
          DefaultExt := '.bmp';
          InitialDir := ExtractFilePath(nomficgraph);
          if Execute then
            if FileExists(FileName) then
              if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
                 mtConfirmation,[mbYes,mbNo],0) <> mrYes then goto 1;
          nomficgraph := FileName;
          bm.SaveToFile(nomficgraph);
        end;
1:
  finally
    bm.Free;
  end;
  status;
end;

procedure tform_graph.file_saveExecute2(Sender: TObject);
label 1;
begin
  if ( nomficgraph = '' ) then
    nomficgraph := s_ecri_model(1) + '.bmp';
  if inputfileopened then
    bbb.SaveToFile(nomficgraph)
  else
    with savedialog1 do
      begin
        FileName := nomficgraph;
        Filter := 'Bitmap file (*.bmp)|All files(*)';
        DefaultExt := '.bmp';
        InitialDir := ExtractFilePath(nomficgraph);
        if Execute then
          if FileExists(FileName) then
            if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
              mtConfirmation,[mbYes,mbNo],0) <> mrYes then goto 1;
        nomficgraph := FileName;
        bbb.SaveToFile(nomficgraph);
      end;
1:
  status;
end;

procedure tform_graph.graph_clear_buttonClick(Sender: TObject);
begin
  grafgraf := graf_efface;
  efface(nil);
end;

procedure tform_graph.FormDestroy(Sender: TObject);
begin
  bbb.Free;
end;

end.
