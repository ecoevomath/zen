program zen;

{$MODE Delphi}

uses
  Forms, Interfaces,
  fzen in 'fzen.pas' {form_zen},
  fgraph in 'fgraph.pas' {form_graph},
  fgraphset in 'fgraphset.pas' {form_graphset},
  fabout in 'fabout.pas' {form_about},
  frunset in 'frunset.pas' {form_runset},
  fviewvar in 'fviewvar.pas' {form_view_variables},
  fcalc in 'fcalc.pas' {form_calc},
  fedit in 'fedit.pas' {form_edit},
  fviewall in 'fviewall.pas' {form_view_all},
  ftext in 'ftext.pas' {form_text},
  ftextset in 'ftextset.pas' {form_textset};

{.$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(Tform_zen, form_zen);
  Application.CreateForm(Tform_graph, form_graph);
  Application.CreateForm(Tform_graphset, form_graphset);
  Application.CreateForm(Tform_about, form_about);
  Application.CreateForm(Tform_runset, form_runset);
  Application.CreateForm(Tform_view_variables, form_view_variables);
  Application.CreateForm(Tform_calc, form_calc);
  Application.CreateForm(Tform_edit, form_edit);
  Application.CreateForm(Tform_view_all, form_view_all);
  Application.CreateForm(Tform_text, form_text);
  Application.CreateForm(Tform_textset, form_textset);
  Application.Run;
end.
