unit iglobvar;

{$MODE Delphi}


{  @@@@@@  variables globales   @@@@@@  }


interface

const  pisur2 = pi/2.0;
       bigint = 2147483647;
       hortab = chr(9);

{ ------  types  utilitaires  ------ }

const  maxextended = 1.0e+4932;
       minextended = 4.0e-4932;


{ ------  types internes  ------ }

const  type_lis      =  0;
       type_ree      =  1;
       type_op1      =  3;
       type_op2      =  4;
       type_variable =  5;
       type_fun      =  6;
       type_arg      =  7;
       type_rel      = 10;
       type_modele   = 11;
       type_group    = 12;
       type_mut      = 13;
       type_char     = 20;
       type_inconnu  = 100;


{ ------ dictionnaire ------ } 
 
var    dic,dic_nb,char_nb : integer; 


{ ------  listes  ------ }

const  lis_nb_max = 50000;

type   lis_type = record
                    car_type : integer;
                    car      : integer;
                    cdr      : integer;
                  end;
                     
type   alis       = array[1..lis_nb_max] of lis_type;
var    lis        : alis;
       lis_nb     : integer;
       lis_lib    : integer;


{ ------  operateurs   ------ }

const  op2_plus    =  1;
       op2_mult    =  2;
       op2_moins   =  3;
       op2_div     =  4;
       op2_puis    =  5;
       op2_infe    =  6;
       op2_supe    =  7;
       op2_mod     =  8;
       op2_convol  = 10;
       
const  op1_moins   =  101;
       op1_cos     =  102;
       op1_sin     =  103;
       op1_tan     =  104;
       op1_acos    =  105;
       op1_asin    =  106;
       op1_atan    =  107;
       op1_ln      =  108;
       op1_log     =  109;
       op1_exp     =  110;
       op1_fact    =  111;
       op1_sqrt    =  112;
       op1_abs     =  113;
       op1_trunc   =  114;
       op1_round   =  115;
       op1_ln0     =  116;

       op1_gauss   =  130;
       op1_rand    =  131;
       op1_ber     =  132;
       op1_gamm    =  133;
       op1_poisson =  134;
       op1_geom    =  135;
       op1_expo    =  136;
                      
{ ------  reels  ------ }  

const  ree_nb_max = 2000;

type   ree_type = record
                     val  : extended;
                     mark : integer;
                     suiv : integer;
                   end;

var    ree        : array[1..ree_nb_max] of ree_type;
       ree_nb     : integer;
       ree_lib    : integer;
       ree_deb    : integer;
       ree_zero   : integer;


{ ------  variables  ------ }  

const  variable_nb_max = 5000;

const  typvargroup_inconnu = 0;
       typvargroup_rel  = 1;
       typvargroup_reli = 2;
       typvargroup_mut  = 3;
       typvargroup_loc  = 4;

type   variable_type = record
                         nom    : integer;
                         xrel   : integer; { relation associee si <> 0 }                         xgroup : integer; { groupe associe si <> 0 }
                         typvargroup : integer; { type si variable groupe }
                         irel   : integer; { indice d'apparition dans xrel groupe/lval pheno }
                         ireli  : integer; { indice d'apparition dans xreli group/lvali pheno }
                         xmut   : integer; { mutation associee }
                         imut   : integer; { indice d'apparition dans xvarmut groupe/lmut pheno }
                         iloc   : integer; { indice d'apparition dans xvarloc groupe/lloc pheno }
                         mut_declench : integer; { liste des mutations declenchees a l'evaluation de cette variable }
                         exp_type : integer; { type expression associee }                         exp    : integer;   { expression associee }                         val0   : extended;  { valeur initiale }                         val    : extended;  { valeur courante }                       end;

var    variable           : array[1..variable_nb_max] of variable_type;
       variable_nb        : integer;
       variable_nb_predef : integer;
       xtime              : integer;


{ ------  fonctions  ------ }

const  fun_nb_max   = 100;
       larg_nb_max  = 30;

type   larg_type  = array[1..larg_nb_max] of integer;
       rlarg_type = array[1..larg_nb_max] of extended;

type   fun_type = record
                    nom       : integer;
                    nb_arg    : integer;   { nombre d'arguments }                    xarg      : larg_type; { tableau des arguments }                    exp_type  : integer;   { type expression associee }                    exp       : integer;   { expression associee }                  end;

var    fun           : array[1..fun_nb_max] of fun_type;
       fun_nb        : integer;
       fun_nb_predef : integer;
       fun_if        : integer;
       fun_min       : integer;
       fun_max       : integer;
       fun_gaussf    : integer;
       fun_lognormf  : integer;
       fun_binomf    : integer;
       fun_poissonf  : integer;
       fun_nbinomf   : integer;
       fun_nbinom1f  : integer;
       fun_betaf     : integer;
       fun_beta1f    : integer;
       fun_tabf      : integer;
       fun_bicof     : integer;
       fun_gratef    : integer;
       fun_bdf       : integer;

       fun_groupsumf   : integer;
       fun_groupsum1f  : integer;
       fun_groupmeanf  : integer;
       fun_groupmaxf   : integer;
       fun_groupminf   : integer;
       fun_groupcardf  : integer;
       fun_grouppopf   : integer;
       fun_groupmultif : integer;
       fun_groupgrowthf   : integer;
       fun_grouplifetimef : integer;

       fun_datef       : integer;
       fun_magicf      : integer;
       fun_focalf      : integer;


{ ------  arguments des fonctions  ------ }

const  arg_nb_max = 1000;

type   arg_type = record
                    nom : integer;
                    val : extended;
                  end;

var    arg      : array[1..arg_nb_max] of arg_type;
       arg_nb   : integer;


{ ------  relations  ------ }                       

const  rel_nb_max = 500;

type   rel_type = record
                     nom       : integer;
                     xmodele   : integer;  { modele associe si <> 0 }                     xgroup    : integer;  { groupe associe si <> 0 }
                     indep     : integer;  { indicateur relation independante }
                     exp_type  : integer;  { type expression associee }                     exp       : integer;  { expression associee }                     xvar      : integer;  { variable associee }                     val0      : extended; { valeur initiale }                     val       : extended; { valeur courante }                   end;

var    rel      : array[1..rel_nb_max] of rel_type;
       rel_nb   : integer;


{ ------  modeles  ------ }

const  modele_nb_max   = 5; 
       mod_rel_nb_max  = 100;

type   mod_rel_type    = array[1..mod_rel_nb_max] of integer;
       r_mod_rel_type  = array[1..mod_rel_nb_max] of extended;
 
type   modele_type = record
                       nom    : integer;
                       size   : integer;      { taille = nombre de relations d'etat }                       xrel   : mod_rel_type; { tableau des relations d'etat associees }                       sizei  : integer;      { nombre de relations independantes }
                       xreli  : mod_rel_type; { tableau des relations independantes }
                       pop0   : extended;     { nombre initial d'individus }
                       pop    : extended;     { nombre courant d'individus }
                     end;

var    modele        : array[1..modele_nb_max] of modele_type;
       modele_nb     : integer;


{ ------ groupes ------ } 

const  group_nb_max        = 10; 
       group_rel_nb_max    = 20;
       group_varmut_nb_max = 20;
       group_varloc_nb_max = 100;

type   group_rel_type      = array[1..group_rel_nb_max] of integer;
       r_group_rel_type    = array[1..group_rel_nb_max] of extended;
       group_varmut_type   = array[1..group_varmut_nb_max] of integer;
       r_group_varmut_type = array[1..group_varmut_nb_max] of extended;
       group_varloc_type   = array[1..group_varloc_nb_max] of integer;
       r_group_varloc_type = array[1..group_varloc_nb_max] of extended;

type   group_type = record 
                      nom      : integer;
                      size     : integer;   { taille = nb de relations d'etat associees }
                      xrel     : group_rel_type; { tableau des relations d'etat associees }
                      sizei    : integer;   { nombre de relations independantes }
                      xreli    : group_rel_type; { tableau des relations independantes }
                      sizemut  : integer;   { nombre de variables mutantes }
                      xvarmut  : group_varmut_type; { tableau des variables mutantes }
                      sizeloc  : integer;   { nombre de variables locales }
                      xvarloc  : group_varloc_type; { tableau des variables locales }
                      x_deb0   : integer;   { tete liste initiale des phenotypes }
                      x_fin0   : integer;   { queue liste initiale des phenotypes }
                      x_deb    : integer;   { tete liste des phenotypes }
                      x_fin    : integer;   { queue liste des phenotypes }
                      m_deb    : integer;   { tete liste temporaire des mutants }
                      m_fin    : integer;   { queue liste temporaire des mutants }
                      tvie     : integer;   { temps de vie des phenotypes }
                      cre      : integer;   { nombre de phenotypes crees }
                      des      : integer;   { nombre de phenotypes detruits }
                      pop0     : integer;   { nombre initial d'individus }
                      pop      : integer;   { nombre courant d'individus }
                      longlis2 : integer;   { nombre courant de phenotypes }
                    end;

var    group        : array[1..group_nb_max] of group_type;
       group_nb     : integer; 


{ ------ phenotypes ------ } 
 
type   pheno_type = record
                      num  : integer; { numero du phenotype }
                      datc : integer; { date de creation }
                      datd : integer; { date de destruction }
                      anc  : integer; { numero du phenotype ancetre }
                      lval : r_group_rel_type;     { valeurs des variables d'etat }
                      lval_sav : r_group_rel_type; { sauvegarde valeurs des variables d'etat }
                      lvali : r_group_rel_type;     { valeurs des variables des relations independantes }
                      lvali_sav : r_group_rel_type; { sauvegarde valeurs des variables des relations independantes }
                      lmut : r_group_varmut_type;  { valeurs des variables mutantes }
                      ldat : group_varmut_type;    { date prochaine mutation }
                      lloc : r_group_varloc_type;  { valeurs des variables locales }
                      xgroup : integer; { groupe associe }
                      nb0  : integer;   { nombre initial d'individus ayant ce phenotype }
                      nb   : integer;   { nombre courant d'individus ayant ce phenotype }
                    end; 


{ ------  mutations  ------ }   
 
const  mut_nb_max = 100; 
 
type   mut_type   = record 
                      xvar             : integer; { variable associee }
                      declench_var     : integer; { variable declenchante }
                      occur_exp_type   : integer; { type expression date d'occurence }
                      occur_exp        : integer; { expression date d'occurence }
                      nbmut_exp_type   : integer; { type expression nombre de mutations }
                      nbmut_exp        : integer; { expression nombre de mutations }
                      distrib_exp_type : integer; { type expression distributions}
                      distrib_exp      : integer; { expression distribution}
                      concern_var      : integer; { variable concernee mise a 1}
                      replace          : integer; { replace mode 0/1 }
                    end;

var    mut     : array[1..mut_nb_max] of mut_type;
       mut_nb  : integer;


{ ------ listes doubles ------ } 

const  lis2_nb_max = 20000;

type   lis2_type = record 
                     atom : pheno_type; 
                     prec : integer;
                     suiv : integer; 
                   end;

var    lis2         : array[1..lis2_nb_max] of lis2_type;
       lis2_nb      : integer; { nombre de phenotypes actuels }
       lis2_num     : integer; { numero des phenotypes }
       lis2_lib_deb : integer;
       lis2_lib_fin : integer;

{ ------  fichiers de sortie  ------ }

const  fic_nb_max = 5;
       var_fic_nb_max = 10;

type   var_fic_type = array[1..var_fic_nb_max] of integer;

type   fic_type = record
                    nam : string;
                    f   : TextFile;
                    xgroup : integer;
                    var_fic_nb : integer;   { nombre de variables }
                    var_fic : var_fic_type; { variables }
                    precis : var_fic_type;  { precision associee }
                  end;

var    fic : array[1..fic_nb_max] of fic_type;
       fic_nb : integer;

{ ------  variables globales  ------ }

var    t__     : integer; { temps courant }
       traj__  : integer; { trajectoire courante (montecarlo) }

       nomfic : string;           { nom fichier modele courant }
       modelfileopened : boolean; { indicateur ouverture fichier modele }
       nomficin : string;         { nom fichier commande }
       inputfileopened : boolean; { indicateur ouverture fichier commande }
       nomficout : string;        { nom fichier output }
       outputfile : boolean;      { indicateur ouverture fichier output }
       ficout : text;

       nb_cycle : integer;        { nombre de cycles run }
       nb_cycle_carlo : integer;  { nombre de cycles montecarlo }
       nb_run_carlo : integer;    { nombre de trajectoires montecarlo }
       seuil_ext : extended;      { seuil d'extinction montecarlo }
       seuil_div : extended;      { seuil de divergence montecarlo }
       dt_texte_interp : integer; { frequence des affichages fenetre interp }

       runstop : boolean;         { indicateur interrupt exec }

implementation

end.
