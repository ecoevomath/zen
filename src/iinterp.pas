unit iinterp;

{$MODE Delphi}

{  @@@@@@   interpreteur   @@@@@@  }

interface

uses Classes;

const  maxvartexte_interp = 10; { nombre max de variables affichees  }

var    lines_interp : TStrings;
       texte_interp : boolean; { affichage on/off }
       nb_vartexte_interp : integer; { nombre de variables a afficher }
       vartexte_interp : array[1..maxvartexte_interp] of integer;
       {var_file : array[1..maxvartexte_interp] of integer;}

procedure interp(s : string);
procedure init_interp;
procedure fin_interp;
procedure init_texte_interp;

implementation

uses   SysUtils,Dialogs,
       iglobvar,
       isymb,
       isyntax,
       icompil,
       imath,
       irun,
       icarlo,
       iutil,
       fgraph,ftext,fzen;

procedure init_interp;
begin
  lines_interp := TStringList.Create;
end;

procedure fin_interp;
begin
  lines_interp.Free;
end;

procedure erreur_interp(s : string);
begin
  s := 'Interp - ' + s;
  MessageDlg(s,mtError,[mbOk],0,mbOk);
end;

procedure init_texte_interp;
var i,k,x : integer;
begin
  texte_interp := true;
  k := 0;
  for x := 1 to modele_nb do with modele[x] do
    for i := 1 to size do
      if ( k <= maxvartexte_interp-1 ) then
        begin
          k := k + 1;
          vartexte_interp[k] := rel[xrel[i]].xvar;
        end;
  nb_vartexte_interp := k;
end;

procedure interp_texte(s : string);
var i,x,tx : integer;
    xx : array[1..maxvartexte_interp] of integer;
begin
  if ( s = '' ) then
    begin
      texte_interp := false;
      iwriteln('Text OFF');
      exit;
    end;
  separe(s,' ');
  with lines_separe do
    begin
      if ( Count > maxvartexte_interp ) then
        begin
          erreur_interp('too many variables for text interp');
          exit;
        end;
      for i := 0 to Count - 1 do
        begin
          trouve_obj(tronque(lines_separe[i]),x,tx);
          if ( x = 0 ) or ( tx <> type_variable ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          xx[i+1] := x;
        end;
      nb_vartexte_interp := Count;
      for i := 1 to nb_vartexte_interp do vartexte_interp[i] := xx[i];
    end;
  texte_interp := true;
  iwriteln('Text ON');
end;

procedure interp_info;
var truc : packed record case integer of
             0 : ( ii : integer );
             1 : ( c4,c3,c2,c1 : char );
           end;

{procedure affiche_mem;
const mega = 1024*1024;
var mem : TMemoryStatus;
begin
  with mem do
    begin
      mem.dwLength:=SizeOf(TMemoryStatus);
      GlobalMemoryStatus(mem);
      iwriteln('Used memory ' + IntToStr(dwMemoryLoad) + ' %');
      iwriteln('Total ' + FloatToStrF(dwAvailPhys/mega,ffFixed,5,1)
                + ' / ' + FloatToStrF(dwTotalPhys/mega,ffFixed,5,1) + ' Mo';
      iwriteln('Paginated ' + FloatToStrF(Mem.dwAvailPageFile/mega,ffFixed,5,1)
                + ' / ' + FloatToStrF(Mem.dwTotalPageFile/mega,ffFixed,5,1) + ' Mo';
      iwriteln('Virtual ' + FloatToStrF(Mem.dwAvailVirtual/mega,ffFixed,5,1)
                 +' / '+ FloatToStrF(Mem.dwTotalVirtual/mega,ffFixed,5,1) + ' Mo';
    end;
end;}

begin
  iwriteln(Format('dic_nb    = %6d',[dic_nb]));
  iwriteln(Format('char_nb   = %6d',[char_nb]));
  iwriteln(Format('lis_nb    = %6d   [%6d]  %12d',
                  [lis_nb,lis_nb_max,lis_nb_max*SizeOf(lis_type)]));
  iwriteln(Format('lis2_nb   = %6d   [%6d]  %12d',
                  [lis2_nb,lis2_nb_max,lis2_nb_max*SizeOf(lis2_type)]));
  iwriteln('  -> lis2_num  = ' + IntToStr(lis2_num));
  iwriteln(Format('ree_nb    = %6d   [%6d]  %12d',
                  [ree_nb,ree_nb_max,ree_nb_max*SizeOf(ree_type)]));
  iwriteln(Format('var_nb    = %6d   [%6d]  %12d',
                  [variable_nb,variable_nb_max,variable_nb_max*SizeOf(variable_type)]));
  iwriteln(Format('fun_nb    = %6d   [%6d]  %12d',
                  [fun_nb,fun_nb_max,fun_nb_max*SizeOf(fun_type)]));
  iwriteln(Format('  fun_nb_predef = %6d',[fun_nb_predef]));
  iwriteln(Format('arg_nb    = %6d   [%6d]  %12d',
                  [arg_nb,arg_nb_max,arg_nb_max*SizeOf(arg_type)]));
  iwriteln(Format('model_nb  = %6d   [%6d]  %12d',
                  [modele_nb,modele_nb_max,modele_nb_max*SizeOf(modele_type)]));
  iwriteln(Format('  mod_rel_nb_max = %6d',[mod_rel_nb_max]));
  iwriteln(Format('group_nb  = %6d   [%6d]  %12d',
                  [group_nb,group_nb_max,group_nb_max*SizeOf(group_type)]));
  iwriteln(Format('  group_rel_nb_max    = %6d',[group_rel_nb_max]));
  iwriteln(Format('  group_varmut_nb_max = %6d',[group_varmut_nb_max]));
  iwriteln(Format('  group_varloc_nb_max = %6d',[group_varloc_nb_max]));
  iwriteln(Format('mut_nb    = %6d   [%6d]  %12d',
                  [mut_nb,mut_nb_max,mut_nb_max*SizeOf(mut_type)]));
  iwriteln(Format('rel_nb    = %6d   [%6d]  %12d',
                  [rel_nb,rel_nb_max,rel_nb_max*SizeOf(rel_type)]));
  iwriteln(Format('fic_nb    = %6d   [%6d]  %12d',
                  [fic_nb,fic_nb_max,fic_nb_max*SizeOf(fic_type)]));
  iwriteln(Format('maxvargraph = %6d',[maxvargraph]));
  iwriteln(Format('maxgraph    = %6d',[maxgraph]));
  iwriteln(Format('maxvartext  = %6d',[maxvartext]));
  iwriteln(Format('maxtext     = %6d',[maxtext]));
  {iwriteln('AllocMemSize -> ' + IntToStr(AllocMemSize)); }
  with truc do
    begin
      ii := 1937007984;
      iwriteln(c1 + c2 + c3 + c4);
      ii := 1751215717;
      iwriteln(c1 + c2 + c3 + c4);
      ii := 1818584933;
      iwriteln(c1 + c2 + c3 + c4);
      ii := 1852076645;
      iwriteln(c1 + c2 + c3 + c4);
    end;
end;

procedure voir(x,tx : word);
begin
  if ( x = 0 ) then
    begin
      b_ecri_list_modele;
      bwriteln(lines_syntax);
      b_ecri_list_group;
      bwriteln(lines_syntax);
      iwriteln('VARIABLES:');
      b_ecri_list_variable;
      bwriteln(lines_syntax);
      b_ecri_list_rel_indep;
      if ( lines_syntax.Count <> 0 ) then
        begin
          iwriteln('OTHER RELATIONS:');
          bwriteln(lines_syntax);
        end;
      b_ecri_list_fun;
      if ( lines_syntax.Count <> 0 ) then
        begin
          iwriteln('FUNCTIONS:');
          bwriteln(lines_syntax);
        end;
      exit;
    end;
  b_ecri(x,tx);
  bwriteln(lines_syntax);
end;

procedure interp_voir(s : string);
var i,x,tx : integer;
begin
  separe(s,' ');
  with lines_separe do
    begin
      if ( Count = 0 ) then
        begin
          voir(0,0);
          exit
        end;
      for i := 0 to Count - 1 do
        begin
          trouve_obj(tronque(lines_separe[i]),x,tx);
          voir(x,tx);
        end;
    end;
end;

procedure interp_newvar(s : string);
var x,v,tv,pos : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos = 0 ) then exit;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := propre(s2);
  if ( s2 = '' ) then exit;
  if not est_nom(s1) then
    begin
      erreur_interp(s1 + ' unproper name for a variable');
      exit;
    end;
  x := trouve_dic(s1);
  if ( x <> 0 ) then
    begin
      x := trouve_variable(x);
      if ( x <> 0 ) then
        begin
          erreur_interp('variable already exists');
          exit;
        end
      else
        begin
          erreur_interp('name already exists');
          exit;
        end;
    end;
  if est_reserve(s1) then
    begin
      erreur_interp('reserved word');
      exit;
    end;
  lirexp(s2,v,tv);
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  x := cre_dic(s1);
  x := cre_variable(x);
  set_variable(x,v,tv);
  form_zen.run_initExecute(nil);
end;

procedure interp_chg_variable(s : string);
var x,v,tv,pos,nom : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos = 0 ) then exit;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := propre(s2);
  nom := trouve_dic(s1);
  if ( nom = 0 ) then
    begin
      erreur_interp('unknown variable');
      exit;
    end;
  x := trouve_variable(nom);
  if ( x = 0 ) then
    begin
      erreur_interp('unknown variable');
      exit;
    end;
  if ( x = xtime ) then
    begin
      erreur_interp('time cannot be changed');
      exit;
    end;
  lirexp(s2,v,tv);
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  if ( variable[x].xrel <> 0 ) then
    begin
      if ( tv <> type_ree ) then
        begin
          erreur_interp('relation-variable must be set to a real value');
          exit;
        end;
    end;
  if ( variable[x].xmut <> 0 ) then
    begin
      if ( tv <> type_ree ) then
        begin
          erreur_interp('mutation-variable must be set to a real value');
          exit;
        end;
    end;
  set_variable(x,v,tv);
  form_zen.run_initExecute(nil);
end;

{ ------ run ------ }

procedure interp_init(s : string);
var n : integer;
begin
  if ( s <> '' ) then
    if est_entier(s,n) then
      begin
        graine0 := graine00 + (n-1);
        if ( n <= 0 ) then graine0 := graine00;
        graine := graine0;
        iwriteln('random generator seed -> ' + IntToStr(graine0-graine00+1));
      end;
  form_zen.run_initExecute(nil);
end;

procedure interp_run(s : string);
var n1,n2,pos : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then
    begin
      form_zen.run_runExecute(nil);
      exit;
    end;
  pos := position(s,' ');
  with form_zen do
    begin
      if ( pos = 0 ) then
        if est_entier(s,n1) then
          nb_cycle := n1
        else
          begin
            erreur_interp('integer expected');
            exit;
          end
      else
        begin
          coupe(s,pos,s1,s2);
          s2 := tronque(s2);
          if not est_entier(s1,n1) or not est_entier(s2,n2) then
            begin
              erreur_interp('2 integer values expected');
              exit;
            end;
          nb_cycle := n1;
          dt_texte_interp := n2;
        end;
      run_runExecute(nil);
    end;
end;

procedure interp_run_carlo(s : string);
var pos,nb_cycle_carlo1,nb_run_carlo1 : integer;
    seuil_ext1,seuil_div1 : extended;
    s1,s2,s3,s4 : string;
begin
  pos := position(s,' ');
  if ( pos = 0 ) then
    begin
      erreur_interp('arguments expected');
      exit;
    end;
  coupe(s,pos,s1,s2);
  s2 := tronque(s2);
  if not est_entier(s1,nb_cycle_carlo1) then
    begin
      erreur_interp('number of time steps expected');
      exit;
    end;
  if ( nb_cycle_carlo1 < 1 ) then
    begin
      erreur_interp('number of time steps < 1');
      exit;
    end;
  if ( s2 = '' ) then
    begin
      erreur_interp('arguments expected');
      exit;
    end;
  pos := position(s2,' ');
  if ( pos = 0 ) then
    begin
      if not est_entier(s2,nb_run_carlo1) then
        begin
          erreur_interp('number of trajectories expected');
          exit;
        end;
      if ( nb_run_carlo1 < 1 ) then
        begin
          erreur_interp('number of trajectories < 1');
          exit;
        end;
      with form_zen do
        begin
          nb_cycle_carlo := nb_cycle_carlo1;
          nb_run_carlo   := nb_run_carlo1;
          montecarlo_runExecute(nil);
        end;
      exit;
    end;
  seuil_ext1 := seuil_ext;
  seuil_div1 := seuil_div;
  coupe(s2,pos,s2,s3);
  if not est_entier(s2,nb_run_carlo1) then
    begin
      erreur_interp('number of trajectories expected');
      exit;
    end;
  if ( nb_run_carlo1 < 1 ) then
    begin
      erreur_interp('number of trajectories < 1');
      exit;
    end;
  s3 := tronque(s3);
  pos := position(s3,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s3,pos,s3,s4);
      if not est_reel(s3,seuil_ext1) then
        begin
          erreur_interp('extinction threshold expected');
          exit;
        end;
      s4 := tronque(s4);
      if not est_reel(s4,seuil_div1) then
        begin
          erreur_interp('divergence threshold expected');
          exit;
        end;
      if ( seuil_ext1 < 0.0 ) then
        begin
          erreur_interp('extinction threshold < 0');
          exit;
        end;
      if ( seuil_div1 <= 0.0 ) then
        begin
          erreur_interp('divergence threshold <= 0');
          exit;
        end;
      if ( seuil_ext1 >= seuil_div1 ) then
        begin
          erreur_interp('extinction threshold >= divergence threshold');
          exit;
        end;
    end
  else
    begin
      if not est_reel(s3,seuil_ext1) then
        begin
          erreur_interp('extinction threshold expected');
          exit;
        end;
      if ( seuil_ext1 < 0.0 ) then
        begin
          erreur_interp('extinction threshold < 0');
          exit;
        end;
      if ( seuil_ext1 >= seuil_div1 ) then
        begin
          erreur_interp('extinction threshold >= divergence threshold');
          exit;
        end;
    end;
  with form_zen do
    begin
      nb_cycle_carlo := nb_cycle_carlo1;
      nb_run_carlo   := nb_run_carlo1;
      seuil_ext := seuil_ext1;
      seuil_div := seuil_div1;
      montecarlo_runExecute(nil);
    end;
end;

procedure interp_fic(s : string);
{ s = nom_fic x1 ... xn }
{ avec  xi = nom_variable ou xi = nom_variable:precision }
var  nom,x,i,n,xg,ific,pos,p : integer;
     xx,pp : var_fic_type;
     nom_fic,s1,s2 : string;
     b : boolean;
begin
  if ( s = '' ) then exit;
  separe(s,' ');
  with lines_separe do
    begin
      n := Count - 1;
      nom_fic := tronque(lines_separe[0]);
      if ( n = 0 ) then { close fic }
        begin
          b := false;
          for i := 1 to fic_nb do with fic[i] do
            if ( nam = nom_fic ) then
              begin
                b := true;
                CloseFile(f);
                iwriteln('File ' + nam + ' closed');
                fic[i] := fic[fic_nb];
                fic_nb := fic_nb - 1;
                exit;
              end;
          if not b then
            begin
              erreur_interp('unknown file name');
              exit;
            end;
        end;
      if ( n > var_fic_nb_max ) then
        begin
          erreur_interp('too many variables for file');
          exit;
        end;
      for i := 1 to n do
        begin
          s1 := tronque(lines_separe[i]);
          pos := position(s1,':');
          if ( pos > 0 ) then
            begin
              coupe(s1,pos,s1,s2);
              if  est_entier(s2,p) then
                if ( p > 0 ) then
                  pp[i] := p
                else
                  begin
                    erreur_interp('precision: positive integer expected');
                    exit;
                  end
              else
                begin
                  erreur_interp('precision: positive integer expected');
                  exit;
                end;
            end
          else
            pp[i] := 4;
          nom := trouve_dic(tronque(s1));
          if ( nom = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          x := trouve_variable(nom);
          if ( x = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          xx[i] := x;
        end;
      xg := variable[xx[1]].xgroup;
      b := true;
      for i := 2 to n do with variable[xx[i]] do
        b := b and ( xgroup = xg );
      if not b then
        begin
          erreur_interp('file variables muts belong to same group or no group');
          exit;
        end;
      ific := trouve_fic(nom_fic);
      if ( ific = 0 ) then
        ific := cre_fic(nom_fic)
      else
        begin
          {CloseFile(fic[ific].f);}
          erreur_interp('file already opened');
          exit;
        end;
      with fic[ific] do
        begin
          AssignFile(f,nam);
          rewrite(f);
          nam := nom_fic;
          iwriteln('File ' + nam + ' opened');
          var_fic_nb := n;
          var_fic := xx;
          precis  := pp;
          xgroup  := xg;
        end;
    end;
end;

{ ------ graphique ------ }

procedure interp_efface;
begin
  tab_form_graph[i_form_graph].efface(nil);
end;

procedure interp_addgraph;
begin
  with tab_form_graph[i_form_graph] do
    begin
      gplus := not gplus;
      if gplus then
        iwriteln('Addgraph ON (graphic window #' + IntToStr(i_form_graph) + ')')
      else
        iwriteln('Addgraph OFF (graphic window #' + IntToStr(i_form_graph) + ')');
      status_gplus;
    end;
end;

procedure interp_graph(s : string);
var  xx,nom,i,x : integer;
     yy : array[1..maxvargraph] of integer;
begin
  if ( s = '' ) then exit;
  separe(s,' ');
  with tab_form_graph[i_form_graph],lines_separe do
    begin
      if ( Count-1 > maxvargraph ) then
        begin
          erreur_interp('too many variables for graphics');
          exit;
        end;
      if ( Count < 2 ) then
        begin
          erreur_interp('Y-variables missing');
          exit;
        end;
      nom := trouve_dic(tronque(lines_separe[0]));
      if ( nom = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      xx := trouve_variable(nom);
      if ( xx = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      for i := 1 to Count - 1 do
        begin
          nom := trouve_dic(tronque(lines_separe[i]));
          if ( nom = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          x := trouve_variable(nom);
          if ( x = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          yy[i] := x;
        end;
      vargraph_x := xx;
      nb_vargraph_y := Count - 1;
      for i := 1 to nb_vargraph_y do vargraph_y[i] := yy[i];
      for i := nb_vargraph_y + 1 to maxvargraph do vargraph_y[i] := 0;
      for i := 1 to maxvargraph do
        vargraph_y_col[i] := var_color(vargraph_y[i]);
      groupgroup := test_group;
      status_var;
    end;
end;

procedure interp_graph_window(s : string);
var i : integer;
begin
  if ( s = '' ) then
    begin
      erreur_interp('positive integer expected');
      exit;
    end;
  if not est_entier(s,i) then
    begin
      erreur_interp('positive integer expected');
      exit;
    end;
  if ( i < 1 ) then
    begin
      erreur_interp('positive integer expected');
      exit;
    end;
  if ( i > maxform_graph ) then
    begin
      erreur_interp('No more than ' + IntToStr(maxform_graph) +
                    ' graphic windows available');
      exit;
    end;
  i_form_graph := i;
  with tab_form_graph[i_form_graph] do
    if not Visible then Show;
  iwriteln('Graphic window #' +  IntToStr(i) +' selected');
end;

procedure interp_xscale(s : string);
var  pos : integer;
     s1,s2 : string;
     a,b : extended;
     fg : tform_graph;
begin
  fg := tab_form_graph[i_form_graph];
  if ( s = '' ) then
    if ( fg.xscale ) then
      begin
        fg.xscale := false;
        iwriteln('Xscale OFF (graphic window #' + IntToStr(i_form_graph) + ')');
        exit;
      end
    else
  else
    begin
      pos := position(s,' ');
      if ( pos = 0 ) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      if not est_reel(s1,a) or not est_reel(s2,b) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      fg.xmin := min(a,b);
      fg.xmax := max(a,b);
    end;
  fg.xscale := true;
  iwriteln('Xscale ON  [' + FloatToStr(fg.xmin) + ', '
                          + FloatToStr(fg.xmax) +
           '] (graphic window #' + IntToStr(i_form_graph) + ')');
end;

procedure interp_yscale(s : string);
var  pos : integer;
     s1,s2 : string;
     a,b : extended;
     fg : tform_graph;
begin
  fg := tab_form_graph[i_form_graph];
  if ( s = '' ) then
    if ( fg.yscale ) then
      begin
        fg.yscale := false;
        iwriteln('Yscale OFF (graphic window #' + IntToStr(i_form_graph) + ')');
        exit;
      end
    else
  else
    begin
      pos := position(s,' ');
      if ( pos = 0 ) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      if not est_reel(s1,a) or not est_reel(s2,b) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      fg.ymin := min(a,b);
      fg.ymax := max(a,b);
    end;
  fg.yscale := true;
  iwriteln('Yscale ON  [' + FloatToStr(fg.ymin) + ', '
                          + FloatToStr(fg.ymax) +
           '] (graphic window #' + IntToStr(i_form_graph) + ')');
end;

procedure interp_line(s : string);
var col : integer;
begin
  with tab_form_graph[i_form_graph] do
    begin
      if ( s = '' ) then
        line0 := not line0
      else
        if est_entier(s,col) then
          begin
            if ( col < 1 ) or ( col > maxcolors ) then
              col := col mod maxcolors + 1;
            vargraph_y_col[1] := colors[col];
            line0 := true;
          end
        else
          begin
            erreur_interp('positive integer expected');
            exit;
          end;
      if line0 then
        iwriteln('Line ON (graphic window #' + IntToStr(i_form_graph) + ')')
      else
        iwriteln('Line OFF (graphic window #' + IntToStr(i_form_graph) + ')');
    end;
end;

procedure interp_savegraph(s : string);
begin
  with tab_form_graph[i_form_graph] do
    begin
      nomficgraph := s;
      file_saveExecute2(nil); {iiiii}
    end;
end;

procedure interp_distrib(s : string);
var delta : extended;
begin
  with tab_form_graph[i_form_graph] do
    begin
      if ( s = '') then
        if distrib then
          distrib := false
        else
          begin
            distrib := true;
            d_distrib := 1.0;
          end
      else
        if est_reel(s,delta) then
          if ( delta > 0.0 ) then
            begin
              distrib := true;
              d_distrib := delta;
            end
          else
            begin
              erreur_interp('positive real number expected');
              exit;
            end
        else
          begin
            erreur_interp('positive real number expected');
            exit;
          end;
      if distrib then
        iwriteln('Distribution mode ON')
      else
        iwriteln('Distribution mode OFF');
      status_distrib;
    end;
end;

{ ------ autres ------ }

procedure interp_comment(s : string);
begin
  iwriteln(s);
end;

procedure interp_tempo(s : string);
var sec,nms,c : integer;
begin
  if ( s = '' ) then
    exit
  else
    if est_entier(s,sec) then
      begin
        nms := sec*1000;
        c := clock;
        while (clock - c) < nms do;
      end
    else
      erreur_interp('integer expected');
end;

function  traduc(s : string) : char;
begin
  if ( s = 'a' ) then traduc := 'a'
  else
  if ( s = 'b' ) then traduc := 'b'
  else
  if ( s = 'change' ) or ( s = 'changevar' ) or ( s = 'c' ) then traduc := 'c'
  else
  if ( s = 'clear' ) or ( s = 'e' ) then traduc := 'e'
  else
  if ( s = 'file' ) or ( s = 'f' ) then traduc := 'f'
  else
  if ( s = 'graph' ) or ( s = 'g' ) then traduc := 'g'
  else
  if ( s = 'help' ) or ( s = 'h' ) or ( s = '?' ) then traduc := 'h'
  else
  if ( s = 'init' ) or ( s = 'i' ) then traduc := 'i'
  else
  if ( s = 'line' ) or ( s = 'l' ) then traduc := 'l'
  else
  if ( s = 'montecarlo' ) or ( s = 'm' ) then traduc := 'm'
  else
  if ( s = 'new' ) or ( s = 'newvar' ) or ( s = 'n' ) then traduc := 'n'
  else
  if ( s = 'run' ) or ( s = 'r' ) then traduc := 'r'
  else
  if ( s = 'text' ) or ( s = 't' ) then traduc := 't'
  else
  if ( s = 'distribution' ) or ( s = 'distrib' ) or ( s = 'u' ) then traduc := 'u'
  else
  if ( s = 'view' ) or ( s = 'v' ) then traduc := 'v'
  else
  if ( s = 'window' ) or ( s = 'w' ) then traduc := 'w'
  else
  if ( s = 'xscale' ) or ( s = 'x' ) then traduc := 'x'
  else
  if ( s = 'yscale' ) or ( s = 'y' ) then traduc := 'y'
  else
  if ( s = 'add' ) or ( s = 'addgraph' ) or ( s = '+' ) then traduc := '+'
  else
  if ( s = '_' ) then traduc := '_'
  else
  if ( s = 'save' ) or ( s = 'savegraph' ) or ( s = '!' ) then traduc := '!'
  else
  if ( s = '{' )  then traduc := '{'
  else
  if ( s = '~' )  then traduc := '~'
  else
    traduc := '=';
end;

procedure interp_help(s : string);
var c : char;
begin
  if ( s = '' ) then
    begin
      iwriteln('COMMANDS:');
      iwriteln('Help  Text  View');
      iwriteln('Run  Montecarlo  Init');
      iwriteln('Changevar  Newvar');
      iwriteln('Graph  addgraph  savegraph  File');
      iwriteln('clEar  Line  distribUtion');
      iwriteln('Window  Xscale  Yscale'#13#10);
      iwriteln('BINARY OPERATORS:');
      iwriteln('  +      -     *     /      ^');
      iwriteln('  >      <     \     @'#13#10);
      iwriteln('UNARY OPERATORS:');
      iwriteln('  -      sqrt  abs   trunc   round');
      iwriteln('  sin    asin  cos   acos    tan    atan');
      iwriteln('  ln     ln0   log   exp     fact');
      iwriteln('  gauss  rand  ber   gamm    poisson');
      iwriteln('  geom   expo'#13#10);
      iwriteln('FUNCTIONS:');
      iwriteln('  if      min        max');
      iwriteln('  gaussf  lognormf   binomf  poissonf');
      iwriteln('  nbinomf nbinom1f');
      iwriteln('  betaf   beta1f     bicof   tabf');
      iwriteln('  gratef  bdf'#13#10);
      iwriteln('GROUP FUNCTIONS:');
      iwriteln('  groupsumf   groupsum1f');
      iwriteln('  groupmeanf');
      iwriteln('  groupmaxf   groupminf');
      iwriteln('  groupcardf  grouppopf');
      iwriteln('  groupgrowthf  grouplifetimef');
      iwriteln('  datef  magicf');
      iwriteln('  focalf'#13#10);
      iwriteln('type help xxx for help about operator xxx');
      iwriteln('> Press Ctrl-Alt simultaneously to break execution <');
      exit;
    end;
  c := traduc(s);
  case c of
  'c'  : begin
           iwriteln('changevar or change or c');
           iwriteln('  change x exp');
           iwriteln('set existing variable x with expression expr');
         end;
  'e'  : begin
           iwriteln('clear or e');
           iwriteln('clear current graphics');
         end;
  'f'  : begin
           iwriteln('file or f (on/off)');
           iwriteln('  file file_name x1 ... xn');
           iwriteln('save values of variables x1, ..., xn');
           iwriteln('in text file with name file_name');
           iwriteln('x1, ...,xn must all belong to the same group');
           iwriteln('or to no group at all');
         end;
  'g'  : begin
           iwriteln('graph or g');
           iwriteln('  graph x y1 .. yn');
           iwriteln('plot y1(x), ..., yn(x) on the next run');
           iwriteln('plot distributions of y1, .., yn if distrib mode on');
         end;
  'h'  : begin
           iwriteln('help or h or ?  -> help');
         end;
  'i'  : begin
           iwriteln('init or i');
           iwriteln('set t = 0 and set all variables to their initial values');
           iwriteln('  init n');
           iwriteln('initialize and set the random generator seed to n');
           iwriteln('  init 1');
           iwriteln('initialize and back to the default seed');
         end;
  'l'  : begin
           iwriteln('line or l  (on/off)');
           iwriteln('  line c');
           iwriteln('plot lines between points on graphics');
           iwriteln('color #c (c = 1, ..., 16)');
         end;
  'm'  : begin
           iwriteln('montecarlo or m');
           iwriteln('  montecarlo T M');
           iwriteln('run the models for T time steps, over M trajectories');
           iwriteln('  montecarlo T M Ext Esc');
           iwriteln('trajectories such that n < Ext are declared extinct,');
           iwriteln('  but are computed (default Ext threshold = 1)');
           iwriteln('trajectories such that n > Esc are declared to escape,');
           writeln('  but are computed (default Esc threshold = 10^7)');
         end;
  'n'  : begin
           iwriteln('newvar or new or n');
           iwriteln('  newvar x expr');
           iwriteln('create new variable x with expression expr');
         end;
  'r'  : begin
           iwriteln('run or r');
           iwriteln('  run T ');
           iwriteln('run the models for T time steps');
           iwriteln('  run T S');
           iwriteln('run the models  for T time steps,');
           iwriteln('  with output every S time steps');
         end;
  't'  : begin
           iwriteln('text or t  (on/off)');
           iwriteln('  text x1 .. xn');
           iwriteln('display values of variables x1, ..., xn');
         end;
  'u'  : begin
           iwriteln('distribution or distrib or u  (on/off)');
           iwriteln('  distrib  D');
           iwriteln('distribution mode for graphics');
           iwriteln('plot distributions of graphic variables (command graph)');
           iwriteln(' D*j <= x < D*(j+1) (default D = 1)');
         end;
  'v'  : begin
           iwriteln('view or v');
           iwriteln('  view');
           iwriteln('display all objects');
           iwriteln('  view x1 ... xn');
           iwriteln('display objects x1, ..., xn,');
         end;
  'w'  : begin
           iwriteln('window or w');
           iwriteln('  window i');
           iwriteln('create and select graphic window #i');
           iwriteln('next graph commands will apply to this window');
           iwriteln('-> graph xscale yscale addgraph clear distrib savegraph');
         end;
  'x'  : begin
           iwriteln('xscale or x  (on/off)');
           iwriteln('  xscale min max');
           iwriteln('set bounds on X axis between min and max');
         end;
  'y'  : begin
           iwriteln('yscale or y  (on/off)');
           iwriteln('  yscale min max');
           iwriteln('set bounds on Y axis between min and max');
         end;
  '+'  : begin
           iwriteln('addgraph or add or +  (on/off)');
           iwriteln('plot next graph without erasing the previous one');
         end;
  '!'  : begin
           iwriteln('save or savegraph or !');
           iwriteln('  save xxx.bmp');
           iwriteln('store graphics in file xxx.bmp');
         end;
   else
     begin
       if (s = '+') or (s = '-') or (s = '*') or (s = '/') or (s = '^') then
         begin
           iwriteln('a + b  sum of a and b');
           iwriteln('a - b  difference of a and b');
           iwriteln('a * b  product of a and b');
           iwriteln('a / b  quotient of a and b');
           iwriteln('a ^ b  a power b');
           iwriteln('-a     opposite of a');
         end
       else
       if (s = '\') then
         begin
           iwriteln('a \ b  real remainder');
           iwriteln('  7.4 \ 2 = 1.4');
         end
       else
       if (s = '<') or (s = '>' ) then
         begin
           iwriteln('a < b  comparison');
           iwriteln('  a < b = 1 if a < b and 0 otherwise');
           iwriteln('a > b  comparison');
           iwriteln('  a > b = 1 if a > b and 0 otherwise');
           iwriteln('  2 < 2 + 3 = 3; 2 < (2 + 3) = 1');
         end
       else
       if (s = '@') then
         begin
           iwriteln('x @ n  sum of n samples of x');
           iwriteln('  poisson(f)@n = poissonf(n,f)');
           iwriteln('  ber(p)@n = binomf(n,p)');
         end
       else
       if (s = 'sqrt') then
         begin
           iwriteln('sqrt(a)');
           iwriteln('  square root of a');
           iwriteln('  domain: a >= 0');
         end
       else
       if (s = 'abs') then
         begin
           iwriteln('abs(a)');
           iwriteln('  absolute value of a');
           iwriteln('  abs(1.5) = 1.5; abs(-2.1) = 2.1');
         end
       else
       if (s = 'trunc') then
         begin
           iwriteln('trunc(a)');
           iwriteln('  integer part of a');
           iwriteln('  trunc(1.2)  =  1; trunc(1.9)  =  1');
           iwriteln('  trunc(-1.2) = -2; trunc(-1.9) = -2');
         end
       else
       if (s = 'round') then
         begin
           iwriteln('round(a)');
           iwriteln('  nearest integer');
           iwriteln('  round(1.2)  =  1; round(1.9)  =  2');
           iwriteln('  round(-1.2) = -1; round(-1.9) = -2');
         end
       else
       if (s = 'sin') or (s = 'cos') or (s = 'tan') then
         begin
           iwriteln('sin(a)  sinus of a (rad)');
           iwriteln('cos(a)  cosinus of a (rad)');
           iwriteln('tan(a)  tangent of a (rad)');
         end
       else
       if (s = 'asin') or ( s = 'acos') or (s = 'atan') then
         begin
           iwriteln('asin(a)  inverse sinus of a');
           iwriteln('acos(a)  inverse cosinus of a');
           iwriteln('atan(a)  inverse tangent of a');
         end
       else
       if (s = 'ln') or (s = 'ln0') or (s = 'log') or (s = 'exp') or (s = 'fact') then
         begin
           iwriteln('ln(a)');
           iwriteln('  neperian logarithm of a');
           iwriteln('  domain: a > 0');
           iwriteln('ln0(a) = if a > 0 then ln(a) else 0');
           iwriteln('log(a)');
           iwriteln('  decimal logarithm of a');
           iwriteln('  domain: a > 0');
         end
       else
       if (s = 'exp') or (s = 'fact') then
         begin
           iwriteln('exp(a)  exponential of a');
           iwriteln('fact(n) = n! = factorial n');
         end
       else
       if (s = 'gauss') or (s = 'gaussf') then
         begin
           iwriteln('gauss(s)');
           iwriteln('  normal distribution with mean 0 and standard deviation s');
           iwriteln('  domain: s >= 0');
           iwriteln('gaussf(m,s)');
           iwriteln('  normal distribution with mean m and standard deviation s');
           iwriteln('  domain: s >= 0');
           iwriteln('gaussf(m,s) = m + gauss(s)');
         end
       else
       if (s = 'lognormf') then
         begin
           iwriteln('lognormf(m,s)');
           iwriteln('  lognormal distribution with mean m and standard deviation s');
           iwriteln('  domain: m, s > 0');
         end
       else
       if (s = 'rand') then
         begin
           iwriteln('rand(a)');
           iwriteln('  uniform distribution over [0, a[');
           iwriteln('  rand(-a) = rand(a)');
           iwriteln('  trunc(rand(a)) -> random integer between 0 and a-1');
         end
       else
       if (s = 'ber') then
         begin
           iwriteln('ber(p)');
           iwriteln('  bernouilli distribution');
           iwriteln('  ber(p) = 0 with probability 1-p');
           iwriteln('         = 1 with probability p');
           iwriteln('  domain: 0 <= p <= 1');
         end
       else
       if (s = 'gamm') then
         begin
           iwriteln('gamm(m)');
           iwriteln('  gamma distribution with mean m');
           iwriteln('  domain: m >= 0');
         end
       else
       if (s = 'poisson') then
         begin
           iwriteln('poisson(f)');
           iwriteln('  poisson distribution with mean f');
           iwriteln('  domain: f >= 0');
         end
       else
       if (s = 'geom') then
         begin
           iwriteln('geom(p)');
           iwriteln('  geometric distribution with parameter p');
           iwriteln('  P(X = k) = p(1-p)^k');
           iwriteln('  domain: 0 <= p <= 1');
         end
       else
       if (s = 'expo') then
         begin
           iwriteln('expo(a)');
           iwriteln('  exponential distribution with parameter a');
           iwriteln('  domain: a > 0');
         end
       else
       if (s = 'if') then
         begin
           iwriteln('if(a,b1,b2)');
           iwriteln('  conditionnal: if a <> 0 then b1 else b2');
           iwriteln('  if(t>3,1,2) = 2 for t <= 3');
           iwriteln('  if(t>3,1,2) = 1 for t >  3');
         end
       else
       if (s = 'min') or (s = 'max' ) then
         begin
           iwriteln('min(a1, ..., an)   minimum of a1, ..., an');
           iwriteln('max(a1, ..., an)   maximum of a1, ..., an');
         end
       else
       if (s = 'gratef') then
         begin
           iwriteln('gratef(x)');
           iwriteln('  growth rate of variable x');
           iwriteln('  computed as exp((ln(x(t))-ln(x(0)))/t)');
         end
       else
       if (s = 'binomf') then
         begin
           iwriteln('binomf(n,p)');
           iwriteln('  binomial distribution with parameter p');
           iwriteln('  domain: n integer >= 0, 0 <= p <= 1');
         end
       else
       if (s = 'poissonf') then
         begin
           iwriteln('poissonf(n,f)');
           iwriteln('  sum of n trials of the Poisson distribution with mean f');
           iwriteln('  domain: n integer >= 0, f >= 0');
         end
       else
       if (s = 'nbinomf') or (s = 'nbinom1f') then
         begin
           iwriteln('binomf(r,p)');
           iwriteln('  negative binomial distribution');
           iwriteln('  domain: r real > 0, 0 < p <= 1');
           iwriteln('binom1f(m,s)');
           iwriteln('  negative binomial distribution with mean m and standard deviation s');
           iwriteln('  domain: 0 < m < s^2');
         end
       else
       if (s = 'betaf') or (s = 'beta1f' ) then
         begin
           iwriteln('betaf(a,b)');
           iwriteln('  beta distribution');
           iwriteln('  domain: a > 0, b > 0');
           iwriteln('beta1f(m,s)');
           iwriteln('  beta distribution with mean m and standard deviation s');
           iwriteln('  domain: 0 < m < 1, 0 < s^2 < m(1-m)');
           iwriteln('beta, beta1f are 0 outside [0,1]');
         end
       else
       if (s = 'bicof') then
         begin
           iwriteln('bicof(n,k)');
           iwriteln('  binomial coefficient C(n,k)');
           iwriteln('  domain: n >= 0, k >= 0');
         end
       else
       if (s = 'tabf') then
         begin
           iwriteln('tabf(p0, ...,pn)');
           iwriteln('  tabulated integer distribution');
           iwriteln('  domain: 0 <= pk <= 1, p0 + ... + pn = 1');
           iwriteln('  return k with probability pk');
           iwriteln('  tabf(0.4,0.6) = ber(0.6)');
         end
       else
       if (s = 'bdf') then
         begin
           iwriteln('bdf(n,b,d,delta)');
           iwriteln('  sum of n trials of special geom distribution');
           iwriteln('  domain: n integer >= 0, b,d,delta >= 0');
           iwriteln('  b = birth rate; d = death rate; delta = time step');
           iwriteln('  used for discrete time version of continuous time birth-death process');
         end
       else
       if (s = 'groupsumf') then
         begin
           iwriteln('groupsumf(G,expr)');
           iwriteln('  sum of values of expression expr over all individuals in group G');
           iwriteln('  groupsumf(G,x)  = sum(i=1,..,grouppop;v_i*nb_i)');
         end
       else
       if (s = 'groupsum1f' ) then
         begin
           iwriteln('groupsum1f(G,expr)');
           iwriteln('  sum of values of expression expr');
           iwriteln('  over all phenotypes in group G');
           iwriteln('  groupsum1f(G,x) = sum(i=1,..,groupcard;v_i)');
         end
       else
       if (s = 'grouppopf' ) then
         begin
           iwriteln('grouppopf(G)');
           iwriteln('  number of individuals in group G');
         end
       else
       if (s = 'groupcardf' ) then
         begin
           iwriteln('groupcardf(G)');
           iwriteln('  number of phenotypes in group G');
         end
       else
       if (s = 'groupgrowthf' ) then
         begin
           iwriteln('groupgrowthf(G)');
           iwriteln('  growth rate of phenotypes in group G');
         end
       else
       if (s = 'grouplifetimef' ) then
         begin
           iwriteln('grouppopf(G)');
           iwriteln('  lifetime duration of phenotypes in group G');
         end
       else
       if (s = 'groupmaxf' ) then
         begin
           iwriteln('groupmaxf(x)');
           iwriteln('  maximum value of group variable x across phenotypes in group');
           iwriteln('  groupmaxf(x) = max(i=1,..,groupcard;x_i)');
         end
       else
       if (s = 'groupminf' ) then
         begin
           iwriteln('groupminf(x)');
           iwriteln('  minimum value of group variable x across phenotypes in group');
           iwriteln('  groupminf(x) = min(i=1,..,groupcard;x_i)');
         end
       else
       if (s = 'groupmeanf' ) then
         begin
           iwriteln('groupmeanf(x)');
           iwriteln('  mean value of group variable x across phenotypes in group');
           iwriteln('  groupmeanf(x) = (1/groupcard)*sum(i=1,..,groupcard;x_i*nb_i)');
         end
       else
       if (s = 'datef' ) then
         begin
           iwriteln('datef(x)');
           iwriteln('  date of creation of phenotype');
           iwriteln('  x is any variable pertaining to the group of interest');
         end
       else
       if (s = 'focalf' ) then
         begin
           iwriteln('focalf(x)');
           iwriteln('  value of group variable x from a focal phenotype in group');
         end
       else
       if (s = 'magicf' ) then
         begin
           iwriteln('x = magicf(xa, ...,xz)');
           iwriteln('  ensures that variable x is updated just after variables xa, ..., xz');
         end
     end;
  end;
end;

procedure interp(s : string);
var  pos : integer;
     c : char;
     s1,s2 : string;
begin
  s := tronque(minuscule(s));
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos > 0 ) then
    begin
      coupe(s,pos,s1,s2);
      c := traduc(s1);
      s := tronque(s2);
    end
  else
    begin
      c := traduc(s);
      s := '';
    end;
  case c of
  'c'   : interp_chg_variable(s);
  'e'   : interp_efface;
  'f'   : interp_fic(s);
  'g'   : interp_graph(s);
  'h'   : interp_help(s);
  'i'   : interp_init(s);
  'l'   : interp_line(s);
  'n'   : interp_newvar(s);
  'm'   : interp_run_carlo(s);
  'r'   : interp_run(s);
  't'   : interp_texte(s);
  'u'   : interp_distrib(s);
  'v'   : interp_voir(s);
  'w'   : interp_graph_window(s);
  'x'   : interp_xscale(s);
  'y'   : interp_yscale(s);
  '+'   : interp_addgraph;
  '_'   : interp_info;
  '!'   : interp_savegraph(s);
  '{'   : interp_comment(s);
  '~'   : interp_tempo(s);
  else;
  end;
end;

end.

