unit fgraphset;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls,
  fgraph;

type
  Tform_graphset = class(TForm)
    colordialog: TColorDialog;
    panel1: TPanel;
    run_box: TGroupBox;
    vargraph_x_edit: TEdit;
    x: TLabel;
    vargraph_y1_edit: TEdit;
    vargraph_y2_edit: TEdit;
    vargraph_y3_edit: TEdit;
    vargraph_y4_edit: TEdit;
    y1: TLabel;
    y2: TLabel;
    y3: TLabel;
    y4: TLabel;
    color_y1_button: TButton;
    color_y2_button: TButton;
    color_y3_button: TButton;
    color_y4_button: TButton;
    GroupBox2: TGroupBox;
    distrib0_check: TCheckBox;
    d_distrib_edit: TEdit;
    d_distrib_label: TLabel;
    distrib_check: TCheckBox;
    carlo_box: TGroupBox;
    minmax_check: TCheckBox;
    sigma_check: TCheckBox;
    axis_box: TGroupBox;
    xmin_edit: TEdit;
    ymin_edit: TEdit;
    xmin: TLabel;
    ymin: TLabel;
    xmax_edit: TEdit;
    ymax_edit: TEdit;
    xmax: TLabel;
    ymax: TLabel;
    bordoff_check: TCheckBox;
    xscale_check: TCheckBox;
    yscale_check: TCheckBox;
    GroupBox1: TGroupBox;
    lineoff_check: TCheckBox;
    gplus_check: TCheckBox;
    black_and_white_check: TCheckBox;
    white_and_black_check: TCheckBox;
    coeff_color_edit: TEdit;
    coeff_color_label: TLabel;
    ok_button: TButton;
    cancel_button: TButton;
    apply_button: TButton;
    dt_update_edit: TEdit;
    dt_update_label: TLabel;
    spec_check: TCheckBox;
    procedure cancel_buttonClick(Sender: TObject);
    procedure ok_buttonClick(Sender: TObject);
    procedure apply_buttonClick(Sender: TObject);
    procedure color_y1_buttonClick(Sender: TObject);
    procedure color_y2_buttonClick(Sender: TObject);
    procedure color_y3_buttonClick(Sender: TObject);
    procedure color_y4_buttonClick(  Sender: TObject);
    procedure black_and_white_checkClick(Sender: TObject);
    procedure white_and_black_checkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    err_graphset : boolean;
    procedure erreur(s : string);
  public
    fg : tform_graph;
  end;

var  form_graphset: tform_graphset;

implementation

uses iglobvar,iutil,isymb,isyntax;

{$R *.lfm}

procedure tform_graphset.erreur(s : string);
begin
  erreur_('Graph Settings - ' + s);
  err_graphset := true;
end;

procedure Tform_graphset.cancel_buttonClick(Sender: TObject);
begin
  Close;
end;

function  test_var(s : string;var x : integer) : boolean;
var nom : integer;
begin
  test_var := false;
  x := 0;
  if ( s = '' ) then exit;
  s := tronque(minuscule(s));
  nom := trouve_dic(s);
  if ( nom = 0 ) then exit;
  x := trouve_variable(nom);
  if ( x <> 0 ) then test_var := true;
end;

procedure Tform_graphset.ok_buttonClick(Sender: TObject);
begin
  apply_buttonClick(nil);
  if not err_graphset then Close;
end;

procedure Tform_graphset.apply_buttonClick(Sender: TObject);
var i,k : integer;
    vargraph_xa : integer;
    vargraph_ya : array[1..4] of integer;
    d_distriba  : extended;
    xmina,xmaxa,ymina,ymaxa,coeff_aeca : extended;
    distriba,distrib0a : boolean;
    xscalea,yscalea : boolean;
    dt_updatea : integer;
begin
  err_graphset := false;
  with fg do
    begin
      if ( vargraph_x_edit.Text = '' ) then
        begin
          erreur('X-variable expected');
          exit;
        end
      else
        if test_var(vargraph_x_edit.Text,vargraph_xa) then
        else
          begin
            erreur('X-variable unknown');
            exit;
          end;
      if ( vargraph_y1_edit.Text = '' ) then
        vargraph_ya[1] := 0
      else
        if test_var(vargraph_y1_edit.Text,vargraph_ya[1]) then
        else
          begin
            erreur('Y1-variable unknown');
            exit;
          end;
      if ( vargraph_y2_edit.Text = '' ) then
        vargraph_ya[2] := 0
      else
        if test_var(vargraph_y2_edit.Text,vargraph_ya[2]) then
        else
          begin
            erreur('Y2-variable unknown');
            exit;
          end;
      if ( vargraph_y3_edit.Text = '' ) then
        vargraph_ya[3] := 0
      else
        if test_var(vargraph_y3_edit.Text,vargraph_ya[3]) then
        else
          begin
            erreur('Y3-variable unknown');
            exit;
          end;
      if ( vargraph_y4_edit.Text = '' ) then
        vargraph_ya[4] := 0
      else
        if test_var(vargraph_y4_edit.Text,vargraph_ya[4]) then
        else
          begin
            erreur('Y4-variable unknown');
            exit;
          end;

      if distrib_check.State = cbChecked then
        if est_reel(d_distrib_edit.Text,d_distriba) then
          distriba := true
        else
          begin
            erreur('Delta_distrib: real value expected');
            exit;
          end
      else
        distriba := false;
      if distrib0_check.State = cbChecked then
        if est_reel(d_distrib_edit.Text,d_distriba) then
          distrib0a := true
        else
          begin
            erreur('Delta_distrib: real value expected');
            exit;
          end
      else
        distrib0a := false;

      if xscale_check.State = cbChecked then
        begin
          if not est_reel(xmin_edit.Text,xmina) then
            begin
              erreur('Xmin: real value expected');
              exit;
            end;
          if not est_reel(xmax_edit.Text,xmaxa) then
            begin
              erreur('Xmax: real value expected');
              exit;
            end;
          if ( xmina >= xmaxa ) then
            begin
              erreur('Xscale: Xmin >= Xmax');
              exit;
            end;
          xscalea := true;
        end
      else
        xscalea := false;
      if yscale_check.state = cbChecked then
        begin
          if not est_reel(ymin_edit.Text,ymina) then
            begin
              erreur('Ymin: real value expected');
              exit;
            end;
          if not est_reel(ymax_edit.Text,ymaxa) then
            begin
              erreur('Ymax: real value expected');
              exit;
            end;
          if ( ymina >= ymaxa ) then
            begin
              erreur('Yscale: Ymin >= Ymax');
              exit;
            end;
          yscalea := true;
        end
      else
        yscalea := false;

      for i := 1 to 3 do
        if ( vargraph_ya[i] = 0 ) and ( vargraph_ya[i+1] <> 0 ) then
          begin
            vargraph_ya[i] := vargraph_ya[i+1];
            vargraph_ya[i+1] := 0;
          end;
      k := 0;
      for i := 1 to 4 do
        if ( vargraph_ya[i] <> 0 ) then k := k+1;
      if k = 0 then
        begin
          erreur('Y-variable expected');
          exit;
        end;
      nb_vargraph_y := k;
      for i := 1 to maxvargraph do vargraph_y[i] := vargraph_ya[i];
      vargraph_x := vargraph_xa;

      vargraph_y1_edit.Text := s_ecri_var(vargraph_y[1]);
      if vargraph_y[2] <> 0 then
        vargraph_y2_edit.Text := s_ecri_var(vargraph_y[2])
      else
        vargraph_y2_edit.Text := '';
      if vargraph_y[3] <> 0 then
        vargraph_y3_edit.Text := s_ecri_var(vargraph_y[3])
      else
        vargraph_y3_edit.Text := '';
      if vargraph_y[4] <> 0 then
        vargraph_y4_edit.Text := s_ecri_var(vargraph_y[4])
      else
        vargraph_y4_edit.Text := '';
      vargraph_y_col[1] := color_y1_button.Color;
      if ( vargraph_y[2] <> 0 ) then
        vargraph_y_col[2] := color_y2_button.Color
      else
        color_y2_button.Color := clBackground;
      if ( vargraph_y[3] <> 0 ) then
        vargraph_y_col[3] := color_y3_button.Color
      else
        color_y3_button.Color := clBackground;
      if ( vargraph_y[4] <> 0 ) then
        vargraph_y_col[4] := color_y4_button.Color
      else
        color_y4_button.Color := clBackground;

      if not est_reel(coeff_color_edit.Text,coeff_aeca) then
        begin
          erreur('Coeff color: real value expected');
          exit;
        end;
      if ( coeff_aeca <= 0.0 ) then
        begin
          erreur('Coeff color : positive value expected');
          exit;
        end;

      if not est_entier(dt_update_edit.Text,dt_updatea) then
        begin
          erreur('Dt update: integer value expected');
          exit;
        end;
      if ( dt_updatea < 0 ) then
        begin
          erreur('Dt update : positive value expected');
          exit;
        end;


      distrib := distriba;
      if distrib then d_distrib := d_distriba;
      distrib0 := distrib0a;
      if distrib0 then d_distrib := d_distriba;

      xscale := xscalea;
      if xscale then
        begin
          xmin := xmina;
          xmax := xmaxa;
        end;
      yscale := yscalea;
      if yscale then
        begin
          ymin := ymina;
          ymax := ymaxa;
        end;

      groupgroup := test_group;
      line0 := lineoff_check.state = cbUnchecked;
      gplus := gplus_check.state = cbChecked;
      bord  := bordoff_check.state = cbUnchecked;
      if groupgroup then
        spec := false
      else
        spec  := spec_check.state = cbchecked; {iiiii}
      black_and_white := black_and_white_check.state = cbChecked;
      white_and_black := white_and_black_check.state = cbChecked;
      gminmax := minmax_check.state = cbChecked;
      gsigma := sigma_check.state = cbChecked;
      coeff_aec := coeff_aeca;
      dt_update := dt_updatea;
      status;
    end;
end;

procedure tform_graphset.FormCreate(Sender: TObject);
begin
  Left   := 374;
  Top    := 4;
  Height := 478;
  Width  := 506;
  adjust(self);
  {with colordialog,form_graph do
    for i := 0 to 15 do
      CustomColors.Append(IntToStr(colors[i])); }
end;

procedure Tform_graphset.FormActivate(Sender: TObject);
begin
  with fg do
    begin
      form_graphset.Caption := 'GRAPHIC SETTINGS <' + IntToStr(ifg) + '>';
      vargraph_x_edit.Text  := s_ecri_var(vargraph_x);
      vargraph_y1_edit.Text := s_ecri_var(vargraph_y[1]);
      if vargraph_y[2] <> 0 then
        vargraph_y2_edit.Text := s_ecri_var(vargraph_y[2])
      else
        vargraph_y2_edit.Text := '';
      if vargraph_y[3] <> 0 then
        vargraph_y3_edit.Text := s_ecri_var(vargraph_y[3])
      else
        vargraph_y3_edit.Text := '';
      if vargraph_y[4] <> 0 then
        vargraph_y4_edit.Text := s_ecri_var(vargraph_y[4])
      else
        vargraph_y4_edit.Text := '';
      color_y1_button.Color := vargraph_y_col[1];
      if vargraph_y[2] <> 0 then
        color_y2_button.Color := vargraph_y_col[2]
      else
        color_y2_button.Color := clBackground;
      if vargraph_y[3] <> 0 then
        color_y3_button.Color := vargraph_y_col[3]
      else
        color_y3_button.Color := clBackground;
      if vargraph_y[4] <> 0 then
        color_y4_button.Color := vargraph_y_col[4]
      else
        color_y4_button.Color := clBackground;
      xmin_edit.Text := Format('%1.2f',[xmin]);
      xmax_edit.Text := Format('%1.2f',[xmax]);
      ymin_edit.Text := Format('%1.2f',[ymin]);
      ymax_edit.Text := Format('%1.2f',[ymax]);
      xscale_check.Checked := xscale;
      yscale_check.Checked := yscale;
      bordoff_check.Checked  := not bord;
      spec_check.Checked  := spec;
      distrib_check.Checked  := distrib;
      distrib0_check.Checked := distrib0;
      d_distrib_edit.Text := Format('%1.2f',[d_distrib]);
      gplus_check.Checked := gplus;
      lineoff_check.Checked := not line0;
      black_and_white_check.Checked := black_and_white;
      white_and_black_check.Checked := white_and_black;
      minmax_check.Checked := gminmax;
      sigma_check.Checked := gsigma;
      err_graphset := false;
      coeff_color_edit.Text := Format('%1.2f',[coeff_aec]);
      dt_update_edit.Text   := IntToStr(dt_update);
    end;
end;

procedure Tform_graphset.black_and_white_checkClick(Sender: TObject);
begin
  with white_and_black_check do
    if State = cbChecked then Checked := false; {State := cbUnchecked;}
end;

procedure Tform_graphset.white_and_black_checkClick(Sender: TObject);
begin
  with black_and_white_check do
    if State = cbChecked then Checked := false; {State := cbUnchecked;}
end;

procedure Tform_graphset.color_y1_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y1_button.Color := colordialog.Color;
end;

procedure Tform_graphset.color_y2_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y2_button.Color := colordialog.Color;
end;

procedure Tform_graphset.color_y3_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y3_button.Color := colordialog.Color;
end;

procedure Tform_graphset.color_y4_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y4_button.Color := colordialog.Color;
end;

end.
