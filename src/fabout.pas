unit fabout;

{$MODE Delphi}

interface

uses
  {SysUtils, Types, Classes,} Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls;

type
  tform_about = class(TForm)
    AboutPanel: TPanel;
    Version: TLabel;
    label_copyright: TLabel;
    label_comments: TLabel;
    OKButton: TButton;
    Image1: TImage;
    label_contrib: TLabel;
    label_contrib1: TLabel;
    label_funding: TLabel;
    label_contrib2: TLabel;
    label_funding1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var  form_about: tform_about;

implementation

uses iutil;

{$R *.lfm}

procedure tform_about.FormCreate(Sender: TObject);
begin
  Left   := 590;
  Top    := 33;
  Height := 336;
  Width  := 354;
  adjust(self);
  with aboutpanel do
    begin
      Font.Size := 8;
      Font.Style := [];
    end;
  with label_copyright do
    begin
      Font.Style := [fsBold];
      Caption := 'Copyright 2002 - Stéphane Legendre';
    end;
  with label_comments do
    begin
      Font.Style := [];
      Caption := 'Eco-evolutionary Team - Ecole Normale Supérieure - Paris';
    end;
  with label_contrib do
    begin
      Font.Style := [fsBold];
      Caption := 'Contribution';
    end;
  with label_contrib1 do
    begin
      Font.Style := [];
      Caption := 'Champagnat, Chapron, Clobert, Ferrière, Gauduchon';
    end;
  with label_contrib2 do
    begin
      Font.Style := [];
      Caption := 'Olivieri, Ravigné, Ronce, van Baalen';
    end;
  with label_funding do
    begin
      Font.Style := [fsBold];
      Caption := 'Project funding';
    end;
  with label_funding1 do
    begin
      Font.Style := [];
      Caption := 'ACI Bioinformatique - CNRS';
    end;
end;

end.
