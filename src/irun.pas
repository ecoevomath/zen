unit irun;

{$MODE Delphi}

{  @@@@@@   run modele   @@@@@@  }

interface

uses   fgraph;

procedure run(nb_cycle : integer);

procedure run_init_graph_group(nb_cyc : integer;fg : tform_graph);
procedure run_graph_distrib(fg : tform_graph);
procedure run_graph_group(fg : tform_graph);
procedure run_fin_graph_distrib(fg : tform_graph);
procedure run_fin_graph_group(fg : tform_graph);

implementation

uses   Sysutils,Classes,Types,
       iglobvar,
       iutil,
       isymb,
       isyntax,
       imath,
       ieval,
       iinterp,
       fzen,ftext;

{ ------  graphique  ------ }

procedure run_graph_traj(fg : tform_graph);
var x,k : integer;
begin
  with fg do
    if ( t_graph_traj <= maxgraph ) then
      begin
        valgraph_x[t_graph_traj] := variable[vargraph_x].val;
        for k := 1 to nb_vargraph_y do
          begin
            x := vargraph_y[k];
            with variable[x] do valgraph_y[k][t_graph_traj] := val
          end;
      end;
end;

procedure run_graph_distrib(fg : tform_graph);
var k,v : integer;
begin
  with fg do
    begin
      for k := 1 to nb_vargraph_y do
        begin
          v := trunc(variable[vargraph_y[k]].val/d_distrib);
          if ( v <= maxgraph ) then
            if distrib0  then
              valgraph_y[k][v] := valgraph_y[k][v] + 1.0
            else
              if ( v > 0 ) then
                valgraph_y[k][v] := valgraph_y[k][v] + 1.0;
        end;
    end;
end;

procedure run_graph_group(fg : tform_graph);
var k,x,y,z,v,w,nbx,nby : integer;
    ex,ey : extended;
begin
  with fg do
    begin
      ex := xmaxg - xming;
      ey := ymaxg - yming;
      for k := 1 to nb_vargraph_y do
        begin
          x := vargraph_y[k];
          with variable[x] do
          if ( xgroup <> 0 ) then with group[xgroup] do
            begin
              y := x_deb;
              while ( y <> 0 ) do with lis2[y] do
                begin
                  v := round(imaxtab2*(eval_var_pheno(x,y)-yming)/ey);
                  if ( v < 0 ) then v := 0;
                  if ( v > imaxtab2 ) then v := imaxtab2; {iiiii}
                  nby := imax(atom.nb,0);
                  if ( variable[vargraph_x].xgroup <> 0 ) then
                    with variable[vargraph_x],group[xgroup] do
                      begin
                        z := x_deb;
                        while ( z <> 0 ) do with lis2[z] do
                          begin
                            w := round(jmaxtab2*(eval_var_pheno(vargraph_x,z)-xming)/ex);
                            if ( w < 0 ) then w := 0;
                            if ( w > jmaxtab2 ) then w := jmaxtab2;
                            nbx := imax(atom.nb,0);
                            tab2_graph[v,w] := tab2_graph[v,w] + nbx;
                            z := suiv;
                          end;
                      end
                  else
                    begin
                      if ( vargraph_x = xtime ) then
                        w := t_graph_group
                      else
                        w := round(jmaxtab2*(variable[vargraph_x].val-xming)/ex);
                      if ( w < 0 ) then w := 0;
                      if ( w > jmaxtab2 ) then w := jmaxtab2;
                      tab2_graph[v,w] := tab2_graph[v,w] + nby;
                    end;
                  y := suiv;
                end;
            end
          else
            begin
              if ( vargraph_y[1] = xtime ) then
                v := t_graph_group
              else
                v := round(imaxtab2*(val-yming)/ey);
              if ( v < 0 ) then v := 0;
              if ( v > imaxtab2 ) then v := imaxtab2;
              with variable[vargraph_x],group[xgroup] do  {xgroup suppose <> 0 }{iiiii}
                begin
                  z := x_deb;
                  while ( z <> 0 ) do with lis2[z] do
                    begin
                      w := round(jmaxtab2*(eval_var_pheno(vargraph_x,z)-xming)/ex);
                      if ( w < 0 ) then w := 0;
                      if ( w > jmaxtab2 ) then w := jmaxtab2;
                      nbx := atom.nb;
                      tab2_graph[v,w] := tab2_graph[v,w] + nbx;
                      z := suiv;
                    end;
                  end;
            end;
        end;
    end;
end;

procedure run_init_graph_traj(fg : tform_graph);
begin
  with fg do dt_graph := (nb_cycle + maxgraph - 1) div maxgraph;
  run_graph_traj(fg);
end;

procedure run_init_graph_distrib(fg : tform_graph);
var i,k : integer;
begin
  with fg do
    begin
      for i := 0 to maxgraph do
        begin
          valgraph_x[i] := i*d_distrib;
          for k := 1 to nb_vargraph_y do valgraph_y[k][i] := 0.0;
        end;
        if distrib0 then run_graph_distrib(fg);
    end;
end;

procedure run_init_graph_group(nb_cyc : integer; fg : tform_graph);
var i,j : integer;
begin
  with fg do
    begin
      for i := 0 to imaxtab2 do
        for j := 0 to jmaxtab2 do
          tab2_graph[i,j] := 0;
      if ( vargraph_x = xtime ) then
        dt_graph := (nb_cyc + jmaxtab2 - 1) div jmaxtab2;
      if ( vargraph_y[1] = xtime ) then  {iiiii 2,3}
        dt_graph := (nb_cyc + imaxtab2 - 1) div imaxtab2;
      if not xscale then
        if ( vargraph_x = xtime ) then
          begin
            xming := t__;
            xmaxg := t__ + nb_cyc;
          end
        else
          begin
            xming := 0.0;
            xmaxg := 1.0;
          end
      else
        begin
          xming := xmin;
          xmaxg := xmax;
        end;
      if not yscale then
        if ( vargraph_y[1] = xtime ) then
          begin
            yming := t__;
            ymaxg := t__ + nb_cyc;
          end
        else
          begin
            yming := 0.0;
            ymaxg := 1.0;
          end
      else
        begin
          yming := ymin;
          ymaxg := ymax;
        end;
    end;
  run_graph_group(fg);
end;

procedure run_init_graph;
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then
        begin
          status_run(nb_cycle);
          t_graph_traj  := 0;
          t_graph_group := 0;
          if distrib then
            run_init_graph_distrib(fg)
          else
            if groupgroup then
              run_init_graph_group(nb_cycle,fg)
            else
              run_init_graph_traj(fg);
          status_dt_graph;
        end;
    end;
end;

procedure run_fin_graph_distrib(fg : tform_graph);
label 1;
var imax,i0,i,k : integer;
    a : extended;
begin
  with fg do
    begin
      if distrib0 then i0 := 0 else i0 := 1;
      imax := 1+i0;
      for i := maxgraph downto i0 do
        for k := 1 to nb_vargraph_y do
          if ( valgraph_y[k][i] <> 0 ) then
            begin
              imax := i;
              goto 1;
            end;
1:
      for k := 1 to nb_vargraph_y do
        begin
          a := 0.0;
          for i := i0 to imax do a := a + valgraph_y[k][i];
          if ( a <> 0.0 ) then
            for i := i0 to imax do
              valgraph_y[k][i] := valgraph_y[k][i]/a;
        end;
      gdistrib(imax+1);
    end;
end;

procedure run_fin_graph_group(fg : tform_graph);
begin
  with fg do
    begin
      if ( vargraph_x = xtime ) then
        gtraj_group(t_graph_group,imaxtab2)
      else
        if ( vargraph_y[1] = xtime ) then
          gtraj_group(jmaxtab2,t_graph_group)
        else
          gtraj_group(jmaxtab2,imaxtab2);
    end;
end;

procedure run_fin_graph_traj(fg : tform_graph);
begin
  with fg do if ( t_graph_traj > 0 ) then
    begin
      if not spec then
        gtraj(imin(t_graph_traj,maxgraph))
      else
        gspec(imin(t_graph_traj,maxgraph));
    end;
end;

procedure run_fin_graph;
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then
      { voir fenetre qui apparait en cours de run...}
        begin
          if not gplus then efface(nil);
          if distrib then
            run_fin_graph_distrib(fg)
          else
            if groupgroup then
              run_fin_graph_group(fg)
            else
              run_fin_graph_traj(fg);
        end;
    end;
end;

procedure run_graph(icyc,nb_cyc : integer);
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then if ( icyc mod dt_graph = 0 ) then
        begin
          t_graph_traj  := t_graph_traj  + 1;
          t_graph_group := t_graph_group + 1;
          if distrib then
            run_graph_distrib(fg)
          else
            if groupgroup then
              run_graph_group(fg)
            else
              run_graph_traj(fg);
          if ( icyc mod dt_update = 0 ) and ( icyc < nb_cyc ) then
            begin
              if not gplus then efface(nil);
              if distrib then
                run_fin_graph_distrib(fg)
              else
                if groupgroup then
                  run_fin_graph_group(fg)
                else
                  run_fin_graph_traj(fg);
            end;
        end;
    end;
end;

{ ------  texte fic  ------ }

{const maxrun = 3000;
{      maxcol = 100;
{      maxrow = 100;
{      coeff = maxrun/maxrow;
{var matab : array[-8..maxcol,0..maxrun] of extended; }
{ ? f toto.txt s }
{ ? r 3000 1     }
{ ? f toto.txt   }

procedure run_fic(icyc : integer);
var i,k,x,y : integer;
    {it,jt : integer;}
begin
  if ( icyc mod dt_texte_interp <> 0 ) then exit;
  for i := 1 to fic_nb do with fic[i] do
    if ( xgroup <> 0 ) then with group[xgroup] do
      begin
        y := x_deb;
        while ( y <> 0 ) do with lis2[y] do
          begin
            write(f,t__:6,' ',atom.nb:6);
            {it := trunc(atom.lmut[2]*64);
            jt := icyc div coeff;
            matab[it,jt] := matab[it,jt] + atom.nb;}
            for k := 1 to var_fic_nb do
              begin
                x := var_fic[k];
                write(f,' ',eval_var_pheno(x,y):1:precis[k]);
              end;
            writeln(f);
            y := suiv;
          end;
      end
    else
      begin
        write(f,t__:6);
        for k := 1 to var_fic_nb do with variable[var_fic[k]] do
          write(f,hortab,val:1:precis[k]);
        writeln(f);
      end;
end;

procedure run_init_fic;
{var it,jt : integer; }
begin
  {for it := -8 to maxcol do
    for jt := 0 to maxrun  do
      matab[it,jt] := 0.0; }
  run_fic(0);
end;

procedure run_fin_fic;
var i : integer;
    {it,jt : integer;}
begin
  {if ( fic_nb > 0 ) then
    with fic[1] do
      begin
        for it := -8 to 72 do
          for jt := 0 to maxrow  do
            matab[it,jt] := matab[it,jt]/coeff;
        writeln(f);
        for jt := 0 to maxrow do
          begin
            for it := -8 to 72 do write(f,matab[it,jt]:5:0,' ');
            writeln(f);
          end;
      end;}
  for i := 1 to fic_nb do Flush(fic[i].f);
end;

{ ------  texte interp  ------ }

procedure run_texte_interp(icyc : integer);
var j : integer;
begin
  if texte_interp then
    if ( icyc mod dt_texte_interp = 0 ) then
      begin
        iwriteln('t = ' + IntToStr(t__));
        for j := 1 to nb_vartexte_interp do
          with variable[vartexte_interp[j]] do
            if ( xgroup = 0 ) then
              iwriteln('  ' + s_ecri_dic(nom) + ' = ' + Format('%1.4f',[val]))
            else
              iwriteln('  ' + s_ecri_dic(nom) + ' -> group' + s_ecri_dic(group[xgroup].nom));
      end;
end;

procedure run_fin_texte_interp;
var j : integer;
begin
  if not texte_interp then
     begin
      iwriteln('t = ' + IntToStr(t__));
      for j := 1 to nb_vartexte_interp do
        with variable[vartexte_interp[j]] do
          if ( xgroup = 0 ) then
            iwriteln('  ' + s_ecri_dic(nom) + ' = ' + Format('%1.4f',[val]))
          else
            iwriteln('  ' + s_ecri_dic(nom) + ' -> group' + s_ecri_dic(group[xgroup].nom));
    end;
end;

{ ------  text  ------ }

procedure run_init_text;
var f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then
        begin
          t_text := t__;
          if ( t__ = 0 ) then init_text_run;
        end;
    end;
end;

procedure run_text(icyc : integer);
var f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then if ( icyc mod dt_text = 0 ) then
        begin
          t_text := t_text + 1;
          maj_text_run;
        end;
    end;
end;

procedure run_fin_text;
var f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then fin_text_run;
    end;
end;

{ ------  procedure run  ------ }

procedure run(nb_cycle : integer);
var icyc,t1 : integer;
    p1 : array[1..modele_nb_max] of extended;
    pop1,cre1,des1,tvie1 : array[1..group_nb_max] of integer;

procedure init_run;
var x : integer;
begin
  t1 := t__;
  for x := 1 to modele_nb do with modele[x] do p1[x] := pop;
  for x := 1 to group_nb do with group[x] do
    begin
      pop1[x]  := pop;
      cre1[x]  := cre;
      des1[x]  := des;
      tvie1[x] := tvie;
    end;
end;

procedure fin_run;
var x,b,d,h : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      iwriteln(s_ecri_modele(x));
      if ( t__ > 0 ) then
        begin
          a := exp((ln0(pop) - ln0(pop0))/t__);
          iwriteln(Format('  growth rate from [t = 0] -> %1.6f',[a]));
        end;
      if ( t1 > 0 ) then
        if ( t__ > t1 ) then
          begin
            a := exp((ln0(pop) - ln0(p1[x]))/(t__ - t1));
            iwriteln('  growth rate from [t = ' + IntToStr(t1) +
                     '] -> ' + Format('%1.6f',[a]));
          end;
    end;
  for x := 1 to group_nb do with group[x] do
    begin
      iwriteln(s_ecri_groupe(x));
      if ( t__ > 0 ) then
        begin
          a := exp((ln0(pop) - ln0(pop0))/t__);
          iwriteln(Format('  growth of group from [t = 0] -> %1.6f',[a]));
        end;
      if ( t1 > 0 ) then
        if ( t__ > t1 ) then
          begin
            a := exp((ln0(pop) - ln0(pop1[x]))/(t__ - t1));
            iwriteln('  growth of group from [t = ' + IntToStr(t1) +
                     '] -> ' + Format('%1.6f',[a]));
          end;
      b := cre - cre1[x];
      d := des - des1[x];
      h := tvie - tvie1[x];
      if ( cre > 0 ) then
        a := tvie/cre
      else
        a := 0.0;
      iwriteln(Format('  life duration of phenotypes from [t = 0] -> %1.4f',[a]));
      if ( b > 0 ) then
        a := h/b
      else
        a := 0.0;
      if ( t1 > 0 ) then
        iwriteln('  life duration of phenotypes from [t = ' +
                 IntToStr(t1) + '] -> ' + Format('%1.4f',[a]));
      if ( t__ > t1 ) then
        begin
          if ( b >= d ) then
            a := exp(ln0(b-d)/(t__ - t1))
          else
            a := exp(-ln(d-b)/(t__ - t1));
          iwriteln('  growth rate of phenotypes from [t = ' +
                   IntToStr(t1) + '] -> ' + Format('%1.6f',[a]));
        end;
    end; 
end;

begin
  init_run;
  run_init_text;
  run_init_graph;
  run_init_fic;
  for icyc := 1 to nb_cycle do
    begin
      run_t;
      run_graph(icyc,nb_cycle);
      run_texte_interp(icyc);
      run_text(icyc);
      run_fic(icyc);
      with form_zen do
        begin
          procproc;
          if runstop then break;
          if ( t__ mod 100 = 0 ) then status_time;
        end;
    end;
  run_fin_graph;
  run_fin_texte_interp;
  run_fin_text;
  run_fin_fic;
  fin_run;
end;

end.

