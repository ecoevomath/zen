unit fzen;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls,ComCtrls, Menus, StdActns, ActnList, ImgList,
  fgraph,ftext;

type

  { tform_zen }

  tform_zen = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    memo_interp: TMemo;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    MainMenu1: TMainMenu;
    ActionList1: TActionList;
    filenew: TAction;
    fileopen: TAction;
    filesave: TAction;
    filesaveas: TAction;
    fileexit: TAction;
    filecompile: TAction;
    helpabout: TAction;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    run_init: TAction;
    run_run: TAction;
    run_settings: TAction;
    montecarlo_run: TAction;
    montecarlo_settings: TAction;
    view_variables: TAction;
    view_all: TAction;
    view_calculator: TAction;
    graph_new: TAction;
    graph_settings: TAction;
    text_new: TAction;
    text_settings: TAction;
    ImageList1: TImageList;
    File1: TMenuItem;
    New1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    nnn1: TMenuItem;
    Exit1: TMenuItem;
    Edit2: TMenuItem;
    Cut1: TMenuItem;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Run1: TMenuItem;
    Init1: TMenuItem;
    MonteCarlo1: TMenuItem;
    Settings2: TMenuItem;
    View1: TMenuItem;
    variaBles1: TMenuItem;
    All1: TMenuItem;
    Text1: TMenuItem;
    Settings3: TMenuItem;
    Graphics1: TMenuItem;
    About1: TMenuItem;
    file_new_button: TToolButton;
    file_open_button: TToolButton;
    file_save_button: TToolButton;
    ToolButton6: TToolButton;
    cut_button: TToolButton;
    copy_button: TToolButton;
    paste_button: TToolButton;
    ToolButton10: TToolButton;
    init_button: TToolButton;
    compil_button: TToolButton;
    run_button: TToolButton;
    ToolButton14: TToolButton;
    montecarlo_button: TToolButton;
    ToolButton16: TToolButton;
    view_button: TToolButton;
    ToolButton18: TToolButton;
    text_button: TToolButton;
    Run2: TMenuItem;
    New2: TMenuItem;
    Calculator1: TMenuItem;
    Settings1: TMenuItem;
    MonteCarlo2: TMenuItem;
    Settings4: TMenuItem;
    graphnew1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Compile1: TMenuItem;
    nnn2: TMenuItem;
    About2: TMenuItem;
    ToolButton1: TToolButton;
    graph_button: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure start_model;
    procedure status_file(filename : string);
    procedure status_run(s : string);
    procedure status_t_exec(ms : integer);
    procedure status_time;
    procedure status_traj;
    procedure fileexitExecute(Sender: TObject);
    procedure filenewExecute(Sender: TObject);
    procedure fileopenExecute(Sender: TObject);
    procedure filesaveExecute(Sender: TObject);
    procedure filesaveasExecute(Sender: TObject);
    procedure filecompileExecute(Sender: TObject);
    procedure run_runExecute(Sender: TObject);
    procedure run_initExecute(Sender: TObject);
    procedure run_settingsExecute(Sender: TObject);
    procedure montecarlo_runExecute(Sender: TObject);
    procedure montecarlo_settingsExecute(Sender: TObject);
    procedure graph_settingsExecute(Sender: TObject);
    procedure graph_newExecute(Sender: TObject);
    procedure view_variablesExecute(Sender: TObject);
    procedure view_calculatorExecute(Sender: TObject);
    procedure edit1_returnpressed(Sender: TObject);
    procedure view_allExecute(Sender: TObject);
    procedure helpaboutExecute(Sender: TObject);
    procedure text_newExecute(Sender: TObject);
    procedure text_settingsExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure procproc;
    procedure FormDestroy(Sender: TObject);
    procedure Edit1DblClick(Sender: TObject);
  private
    procedure erreur(s : string);
    procedure init_defaults;
    procedure interp_commands;
    procedure set_actions0;
    procedure set_actions1;
    procedure myIdleHandler(Sender: TObject; var Done: Boolean);
  public
  end;

var   form_zen: tform_zen;

const maxform_graph = 6;
const maxform_text  = 6;

type  tab_form_graph_type = array[1..maxform_graph] of tform_graph;
type  tab_form_text_type  = array[1..maxform_text]  of tform_text;

var   tab_form_graph : tab_form_graph_type; { array of pointers to graphic windows }
      nb_form_graph  : integer; { number of active graphic windows }
      i_form_graph   : integer; { index of current active graphic window }
      tab_form_text  : tab_form_text_type; { array of pointers to output windows }
      nb_form_text   : integer; { number of active output windows }

implementation

uses iglobvar,
     isymb,
     isyntax,
     imath,
     icompil,
     ieval,
     iinterp,
     iutil,
     irun,
     icarlo,
     frunset,fgraphset,fviewvar,fcalc,fedit,fviewall,fabout,ftextset;

{$R *.lfm}

const histmax = 30;

var  history : array[1..histmax] of string;
     phist,qhist : integer;

procedure tform_zen.erreur(s : string);
begin
  s := 'Commands - ' + s;
  MessageDlg(s,mtError,[mbOk],0,mbOk);
end;

procedure tform_zen.init_defaults;
begin
  nb_cycle := 100;
  dt_texte_interp := 10;
  nb_cycle_carlo := 100;
  nb_run_carlo := 10;
  seuil_ext := 1.0;
  seuil_div := 10000000.0;
end;

procedure tform_zen.set_actions0;
begin
  run_run.Enabled  := false;
  run_init.Enabled := false;
  run_settings.Enabled := false;
  montecarlo_run.Enabled := false;
  montecarlo_settings.Enabled := false;
  view_variables.Enabled := false;
  view_all.Enabled := false;
  view_calculator.Enabled := false;
  graph_new.Enabled := false;
  graph_settings.Enabled := false;
  text_new.Enabled := false;
  text_settings.Enabled := false;
end;

procedure tform_zen.set_actions1;
begin
  run_run.Enabled  := true;
  run_init.Enabled := true;
  run_settings.Enabled := true;
  montecarlo_run.Enabled := true;
  montecarlo_settings.Enabled := true;
  view_variables.Enabled := true;
  view_all.Enabled := true;
  view_calculator.Enabled := true;
  graph_new.Enabled := true;
  graph_settings.Enabled := true;
  text_new.Enabled := true;
  text_settings.Enabled := true;
end;

procedure tform_zen.start_model;
var f : integer;
begin
  init_math; { seed = 1 }
  init_symb;
  with form_edit do
    begin
      if not Visible then Show;
      lines_compil := memo1.Lines;
    end;
  compilation;
  if compiled then
    begin
      run_initExecute(nil);
      init_texte_interp;
      for f := 1 to maxform_text do
        with tab_form_text[f] do init_text;
      for f := 1 to maxform_graph do
        with tab_form_graph[f] do init_graph;
      status_file(nomfic);
      form_edit.status(nomfic);
      with form_view_all do init_viewall;
      set_actions1;
    end;
end;

procedure tform_zen.FormCreate(Sender: TObject);
begin
  Left   := 2;
  Top    := 172;
  Height := 542;
  Width  := 426;
  resolution;
  adjust(self);
  init_math;
  init_symb;
  init_syntax;
  init_defaults;
  init_compilation;
  init_interp;
  modelfileopened := false;
  inputfileopened := false;
  outputfile      := false;
  nomfic    := '';
  nomficin  := '';
  nomficout := '';
  if ( ParamCount > 0 ) and FileExists(paramstr(1)) then
    begin
      nomfic := ParamStr(1);
      lines_compil.LoadFromFile(nomfic);
      modelfileopened := true;
    end;
  if ( ParamCount > 1 ) and FileExists(paramstr(2)) then
    begin
      nomficin := ParamStr(2);
      lines_interp.LoadFromFile(nomficin);
      inputfileopened := true;
      if ( Paramcount > 2 ) then
        begin
          nomficout := ParamStr(3);
          outputfile := true;
          AssignFile(ficout,nomficout);
          rewrite(ficout);
        end;
    end;
  set_actions0;
  Application.OnIdle:= myIdleHandler;
  KeyPreview := true;
  runstop := false;
  phist := 0;
end;

procedure tform_zen.myIdleHandler(Sender: TObject; var Done: Boolean);
begin
  {if runstop then;}
end;

procedure tform_zen.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if ( shift = ([ssAlt,ssCtrl]) ) then
    begin
      runstop := true;
      status_run('Stopped');
    end;
end;

procedure tform_zen.procproc;
begin
  Application.ProcessMessages;
end;

procedure tform_zen.interp_commands;
var i : integer;
    s : string;
begin
  start_model;
  for i := 0 to lines_interp.Count - 1 do
    begin
      s := lines_interp[i];
      iwriteln('> ' + s);
      interp(s);
    end;
  if outputfile then CloseFile(ficout);
  fileexitExecute(nil);
end;

procedure create_form_graph;
var i : integer;
begin
  nb_form_graph := 1;
  i_form_graph  := 1;
  tab_form_graph[1] := form_graph;
  for i := 2 to maxform_graph do
    tab_form_graph[i] := tform_graph.Create(nil);
  for i := 1 to maxform_graph do with tab_form_graph[i] do
    begin
      ifg := i;
      Caption := 'GRAPHICS <' + IntToStr(ifg) +'>';
      Top  := Top  - 20*i;
      Left := Left - 20*i;
    end;
end;

procedure create_form_text;
var i : integer;
begin
  nb_form_text := 1;
  tab_form_text[1] := form_text;
  for i := 2 to maxform_text do
    tab_form_text[i] := tform_text.Create(nil);
  for i := 1 to maxform_text do with tab_form_text[i] do
    begin
      ift := i;
      Caption := 'TEXT <' + IntToStr(ift) +'>';
    end;
end;

procedure tform_zen.FormShow(Sender: TObject);
begin
  create_form_graph;
  create_form_text;
  if modelfileopened then with form_edit do
    begin
      memo1.Lines := lines_compil;
      newpage(nomfic);
      status(nomfic);
      Show;
    end;
  form_graph.Show;
  if inputfileopened then interp_commands;
end;

procedure tform_zen.status_file(filename : string);
begin
  statusbar1.Panels[0].Text := ExtractFileName(filename);
end;

procedure tform_zen.status_run(s : string);
begin
  statusbar1.Panels[1].Text := s;
  iwriteln('> ' + s);
end;

procedure tform_zen.status_t_exec(ms : integer);
begin
  statusbar1.Panels[2].Text := s_ecri_t_exec(ms);
  if outputfile then iwriteln('t_exec = ' + s_ecri_t_exec(ms));
end;

procedure tform_zen.status_time;
begin
  statusbar1.Panels[3].Text := 't = ' +IntToStr(t__);
end;

procedure tform_zen.status_traj;
begin
  statusbar1.Panels[3].Text := 'traj = ' +IntToStr(traj__);
end;

procedure tform_zen.edit1_returnpressed(Sender: TObject);
var s : string;
begin
  if not compiled then exit;
  s := tronque(minuscule(edit1.Text));
  iwriteln('> ' + s);
  if ( s <> '' ) then
    begin
      phist := (phist + 1) mod (histmax + 1);
      if ( phist = 0 ) then phist := 1;
      history[phist] := s;
      qhist := phist;
    end;
  edit1.Clear;
  interp(s);
end;

procedure tform_zen.Edit1DblClick(Sender: TObject);
begin
  edit1.Text := history[qhist];
  qhist := qhist - 1;
  if ( qhist = 0 ) then qhist := histmax;{iiiii}
end;

procedure tform_zen.filenewExecute(Sender: TObject);
begin
  with form_edit do
    begin
      nomfic := 'untitled.zen';
      memo1.Clear;
      newpage(nomfic);
      if not Visible then Show;
    end;
end;

procedure tform_zen.fileopenExecute(Sender: TObject);
begin
  if opendialog1.Execute then with form_edit do
    begin
      nomfic := opendialog1.FileName;
      memo1.Lines.LoadFromFile(nomfic);
      modelfileopened := true;
      newpage(nomfic);
      if not Visible then Show;
    end;
end;

procedure tform_zen.filesaveExecute(Sender: TObject);
begin
  if ( nomfic = 'untitled.zen' ) then
    filesaveasExecute(nil)
  else
    with form_edit do
      begin
        memo1.Lines.SaveToFile(nomfic);
        TMemo(pagecontrol1.ActivePage.Controls[0]).Modified := false;
      end;
end;

procedure tform_zen.filesaveasExecute(Sender: TObject);
begin
  with savedialog1 do
    begin
      FileName := nomfic;
      InitialDir := ExtractFilePath(nomfic);
      if Execute then with form_edit do
        begin
          if FileExists(FileName) then
            if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
               mtConfirmation,[mbYes,mbNo],0) <> mrYes then exit;
          nomfic := FileName;
          memo1.Lines.SaveToFile(nomfic);
          status(nomfic);
          TMemo(pagecontrol1.ActivePage.Controls[0]).Modified := false;
          TMemo(pagecontrol1.ActivePage.Controls[0]).Hint := nomfic;
          pagecontrol1.ActivePage.Caption := ExtractFileName(nomfic);
        end;
    end;
end;

procedure tform_zen.filecompileExecute(Sender: TObject);
begin
  start_model;
end;

procedure tform_zen.run_initExecute(Sender: TObject);
begin
  init_eval;
  status_run('Init ' + IntToStr(graine0-graine00+1));
  status_time;
end;

procedure tform_zen.run_runExecute(Sender: TObject);
var ms : integer;
begin
  status_run('Run ' + IntToStr(nb_cycle));
  runstop := false;
  ms := clock;
  run(nb_cycle);
  status_t_exec(clock - ms);
  status_time;
end;

procedure tform_zen.run_settingsExecute(Sender: TObject);
begin
  form_runset.Show;
end;

procedure tform_zen.montecarlo_runExecute(Sender: TObject);
var ms : integer;
begin
  status_run('Montecarlo ' + IntToStr(nb_cycle_carlo)+ ' ' +
              IntToStr(nb_run_carlo));
  runstop := false;
  ms := clock;
  carlo(nb_cycle_carlo,nb_run_carlo,seuil_ext,seuil_div);
  status_t_exec(clock - ms);
  status_time;
end;

procedure tform_zen.montecarlo_settingsExecute(Sender: TObject);
begin
  run_settingsExecute(nil);
end;

procedure tform_zen.graph_settingsExecute(Sender: TObject);
begin
  with form_graphset do
    begin
      fg := tab_form_graph[i_form_graph];
      Show;
    end;
end;

procedure tform_zen.graph_newExecute(Sender: TObject);
var f : integer;
begin
  nb_form_graph := nb_form_graph + 1;
  if ( nb_form_graph > maxform_graph ) then
    begin
      erreur('no more than ' + IntToStr(maxform_graph) +
             ' graphic windows');
      nb_form_graph := maxform_graph;
      exit;
    end;
  for f := 1 to nb_form_graph do with tab_form_graph[f] do
    if not Visible then Show;
end;

procedure tform_zen.view_calculatorExecute(Sender: TObject);
begin
  form_calc.Show;
end;

procedure tform_zen.view_variablesExecute(Sender: TObject);
begin
  form_view_variables.Show;
end;

procedure tform_zen.view_allExecute(Sender: TObject);
begin
  form_view_all.Show;
end;

procedure tform_zen.helpaboutExecute(Sender: TObject);
begin
  form_about.ShowModal;
end;

procedure tform_zen.text_newExecute(Sender: TObject);
var f : integer;
begin
  if ( nb_form_text = maxform_text ) then
    begin
      erreur('no more than ' + IntToStr(maxform_text) +
             ' text windows');
      exit;
    end;
  for f := 1 to maxform_text do with tab_form_text[f] do
    if ( not Visible ) then
      begin
        Visible := true;
        nb_form_text := nb_form_text + 1;
        break;
      end;
end;

procedure tform_zen.text_settingsExecute(Sender: TObject);
begin
  with form_textset do
    begin
      ft := form_text;
      Show;
    end;
end;

procedure tform_zen.fileexitExecute(Sender: TObject);
begin
  Close;
end;

procedure tform_zen.FormClose(Sender: TObject; var action: TCloseAction);
var action1 : TCloseAction;
begin
  action := caFree;
  with form_edit do FormClose(nil,action1);
  if ( action1 = caNone ) then action := caNone else exit;
end;

procedure tform_zen.FormDestroy(Sender: TObject);
var i : integer;
begin
  for i := 2 to maxform_text  do tab_form_text[i].Free;
  for i := 2 to maxform_graph do tab_form_graph[i].Free;
  for i := 1 to fic_nb do CloseFile(fic[i].f);
end;

end.
