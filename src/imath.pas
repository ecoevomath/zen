unit imath;

{$MODE Delphi}


{  @@@@@@   mathematiques   @@@@@@  }


interface

uses  iglobvar;

type  vecvec_type = array of extended;

procedure init_math;

function  imin(a,b : integer) : integer;
function  imax(a,b : integer) : integer;
function  min(a,b : extended) : extended;
function  max(a,b : extended) : extended;

procedure init_alea;
function  rand(a : extended) : extended;
function  ber(a : extended) : extended;
function  gauss(m,sigma : extended) : extended;
function  lognorm(m,sigma : extended) : extended;
function  geom(p : extended) : extended;
function  exponential(a : extended) : extended;
function  betaf(a,b : extended) : extended;
function  beta1f(m,s : extended) : extended;
function  gamm(a : extended) : extended;
function  poisson(xm : extended) : extended;
function  poissonf(n : integer; m : extended) : extended; 
function  binomf(n : integer;p : extended) : extended;
function  nbinom1f(m : extended;s : extended) : extended;
function  nbinomf(r : extended;p : extended) : extended;
function  tabf(tab : rlarg_type;nb_arg : integer) : extended;
function  bdf(n : integer;b,d,delta : extended) : extended;
procedure multif(x : integer;n : integer;p : vecvec_type;var w : vecvec_type);

function  ln0(a : extended) : extended;
function  log(a : extended) : extended;
function  log0(a : extended) : extended;
function  fact(n : integer) : extended;
function  bicof(n,k : integer) : extended;

procedure spectre(n : integer;var p : vecvec_type);
procedure fft(var data : vecvec_type;nn,isign : integer);

const  graine00 = 1618;

var    graine0 : integer;
       graine  : integer;

implementation

uses {SysUtils,}iutil;

const  maxbicoftab = 50;

var    glinext,glinextp : integer;
       glma : array[1..55] of integer;      { random generator ran3 }

       gaussflip : integer;                 { loi normale }
       gaussgg   : extended;
  
       poissonoldm,poissonsq,poissonalxm,
       poissong : extended;                { loi de poisson }

       binomnold : integer;
       binompold,binomoldg,binomen,
       binompc,binomplog,binompclog : extended; { loi binomiale }

       faclntab : array[1..100] of extended; { factorielle }
       bicoftab : array[0..maxbicoftab,0..maxbicoftab] of extended;


{ ------  utilitaires  ------ }

function  min(a,b : extended) : extended;
begin
  if ( a < b ) then min := a else min := b;
end;

function imin(a,b : integer) : integer;
begin
  if ( a < b ) then imin := a else imin := b;
end;

function  max(a,b : extended) : extended;
begin
  if ( a > b ) then max := a else max := b;
end;

function imax(a,b : integer) : integer;
begin
  if ( a > b ) then imax := a else imax := b;
end;


{ ------  variables aleatoires  ------ }

function ran3(idum : integer) : extended;
{ distribution uniforme sur [0, 1] }
const mbig  = 1000000000;
      mseed = 161803398;
      mz    = 0;
      fac   = 1.0e-9;
var   i,ii,k,mj,mk : integer;
begin
  if ( idum < 0 ) then
    begin
      mj := mseed + idum;
      mj := mj mod mbig;
      glma[55] := mj;
      mk := 1;
      for i := 1 to 54 do
        begin
          ii := 21*i mod 55;
          glma[ii] := mk;
          mk := mj - mk;
          if ( mk < mz ) then mk := mk + mbig;
          mj := glma[ii];
        end;
      for k := 1 to 4 do
        for i := 1 to 55 do
          begin
            glma[i] := glma[i] - glma[1 + (i + 30) mod 55];
            if ( glma[i] < mz ) then glma[i] := glma[i] + mbig;
          end;
      glinext  := 0;
      glinextp := 31;
    end;
  glinext := glinext + 1;
  if ( glinext = 56 ) then glinext := 1;
  glinextp := glinextp + 1;
  if ( glinextp = 56 ) then glinextp := 1;
  mj := glma[glinext] - glma[glinextp];
  if ( mj < mz ) then mj := mj + mbig;
  glma[glinext] := mj;
  ran3 := mj*fac;
end;

procedure init_alea;
var r : extended;
begin
  gaussflip   := 0;
  poissonoldm := -1.0;
  binomnold   := -1;
  binompold   := -1.0;
  r := ran3(-graine);
end;

function  rand(a : extended) : extended;
{ distribution uniforme sur [0, a] }
begin
  rand := ran3(1)*a; 
end;

function  ber(a : extended) : extended;
begin
  if ( ran3(1) >= a ) then
    ber := 0.0
  else
    ber := 1.0;
end;

function  gauss0 : extended;
{ distribution gaussienne de moyenne 0 et d'ecart-type 1 }
var v1,v2,fac,r : extended;
begin
  if ( gaussflip = 0 ) then
    begin
      gaussflip := 1;
      repeat
        v1 := 2.0*ran3(1) - 1.0;
        v2 := 2.0*ran3(1) - 1.0;
        r  := sqr(v1) + sqr(v2);
      until r < 1.0;
      fac := sqrt(-2.0*ln(r)/r);
      gaussgg  := v1*fac;
      gauss0  := v2*fac;
    end
  else
    begin
      gaussflip := 0;
      gauss0 := gaussgg;
    end;
end;

function  gauss(m,sigma : extended) : extended;
{ distribution gaussienne de moyenne m et d'ecart-type sigma }
begin
  gauss := m + gauss0*sigma;
end;

function  lognorm(m,sigma : extended) : extended;
{ distribution lognormale de moyenne m et d'ecart-type sigma }
{ m, sigma > 0 }
var c : extended;
begin
  c := sigma/m;
  c := ln(c*c + 1.0);
  lognorm := exp(gauss(ln(m) - 0.5*c,sqrt(c)));
end;

function  geom(p : extended) : extended;
{ distribution geometrique de parametre p }
{ P(X = k) = p(1-p)^k, moyenne = (1-p)/p  }
{ P(X = 0) = p }
var x : extended;
begin
  if ( p = 0.0 ) or ( p = 1.0 ) then
    geom := 0.0
  else
    begin
      repeat
        x := ran3(1);
      until x > 0.0;
      geom := trunc(ln(x)/ln(1.0 - p));
    end;
end;

function  exponential(a : extended) : extended;
{ distribution exponentielle de parametre a }
{ densité f(x) = a*exp(-ax), moyenne = 1/a, variance = 1/a^2}
var u : extended;
begin
  repeat
    u := ran3(1)
  until u > 0;
  exponential := -ln(u)/a;
end;

function  gammln(xx : extended) : extended;
{ ln de la fonction gamma d'Euler }
const stp  = 2.50662827465;
var x,tmp,ser : extended;
begin
  x   := xx - 1.0;
  tmp := x + 5.5;
  tmp := (x + 0.5)*ln(tmp) - tmp;
  ser := 1.0 + 76.18009173/(x+1.0) - 86.50532033/(x+2.0) + 24.01409822/(x+3.0)
         - 1.231739516/(x+4.0) + 0.120858003e-2/(x+5.0) - 0.536382e-5/(x+6.0);
  gammln := tmp + ln(stp*ser);
end;

function  gamm(a : extended) : extended;
{ var aleatoire suivant une loi gamma de parametre a > 0 }
var am,e,s,v1,v2,x,y : extended;
begin
  if ( a > 1 ) then
    begin
      repeat
        repeat
          repeat
            v1 := 2.0*ran3(1) - 1.0;
            v2 := 2.0*ran3(1) - 1.0;
          until ( sqr(v1) + sqr(v2) <= 1.0 );
          y  := v2/v1;
          am := a - 1.0;
          s  := sqrt(2.0*am + 1.0);
          x  := s*y + am;
        until ( x > 0.0 );
        s := am*ln(x/am) - s*y;
        if ( s > -500.0 ) then
          e := (1.0 + sqr(y))*exp(s)
        else
          e := 0.0;
        {e := (1.0 + sqr(y))*exp(am*ln(x/am) - s*y); }
      until ( ran3(1) <= e );
    end
  else
    begin
      s := exp(1);
      s := s/(a+s);
      repeat
        v1 := ran3(1);
        v2 := ran3(1);
        if ( v1 < s ) then 
          begin
            x := exp(ln(v2)/a);
            e := exp(-x); 
          end
        else
          begin
            x := 1.0 - ln(v2);
            e := exp(ln(x)*(a-1.0));
          end
      until ( ran3(1) < e );
    end;
  gamm := x;
end;

function  betaf(a,b : extended) : extended;
{ var aleatoire suivant une loi beta de parametres a > 0, b > 0 }
var u,v :extended;
begin
  u := gamm(a);
  v := gamm(b);
  betaf := u/(u + v);
  {repeat
    u := exp(ln(ran3(1))/a);
    v := exp(ln(ran3(1))/b);
  until (u + v) <= 1;
  betaf := u/(u + v); }
end;

function  beta1f(m,s : extended) : extended;
{ var aleatoire suivant une loi beta de parametres a, b }
{ calcules de sorte que beta1 ait pour moyenne    m > 0 }
{                              et pour ecart_type s > 0 }
{ on doit avoir 0 < m < 1 et 0 < s^2 < m*(1-m)          }
var a,b : extended;
begin
  a := m*m*(1.0 - m)/(s*s) - m;
  b := a/m - a;
  beta1f := betaf(a,b);
end;

function  tabf(tab : rlarg_type;nb_arg : integer) : extended;
{ distribution entiere tabulee              }
{ retourne k avec probabilite pk = tab[k+1] }
{ P(X = k) = pk                             }
{ pour k = 0 a n = nb_arg - 1               }
{ 0 <= pk <= 1, p0 + ... + pn = 1           }
var i : integer;
    x,s : extended;
begin
  x := ran3(1);
  s := 0.0;
  for i := 1 to nb_arg do
    begin
      s := s + tab[i];
      if ( tab[i] <> 0.0 ) then
        if ( x <= s ) then break;
      if ( s > 1.0 ) then break;
    end;
  tabf := i-1;
end;

function  poisson(xm : extended) : extended;
{ distribution de poisson de parametre xm }
{ moyenne = xm = m, variance = m          }
{ P(X = k) = exp(-m)m^k/m!                }
var em,t,y,s : extended;
begin
  if ( xm < 12.0 ) then
    begin
      if ( xm <> poissonoldm ) then
        begin
          poissonoldm := xm;
          poissong := exp(-xm);
        end;
      em := -1.0;
      t  := 1.0;
      repeat
        em := em + 1.0;
        t  := t*ran3(1);
      until ( t <= poissong );
    end
  else
    begin
      if ( xm <> poissonoldm ) then
        begin
          poissonoldm := xm;
          poissonsq   := sqrt(2.0*xm);
          poissonalxm := ln(xm);
          poissong    := xm*poissonalxm - gammln(xm + 1.0);
        end;
      repeat
        repeat
          y  := pi*ran3(1);
          y  := sin(y)/cos(y);
          em := poissonsq*y + xm;
        until ( em >= 0.0 );
        em := trunc(em);
        s := em*poissonalxm - gammln(em + 1.0) - poissong;
        if ( s > -500.0 ) then
          t := 0.9*(1.0 + sqr(y))*exp(s)
        else
          t := 0.0;
        {t  := 0.9*(1.0 + sqr(y))*exp(em*poissonalxm - gammln(em + 1.0) - poissong);}
      until ( ran3(1) <= t );
    end;
  poisson := em;
end;

function  poissonf(n : integer; m : extended) : extended;
{ somme de n tirages indepentants selon la loi de Poisson de moyenne m }
begin
  poissonf := poisson(n*m);
end;

function  binomf(n : integer;p : extended) : extended;
{ distribution binomiale de parametres n >= 0, 0 <= p <= 1 }
{ P(X = k) = C(k,n)p^k(1-p)^(n-k) }
{ moyenne np, variance np(1-p)    }
var  j : integer;
     u,pp,am,em,g,angle,sq,t,y,s : extended;
begin
  if ( n < 25 ) then
    begin
      if ( p <= 0.5 ) then pp := p else pp := 1.0 - p;
      u := 0.0;
      for j := 1 to n do 
        if ( ran3(1) < pp ) then u := u + 1.0;
      if ( p <> pp ) then u := n - u;
      binomf := u;
    end
  else
    begin
      if ( p <= 0.5 ) then pp := p else pp := 1.0 - p;
      am := n*pp;
      if ( am < 1 ) then
        begin
          g := exp(-am);
          t := 1.0;
          j := -1;
          repeat
            j := j + 1;
            t := t*ran3(1);
          until ( t < g ) or ( j = n );
          u := j;
        end
      else
        begin
          if ( n <> binomnold ) then
            begin
              binomen   := n;
              binomoldg := gammln(binomen + 1.0);
              binomnold := n;
            end;
          if ( pp <> binompold ) then
            begin
              binompc    := 1.0 - pp;
              binomplog  := ln(pp);
              binompclog := ln(binompc);
              binompold  := pp;
            end;
          sq := sqrt(2.0*am*binompc);
          repeat
            repeat
              angle := pi*ran3(1);
              if ( angle = pisur2 ) then
                y := 1.0
              else
                y := sin(angle)/cos(angle);
              em := sq*y + am;
            until ( em >= 0.0 ) and ( em < (binomen + 1.0) );
            em := trunc(em); 
            s := binomoldg - gammln(em + 1.0)
                 - gammln(binomen - em + 1.0) + em*binomplog + (binomen-em)*binompclog;
            if ( s > -500.0 ) then
              t := 1.2*sq*(1.0 + sqr(y))*exp(s)
            else
              t := 0.0;
            {t := 1.2*sq*(1.0 + sqr(y))*exp(binomoldg - gammln(em + 1.0)
                 - gammln(binomen - em + 1.0) + em*binomplog + (binomen-em)*binompclog);}
          until ( ran3(1) <= t );
          u := em;
        end;
      if ( p <> pp ) then u := n - u;
      binomf := u;
    end;
end;

function  nbinomf(r : extended;p : extended) : extended;
{ distribution binomiale negative       }
{ de parametres r > 0, 0 < p <= 1       }
{ P(X = k) = C(k+r-1,r-1)p^r(1-p)^k     }
{ moyenne r(1-p)/p, variance r(1-p)/p^2 }
var y : extended;
begin
  y := gamm(r);
  nbinomf := poisson(y*(1.0-p)/p);
end;

function  nbinom1f(m : extended;s : extended) : extended;
{ distribution binomiale negative de moyenne m et ecart-type s }{ 0 < m < s^2 }
var p,r : extended;
begin
  p := m/(s*s);
  r := p*m/(1.0-p);
  nbinom1f := nbinomf(r,p);
end;

function  bdf(n : integer;b,d,delta : extended) : extended;
{ somme de n tirages avec loi geometrique }
{ pour discretiser un birth-death process en temps continu }
{ selon les formules d'Amaury Lambert }
{ b = birth rate; d = death rate; delta = time step }
{ b, d, delta >= 0 }
var i : integer;
    r,c,e,p0,rk,s : extended;
begin
  r  := b - d;
  if ( r < 0.0 ) then
    begin
      e  := exp(r*delta);
      c  := d - b*e;
      p0 := d*(1.0 - e)/c;
      rk := -r/c;
    end
  else
    if ( r = 0.0 ) then
      begin
        c  := 1.0 + b*delta;
        p0 := b*delta/c;
        rk := 1/c;
      end
    else
      begin
        e  := exp(-r*delta);
        c  := b - d*e;
        p0 := d*(1.0 - e)/c;
        rk := r*e/c;
     end;
  s := 0.0;
  for i := 1 to n do
    if ( ber(p0) = 0.0 ) then s := s + geom(rk) + 1.0;
  bdf := s;
end;

procedure multif(x : integer;n : integer;p : vecvec_type;var w : vecvec_type);
{ distribue aleatoirement n entiers dans w selon les probas dans p }
{ de sorte que w[0] + ... + w[n-1] = x }
{ on suppose   p[0] + ... + p[n-1] = 1 et n > 1 }
var i,x1 : integer;    sum,proba : extended;begin
  for i := 0 to n-1 do w[i] := 0.0;
  sum := 1.0;
  x1  := x;
  for i := 0 to n-2 do
    begin
      proba := p[i]/sum;
      w[i] := binomf(x1,proba);
      x1 := x1 - round(w[i]);
      if ( x1 <= 0 ) then exit;
      sum := sum - p[i];
    end;
  w[n-1] := x1;
end;

{ ------  autres fonctions mathematiques  ------ }

function  ln0(a : extended) : extended;
begin
  if ( a <= 0.0 ) then ln0 := 0.0 else ln0 := ln(a);
end;

function  log(a : extended) : extended;
begin
  log := ln(a)/ln(10.0);
end;

function  log0(a : extended) : extended;
begin
  if ( a <= 0.0 ) then log0 := 0.0 else log0 := log(a);
end;

function  facln(n : integer) : extended;
begin
  if ( n <= 99 ) then
    begin
      if ( faclntab[n+1] < 0.0 ) then
        faclntab[n+1] := gammln(n + 1.0 );
      facln := faclntab[n+1];
    end
  else
    facln := gammln(n + 1.0);
end;

function  fact(n : integer) : extended;
{ factorielle n! }
begin
  if ( n <= 0 ) then
    fact := 1.0
  else
    fact := exp(facln(n));
end;

function  bicof(n,k : integer) : extended;
{ coefficients binomiaux C(n,k) }
var r : extended;
begin
  if ( k > n ) then
    begin
      bicof := 0.0;
      exit;
    end;
  if ( n <= maxbicoftab ) and ( k <= maxbicoftab ) then
    begin
      if ( bicoftab[n,k] < 0.0 ) then
        begin
          r := exp(facln(n) - facln(k) - facln(n-k));
          if ( r < bigint ) then
            bicoftab[n,k] := round(r)
          else
            bicoftab[n,k] := r;
        end;
      bicof := bicoftab[n,k];
    end
  else
    begin
      r := exp(facln(n) - facln(k) - facln(n-k));
      if ( r < bigint ) then
        bicof := round(r)
      else
        bicof := r;
    end;
end;

{ ------   spectre ------ }

procedure fft(var data : vecvec_type;nn,isign : integer);
{ Fast Fourier Transform sur nn points }
{ nn doit etre une puissance de 2      }
{ isign = 1 -> transformee directe , isign = -1 =-> transformee inverse }
{ ref : "Numerical Recipes" pp 390-395 et pp 754-755 }
var  ii,jj,n,mmax,m,j,istep,i : integer;
     wtemp,wr,wpr,wpi,wi,theta,tempi,tempr : extended;
begin
  n := 2*nn;
  j := 1;
  for ii := 1 to nn do
    begin
      i := 2*ii - 1;
      if ( j > i ) then 
        begin
          tempr := data[j];
          tempi := data[j+1];
          data[j]   := data[i];
          data[j+1] := data[i+1];
          data[i]   := tempr;
          data[i+1] := tempi;
        end;
      m := n div 2;
      while ( m >= 2) and ( j > m ) do
        begin
          j := j - m;
          m := m div 2;
        end;
      j := j + m;
    end;
  mmax := 2;
  while ( n > mmax ) do
    begin
      istep := 2*mmax;
      theta := 2.0*pi/(isign*mmax);
      wpr := -2.0*sqr(sin(0.5*theta));
      wpi := sin(theta);
      wr  := 1.0;
      wi  := 0.0;
      for ii := 1 to ( mmax div 2 ) do
        begin
          m := 2*ii - 1;
          for jj := 0 to ( (n-m) div istep ) do
            begin
              i := m + jj*istep;
              j := i + mmax;
              tempr := wr*data[j]   - wi*data[j+1];
              tempi := wr*data[j+1] + wi*data[j];
              data[j]   := data[i]   - tempr;
              data[j+1] := data[i+1] - tempi;
              data[i]   := data[i]   + tempr;
              data[i+1] := data[i+1] + tempi;
            end;
          wtemp := wr;
          wr := wr*wpr - wi*wpi + wr;
          wi := wi*wpr + wtemp*wpi + wi;
        end;
      mmax := istep;
    end;
end;

procedure spectre(n : integer;var p : vecvec_type);
{ spectre de puissance des donnees de p sur n points }
{ spectre retourne dans le tableau p                 }
var  i,isign,nn,j,n4 : integer;
     den,facn,facp,sumw : extended;
     tab : vecvec_type;

function window(i : integer;facn,facp : extended) : extended;
begin
  window := 1.0 - abs(((i-1) - facn)*facp);  { Parzen }
end;

begin
  nn := n + n;
  n4 := nn + nn;
  facn := n - 0.5;
  facp := 1.0/(n + 0.5);
  sumw := 0.0;
  for i := 1 to nn do sumw := sumw + sqr(window(i,facn,facp));
  SetLength(tab,n4+1);
  for i := 1 to n4 do tab[i] := 0.0;
  for i := 0 to n-1  do tab[i+i+1] := p[i]*window(i,facn,facp);
  isign := 1;
  fft(tab,nn,isign);
  den := nn*sumw;
  p[0] := sqr(tab[1]) + sqr(tab[2]);
  for i := 2 to n do
    begin
      j := i + i;
      p[i-1] := sqr(tab[j]) + sqr(tab[j-1]) + sqr(tab[n4+4-j]) + sqr(tab[n4+3-j]);
    end;
  for i := 0 to n-1 do p[i] := p[i]/den;
end;

procedure init_math;
var i,j : integer;
begin
  graine0  := graine00;
  graine   := graine00;
  RandSeed := graine00;
  for i := 1 to 100 do faclntab[i] := -1.0;
  for i := 0 to maxbicoftab do
    for j := 0 to maxbicoftab do bicoftab[i,j] := -1.0;
end;

end.
