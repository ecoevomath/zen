unit ieval;

{$MODE Delphi}

{  @@@@@@   evaluateur   @@@@@@  }


interface

uses   iglobvar;


function  eval_var_pheno(x,y : integer) : extended;
function  eval(x,tx : integer) : extended;
procedure init_eval;
procedure init_eval1;
procedure run_t;

var    ordre_eval    : array[1..variable_nb_max] of integer;
       ordre_eval_nb : integer;
       err_eval : boolean;

implementation

uses   iutil,
       isymb,
       isyntax,
       imath{,
       SysUtils};

const  succ_nb_max = 50;

type   aggg = array[1..variable_nb_max] of
                record
                  succ_nb : integer;
                  succ    : array[1..succ_nb_max] of integer;
                end;

var    ggg : aggg;
       pheno_env   : integer;
       pheno_focal : integer; { phenotype en cours d'evaluation }
       var_focal   : integer; { variable en cours d'evaluation }

       
procedure erreur_eval(s : string);
begin
  s := 'Eval - ' + s;
  iwriteln(s);
  err_eval := true;
end;

function  eval_op1(op : integer;v : extended) : extended;
var w : extended;
begin
  eval_op1 := 0.0;
  case op of
    op1_moins   :  begin
                     eval_op1 := -v;
                   end;
    op1_cos     :  begin
                     eval_op1 := cos(v);
                   end;
    op1_sin     :  begin
                     eval_op1 := sin(v);
                   end;
    op1_tan     :  begin
                     w := cos(v);
                     if ( w <> 0.0 ) then
                       eval_op1 := sin(v)/w
                     else
                       begin
                         erreur_eval('operator argument: tan('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_atan    :  begin
                     eval_op1 := arctan(v);
                   end;
    op1_ln      :  begin
                     if ( v > 0.0 ) then 
                       eval_op1 := ln(v)
                     else
                       begin
                         erreur_eval('operator argument: ln('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_ln0     :  begin
                     eval_op1 := ln0(v)
                   end;
    op1_log     :  begin
                     if ( v > 0.0 ) then 
                       eval_op1 := log(v)
                     else
                       begin
                         erreur_eval('operator argument: log('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_exp     :  begin
                     eval_op1 := exp(v);
                   end;
    op1_fact    :  begin
                     eval_op1 := fact(round(v));
                   end;
    op1_sqrt    :  if ( v >= 0.0 ) then
                     eval_op1 := sqrt(v) 
                   else
                     begin
                       erreur_eval('operator argument: sqrt('+s_ecri_val(v)+')');
                       exit;
                     end;
    op1_abs     :  begin
                     eval_op1 := abs(v);
                   end;
    op1_trunc   :  begin
                     eval_op1 := trunc(v);
                   end;
    op1_round   :  begin
                     eval_op1 := round(v); 
                   end;
    op1_gauss   :  begin
                     if ( v >= 0.0 ) then
                       eval_op1 := gauss(0.0,v)
                     else
                       begin
                         erreur_eval('operator argument: gauss('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_rand    :  begin
                     eval_op1 := rand(abs(v));
                   end;
    op1_ber     :  begin
                     if ( v >= 0 ) and ( v <= 1) then
                       eval_op1 := ber(v)
                     else
                       begin
                         erreur_eval('operator argument: ber('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_gamm    :  begin
                     if ( v >= 0 ) then
                       eval_op1 := gamm(v)
                     else
                       begin
                         erreur_eval('operator argument: gamm('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_geom :  begin
                     if ( v >= 0 ) and ( v <= 1.0 ) then 
                       eval_op1 := geom(v)
                     else
                       begin
                         erreur_eval('operator argument: geom('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_expo :  begin
                     if ( v > 0 ) then
                       eval_op1 := exponential(v)
                     else
                       begin
                         erreur_eval('operator argument: expo('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    op1_poisson :  begin
                     if ( v >= 0 ) then 
                       eval_op1 := poisson(v)
                     else
                       begin
                         erreur_eval('operator argument: poisson('+s_ecri_val(v)+')');
                         exit;
                       end;
                   end;
    else iwriteln('eval_op1?');
  end;
end;

function  eval_op2(op : integer;v,w : extended) : extended;
var n : integer;
begin
  eval_op2 := 0.0;
  case op of
    op2_plus    :  eval_op2 := v + w;
    op2_mult    :  eval_op2 := v * w;
    op2_moins   :  eval_op2 := v - w;
    op2_div     :  if w <> 0.0 then 
                     eval_op2 := v / w
                   else
                     begin
                       erreur_eval('divide by 0');
                       exit;
                     end;
    op2_puis    :  if ( v = 0.0 ) then
                       eval_op2 := 0.0
                   else
                   if ( trunc(w) = w ) then
                     begin
                       n := trunc(w);
                       if ( v < 0.0 ) then 
                          if ( n mod 2 = 0 ) then
                            eval_op2 := exp(w*ln(-v))
                          else
                            eval_op2 := -exp(w*ln(-v))
                       else
                         eval_op2 := exp(w*ln(v))
                     end
                   else
                     if ( v > 0.0 ) then
                       eval_op2 := exp(w*ln(v))
                     else
                       begin
                         erreur_eval('power of a negative number: '+s_ecri_val(v));
                         exit;
                       end;
    op2_infe    :  if v < w then 
                     eval_op2 := 1.0
                   else 
                     eval_op2 := 0.0;
    op2_supe    :  if v > w then 
                     eval_op2 := 1.0
                   else 
                     eval_op2 := 0.0;
    op2_mod     :  if w <> 0.0 then
                     eval_op2 := v - w*trunc(v/w)
                   else
                     begin
                       erreur_eval('mod divide by 0');
                       exit;
                     end;
    else iwriteln('eval_op2?');
  end;
end;

function  eval_if(larg : integer) : extended;
var v : extended;
begin
  {eval_if := 0.0;}
  with lis[larg] do
    begin
      v := eval(car,car_type);
      larg := cdr;
    end;
  if ( v <> 0.0 ) then
    with lis[larg] do eval_if := eval(car,car_type)
  else
    begin
      larg := lis[larg].cdr;
      with lis[larg] do eval_if := eval(car,car_type);
    end;
end;

function  eval_min(larg : integer) : extended;
var v : extended;
begin
  {eval_min := 0.0; }
  v := maxextended;
  while ( larg <> 0 ) do with lis[larg] do
    begin
      v := min(v,eval(car,car_type));
      larg := cdr;
    end;
  eval_min := v;
end;

function  eval_max(larg : integer) : extended;
var v : extended;
begin
  {eval_max := 0.0; }
  v := -maxextended;
  while ( larg <> 0 ) do with lis[larg] do    begin
      v := max(v,eval(car,car_type));
      larg := cdr;
    end;
  eval_max := v;
end;

function  eval_gaussf(larg : integer) : extended;
var v,w : extended;
begin
  eval_gaussf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do w := eval(car,car_type);
  if ( w >= 0.0 ) then
    eval_gaussf := gauss(v,w)
  else
    erreur_eval('function argument: gaussf('+s_ecri_val(v)+')');
end;

function  eval_lognormf(larg : integer) : extended;
var v,w : extended;
begin
  eval_lognormf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0 ) then
    eval_lognormf := lognorm(v,w)
  else
    erreur_eval('function argument: lognorm('+s_ecri_val(v)+')');
end;

function  eval_binomf(larg : integer) : extended;
var v,w : extended;
begin
  eval_binomf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v >= 0.0 ) and ( w >= 0.0 ) and ( w <= 1.0 ) then
    eval_binomf := binomf(round(v),w)
  else
    erreur_eval('function argument: binomf('+s_ecri_val(v)+','+s_ecri_val(w)+')');
end;

function  eval_poissonf(larg : integer) : extended; 
var v,w : extended; 
begin 
  eval_poissonf := 0.0; 
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v >= 0.0 ) and ( w >= 0.0 ) then 
    eval_poissonf := poissonf(round(v),w) 
  else 
    erreur_eval('function argument: poissonf('+s_ecri_val(v)+','+s_ecri_val(w)+')');
end; 

function  eval_nbinomf(larg : integer) : extended;
var v,w : extended;
begin
  eval_nbinomf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0.0 ) and ( w <= 1.0 ) then
    eval_nbinomf := nbinomf(v,w)
  else
    erreur_eval('function argument: nbinomf('+s_ecri_val(v)+','+s_ecri_val(w)+')');
end;

function  eval_nbinom1f(larg : integer) : extended;
var v,w : extended;
begin
  eval_nbinom1f := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type);
  if ( v > 0.0 ) and ( v < w*w ) then
    eval_nbinom1f := nbinom1f(v,w)
  else
    erreur_eval('function argument: nbinom1f('+s_ecri_val(v)+','+s_ecri_val(w)+')');
end;

function  eval_betaf(larg : integer) : extended;
var v,w : extended;
begin
  eval_betaf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0.0 ) then
    eval_betaf := betaf(v,w)
  else
    erreur_eval('function argument: betaf('+s_ecri_val(v)+','+s_ecri_val(w)+')');
end;

function  eval_beta1f(larg : integer) : extended;
var v,w : extended;
begin
  eval_beta1f := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v > 0.0 ) and ( w > 0.0 ) and ( v < 1.0 ) and ( w*w < v*(1.0-v) ) then
    eval_beta1f := beta1f(v,w)
  else
    erreur_eval('function argument: beta1f('+s_ecri_val(v)+','+s_ecri_val(w)+')');
end;

function  eval_bicof(larg : integer) : extended;
var v,w : extended;
begin
  eval_bicof := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do w := eval(car,car_type); 
  if ( v >= 0.0 ) and ( w >= 0.0 ) then
    eval_bicof := bicof(round(v),round(w))
  else
    erreur_eval('function argument: bicof(' + s_ecri_val(v) + ',' + s_ecri_val(w) + ')');
end;

function  eval_tabf(larg : integer) : extended;
var nb_arg : integer;
    v : extended;
    tab : rlarg_type;
begin
  eval_tabf := 0.0;
  nb_arg := 0;
  while ( larg <> 0 ) do with lis[larg] do    begin
      v := eval(car,car_type);
      if ( v >= 0.0 ) and ( v <= 1.0 ) then
        begin
          nb_arg := nb_arg + 1;
          tab[nb_arg] := v;
        end
      else
        erreur_eval('function argument: tabf ' + s_ecri_val(v));
      larg := cdr;
    end;
  eval_tabf := tabf(tab,nb_arg);
end;

function  eval_bdf(larg : integer) : extended;
var v,b,d,delta : extended;
begin
  eval_bdf := 0.0;
  with lis[larg] do
    begin  
      v := eval(car,car_type); 
      larg := cdr; 
    end;
  with lis[larg] do
    begin
      b := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do
    begin
      d := eval(car,car_type);
      larg := cdr;
    end;
  with lis[larg] do delta := eval(car,car_type);
  if ( v >= 0.0 ) and ( b >= 0.0 ) and ( d >= 0.0 ) and ( delta >= 0.0 ) then
    eval_bdf := bdf(round(v),b,d,delta)
  else 
    erreur_eval('function argument: bdf(' + s_ecri_val(v) + ',' +
                 s_ecri_val(b) + ',' + s_ecri_val(d) + ',' + s_ecri_val(delta) + ')');
end;

function  eval_gratef(larg : integer) : extended;
begin
  with lis[larg] do
    if ( t__ > 0 ) then
      eval_gratef := exp((ln0(variable[car].val) - ln0(variable[car].val0))/t__)
    else
      eval_gratef := 0.0;
end;

function  eval_groupsumf(larg : integer) : extended; 
var x,y,pheno_focal_sav,pheno_env_sav : integer;
    s : extended;
begin
  with lis[larg] do 
    begin
      x := car;
      larg := cdr;
    end;
  pheno_focal_sav := pheno_focal;
  pheno_focal := pheno_env;
  s := 0.0;
  with group[x] do
    begin
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do with atom do
        begin
          pheno_env_sav := pheno_env;
          pheno_env := y;
          with lis[larg] do s := s + eval(car,car_type)*nb;
          pheno_env := pheno_env_sav;
          y := suiv;
        end;
    end;
  pheno_focal := pheno_focal_sav;
  eval_groupsumf := s;
end;

function  eval_groupsum1f(larg : integer) : extended; 
var x,y,pheno_focal_sav,pheno_env_sav : integer;
    s : extended;
begin
  with lis[larg] do
    begin
      x := car;
      larg := cdr;
    end;
  pheno_focal_sav := pheno_focal;
  pheno_focal := pheno_env;
  s := 0.0;
  with group[x] do
    begin
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do
        begin
          pheno_env_sav := pheno_env;
          pheno_env := y;
          with lis[larg] do s := s + eval(car,car_type);
          pheno_env := pheno_env_sav;
          y := suiv;
        end;
    end;
  pheno_focal := pheno_focal_sav;
  eval_groupsum1f := s;
end;

function  eval_groupmeanf(larg : integer) : extended; 
var x,y : integer;
    s : extended;
begin
  with lis[larg] do x := car; 
  s := 0.0;
  with variable[x] do with group[xgroup] do
    begin
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do with atom do
        begin
          s := s + eval_var_pheno(x,y)*nb;
          y := suiv;
        end;
      if ( pop > 0 ) then 
        s := s/pop
      else
        s := 0.0;
    end;
  eval_groupmeanf := s;
end;

function  eval_groupmaxf(larg : integer) : extended; 
var x,y : integer;
    s : extended;
begin
  with lis[larg] do x := car;
  s := -maxextended;
  with variable[x] do with group[xgroup] do
    begin
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do
        begin
          s := max(s,eval_var_pheno(x,y));
          y := suiv;
        end;
    end;
  eval_groupmaxf := s;
end;

function  eval_groupminf(larg : integer) : extended; 
var x,y : integer;
    s : extended;
begin
  with lis[larg] do x := car; 
  s := maxextended;
  with variable[x] do with group[xgroup] do
    begin
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do with atom do
        begin
          s := min(s,eval_var_pheno(x,y));
          y := suiv;
        end;
    end;
  eval_groupminf := s;
end;

function  eval_groupcardf(larg : integer) : extended; 
begin
  with lis[larg] do with group[car] do eval_groupcardf := longlis2;
end;

function  eval_grouppopf(larg : integer) : extended;
begin
  with lis[larg] do with group[car] do eval_grouppopf := pop;
end;

var cre1 : array[1..group_nb_max] of integer;

function  eval_groupgrowthf(larg : integer) : extended;
var b : extended;
begin
  with lis[larg] do with group[car] do
    begin
      if ( t__ = 0 ) then
        begin
          cre1[car] := cre; { = 1, car un seul phenotype cree au debut }
          eval_groupgrowthf := 0.0;
          exit;
        end;
      b := cre - cre1[car];
      if ( b >= des ) then
        eval_groupgrowthf := exp(ln0(b-des)/t__)
      else
        eval_groupgrowthf := exp(-ln(des-b)/t__);
    end;
end;

function  eval_grouplifetimef(larg : integer) : extended;
begin
  with lis[larg] do with group[car] do
    if ( cre > 0 ) then
        eval_grouplifetimef := tvie/cre
    else
        eval_grouplifetimef := 0.0;
end;

procedure set_var_pheno(x,y : integer;w : extended);forward;

function  eval_groupmultif(larg : integer) : extended;
{ x = groupmultif(gg,n,xtot) syntaxe obligatoire }
{ distribue la valeur de la variable globale xtot sur }
{ les P phenotypes du groupe g }
{ selon la variable n du groupe gg : n = n_i, i = 1, ..., P }
{ i.e. en proportion n_i/ntot ou ntot est la somme des n_i }
{ la somme des x_i est egale a la valeur de xtot : x = x_i, i = 1, ...,P }
var x,y,u,v,gm_xtot,gm_ntot : integer;
    p,w,s,gm_sum : extended;
begin
  x    := lis[larg].car; { gg }
  larg := lis[larg].cdr;
  v    := lis[larg].car; { n }
  larg := lis[larg].cdr;
  u    := lis[larg].car; { xtot }
  if ( variable[v].xgroup <> x ) then
    begin
      erreur_eval('groupmultif: group mismatch with variable ' + s_ecri_var(v));
      eval_groupmultif := 0.0;
      exit;
    end;
  with group[x] do
    if ( pheno_focal = x_deb ) then { tout calculer }
      begin
        s := 0.0;
        y := x_deb;
        while ( y <> 0 ) do with lis2[y] do
          begin
            s := s + eval_var_pheno(v,y);
            y := suiv;
          end;
        gm_ntot := trunc(s);
        gm_sum  := 1.0;
        gm_xtot := trunc(variable[u].val);
        y := x_deb;
        while ( y <> 0 ) do with lis2[y] do
          begin
            if ( gm_ntot = 0 ) or ( gm_xtot <= 0 ) then
              w := 0.0
            else
              if ( y = x_fin ) then
                w := gm_xtot
              else
                begin
                  p := eval_var_pheno(v,y)/gm_ntot;
                  w := binomf(gm_xtot,p/gm_sum);
                  gm_xtot := gm_xtot - trunc(w);
                  gm_sum  := gm_sum - p;
                end;
            set_var_pheno(var_focal,y,w);
            y := suiv;
          end;
      end;
  eval_groupmultif := eval_var_pheno(var_focal,pheno_focal);
end;

function  eval_datef(larg : integer) : extended;
{ l'argument ne sert qu'a indiquer le groupe }
begin
  if ( pheno_focal = 0 ) then
    begin
      erreur_eval('datef : pheno_focal = 0');
      eval_datef := 0.0;
      exit;
    end;
  with lis2[pheno_focal].atom do eval_datef := datc;
end;

function  eval_magicf(larg : integer) : extended;
{ cette fonction fait quelque chose tout en ne faisant rien }
{ c'est pour ca qu'elle est magique }
begin
  eval_magicf := 1.0;
end;

function  eval_focalf(larg : integer) : extended;
begin
  if ( pheno_focal = 0 ) then
    begin
      erreur_eval('focalf : pheno_focal = 0');
      eval_focalf := 0.0;
      exit;
    end;
  with lis[larg] do eval_focalf := eval_var_pheno(car,pheno_focal);
end;

function  eval_fun1(x,larg : integer) : extended;
var i : integer;
    val_sav : rlarg_type;
begin
  eval_fun1 := 0.0;
  with fun[x] do
    begin
      for i := 1 to nb_arg do with arg[xarg[i]] do        begin
          if ( larg = 0 ) then
            begin
              erreur_eval('missing arguments in function ' + s_ecri_fun(x));
              exit;
            end;
          val_sav[i] := val;
          with lis[larg] do 
            begin             
              val := eval(car,car_type);
              larg := cdr;
            end;
        end;
      if ( larg <> 0 ) then
        begin
          erreur_eval('too many arguments in function ' + s_ecri_fun(x));
          exit;
        end;
      eval_fun1 := eval(exp,exp_type);
      for i := 1 to nb_arg do with arg[xarg[i]] do val := val_sav[i];
    end;
end;

function  eval_fun(x,larg : integer) : extended;
begin
  if ( x > fun_nb_predef ) then
    eval_fun := eval_fun1(x,larg) 
  else
  if ( x = fun_if ) then
    eval_fun := eval_if(larg)
  else
  if ( x = fun_min ) then
    eval_fun := eval_min(larg)
  else
  if ( x = fun_max ) then
    eval_fun := eval_max(larg)
  else
  if ( x = fun_gaussf ) then
    eval_fun := eval_gaussf(larg)
  else
  if ( x = fun_lognormf ) then
    eval_fun := eval_lognormf(larg)
  else
  if ( x = fun_binomf ) then
    eval_fun := eval_binomf(larg)
  else 
  if ( x = fun_poissonf ) then
    eval_fun := eval_poissonf(larg) 
  else
  if ( x = fun_nbinomf ) then
    eval_fun := eval_nbinomf(larg)
  else
  if ( x = fun_nbinom1f ) then
    eval_fun := eval_nbinom1f(larg)
  else
  if ( x = fun_betaf ) then
    eval_fun := eval_betaf(larg)
  else
  if ( x = fun_beta1f ) then
    eval_fun := eval_beta1f(larg)
  else
  if ( x = fun_bicof ) then
    eval_fun := eval_bicof(larg)
  else
  if ( x = fun_tabf ) then
    eval_fun := eval_tabf(larg)
  else
  if ( x = fun_gratef ) then
    eval_fun := eval_gratef(larg)
  else
  if ( x = fun_bdf ) then
    eval_fun := eval_bdf(larg)
  else
  if ( x = fun_groupsumf ) then 
    eval_fun := eval_groupsumf(larg) 
  else 
  if ( x = fun_groupsum1f ) then 
    eval_fun := eval_groupsum1f(larg) 
  else 
  if ( x = fun_groupmeanf ) then 
    eval_fun := eval_groupmeanf(larg) 
  else 
  if ( x = fun_groupmaxf ) then 
    eval_fun := eval_groupmaxf(larg) 
  else 
  if ( x = fun_groupminf ) then 
    eval_fun := eval_groupminf(larg) 
  else 
  if ( x = fun_groupcardf ) then 
    eval_fun := eval_groupcardf(larg) 
  else
  if ( x = fun_grouppopf ) then
    eval_fun := eval_grouppopf(larg)
  else
  if ( x = fun_groupmultif ) then
    eval_fun := eval_groupmultif(larg)
  else
  if ( x = fun_groupgrowthf ) then
    eval_fun := eval_groupgrowthf(larg)
  else
  if ( x = fun_grouplifetimef ) then
    eval_fun := eval_grouplifetimef(larg)
  else
  if ( x = fun_datef ) then
    eval_fun := eval_datef(larg)
  else 
  if ( x = fun_magicf ) then 
    eval_fun := eval_magicf(larg)
  else 
  if ( x = fun_focalf ) then 
    eval_fun := eval_focalf(larg)
end;

function eval_convol(x,tx,y,ty : integer) : extended;
var i,n : integer;
    s : extended;
begin
  eval_convol := 0.0;
  s := eval(y,ty);
  if ( s > bigint ) then
    begin
      erreur_eval('second operand too big in operator @');
      exit;
    end;
  n := trunc(s);
  s := 0.0;
  if ( tx = type_variable ) then with variable[x] do
    for i := 1 to n do s := s + eval(exp,exp_type)
  else
    for i := 1 to n do s := s + eval(x,tx);
  eval_convol := trunc(s); 
end;

function  eval_lis(x : integer) : extended;
var op,top,z : integer;
    v,w : extended;
begin
  eval_lis := 0.0;
  if ( x = 0 ) then
    begin
      erreur_eval('empty list');
      exit;
    end;
  with lis[x] do
    begin
      op  := car;
      top := car_type;
      z   := cdr;
    end;
  case top of
  type_op1 : begin
               with lis[z] do v := eval(car,car_type);
               eval_lis := eval_op1(op,v);
             end;
  type_op2 : begin
               if ( op = op2_convol ) then 
                 begin
                   with lis[z] do
                     eval_lis := eval_convol(car,car_type,
                                 lis[cdr].car,lis[cdr].car_type);
                    exit;
                 end;
               with lis[z] do
                 begin
                   v := eval(car,car_type);
                   z := cdr;
                 end;
               with lis[z] do w := eval(car,car_type);
               eval_lis := eval_op2(op,v,w);
             end;
  type_fun : begin
               eval_lis := eval_fun(op,z);
             end;
  else iwriteln('eval_lis?');
  end;
end;

function  eval_var_pheno(x,y : integer) : extended;
begin
  eval_var_pheno := 0.0;
  if ( y = 0 ) then
    begin
      erreur_eval('group-variable ' + s_ecri_var(x) + ' out of group scope');
      exit;
    end;
  with variable[x] do with lis2[y].atom do
    begin
      if ( variable[x].xgroup <> xgroup ) then
        begin
          erreur_eval('group mismatch with variable ' + s_ecri_var(x));
          exit;
        end;
      case typvargroup of
        typvargroup_rel  : eval_var_pheno := lval[irel];
        typvargroup_reli : eval_var_pheno := lvali[ireli];
        typvargroup_mut  : eval_var_pheno := lmut[imut];
        typvargroup_loc  : eval_var_pheno := lloc[iloc];
        else;
      end;
    end;
end;

procedure set_var_pheno(x,y : integer;w : extended);
begin
  with variable[x] do with lis2[y].atom do
    case typvargroup of
      typvargroup_rel  : lval[irel]   := w;
      typvargroup_reli : lvali[ireli] := w;
      typvargroup_mut  : lmut[imut]   := w;
      typvargroup_loc  : lloc[iloc]   := w;
      else;
    end;
end;

function  eval_variable(x : integer) : extended;
begin
  with variable[x] do
    if ( xgroup = 0 ) then
      eval_variable := val
    else
      eval_variable := eval_var_pheno(x,pheno_env)
end;

function  eval(x,tx : integer) : extended;
begin
  eval := 0.0;
  case tx of
    type_lis       : eval := eval_lis(x);
    type_variable  : eval := eval_variable(x);
    type_arg       : eval := arg[x].val;
    type_ree       : eval := ree[x].val;
    else iwriteln('eval?');
  end;
end;

function  occur_var(x,tx,y : integer) : boolean;
{ regarde si la variable y a une occurence dans l'expression x  }
begin
 occur_var := false;
 case tx of
    type_lis : 
      while( x <> 0 ) do with lis[x] do
        begin
          if ( occur_var(car,car_type,y) ) then
            begin
              occur_var := true;
              exit;
            end;
           x := cdr;
        end;
    type_variable :
      occur_var := y = x;
    type_fun : 
      with fun[x] do occur_var := occur_var(exp,exp_type,y);
    else;
  end;
end;

function  occur_fun1(x : integer;f : integer) : integer;
{ regarde si la fonction f intervient dans la liste x }
{ si oui retourne la liste des arguments de f }
{ premiere occurence ! }
var z : integer;
begin
  while ( x <> 0 ) do with lis[x] do
    begin
      if ( car_type = type_fun ) and ( car = f ) then
        begin
          occur_fun1 := cdr;
          exit;
        end
      else
        if ( car_type = type_lis ) then
          begin
            z := occur_fun1(car,f);
            if ( z <> 0 ) then
              begin
                occur_fun1 := z;
                exit;
              end;
          end;
      x := cdr;
    end;
  occur_fun1 := 0;
end;

function  occur_fun1_essai(var x : integer;f : integer) : integer;
{ regarde si la fonction f intervient dans la liste x }
{ si oui retourne la liste des arguments de f }
{ et x contient la suite de la liste exploree }
var u,v,z,xx : integer;
begin
  u  := x;
  xx := 0;
  while ( u <> 0 ) do with lis[u] do
    begin
      if ( car_type = type_fun ) and ( car = f ) then
        begin
          iwriteln('1-> '+s_ecri(xx,type_lis));
          occur_fun1_essai := cdr;
          x := xx;
          exit;
        end
      else
        if ( car_type = type_lis ) then
          begin
            xx := cdr;
            iwriteln('2-> '+s_ecri(xx,type_lis));
            v := car;
            z := occur_fun1_essai(v,f);
            if ( z <> 0 ) then
              begin
                occur_fun1_essai := z;
                x := xx;
                exit;
              end;
          end;
      xx := u;
      u  := cdr;
    end;
  occur_fun1_essai := 0;
end;

procedure pre_eval1(exp : integer);
{ on ne considere que la premiere occurence dans chaque variable ...}
var z,g : integer;
begin
  z := occur_fun1(exp,fun_gratef);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function gratef');
  {z := occur_fun1(exp,fun_groupsumf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_group ) then
      erreur_eval('group name expected as first argument in function groupsumf');
  z := occur_fun1(exp,fun_groupsum1f);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_group ) then
      erreur_eval('group name expected as first argument in function groupsum1f');}
  z := occur_fun1(exp,fun_groupmeanf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function groupmeanf')
    else
      with variable[car] do if ( xgroup = 0 ) then
        erreur_eval('variable unrelated to group in function groupmeanf');
  z := occur_fun1(exp,fun_groupmaxf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function groupmaxf')
    else
      with variable[car] do if ( xgroup = 0 ) then
        erreur_eval('variable unrelated to group in function groupmaxf');
  z := occur_fun1(exp,fun_groupminf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function groupminf')
    else
      with variable[car] do if ( xgroup = 0 ) then
        erreur_eval('variable unrelated to group in function groupminf');
  z := occur_fun1(exp,fun_groupmultif);
  if ( z <> 0 ) then
    begin
      with lis[exp] do
        if ( car_type <> type_fun ) and ( car <> fun_groupmultif ) then
          erreur_eval('expected syntax: variable = groupmultif()');
        g := lis[z].car;
        {if ( lis[z].car_type <> type_group ) then
          erreur_eval('group name expected in function groupmultif');}
        z := lis[z].cdr;
        with lis[z] do
          if ( car_type <> type_variable ) then
            erreur_eval('variable name expected as second argument in function groupmultif')
          else
        with variable[car] do if ( xgroup <> g ) then
          erreur_eval('variable unrelated to group as second argument in function groupmultif');
        z := lis[z].cdr;
        with lis[z] do
          if ( car_type <> type_variable ) then
            erreur_eval('variable name expected as third argument in function groupmultif')
          else
            with variable[car] do if ( xgroup <> 0 ) then
              erreur_eval('global variable expected as third argument in function groupmultif');
      end;
  z := occur_fun1(exp,fun_magicf);
  while ( z <> 0 ) do with lis[z] do
    begin
      if ( car_type <> type_variable ) then
        erreur_eval('variable name expected in function magicf');
      z := cdr;
    end;
  z := occur_fun1(exp,fun_focalf);
  if ( z <> 0 ) then with lis[z] do
    if ( car_type <> type_variable ) then
      erreur_eval('variable name expected in function focalf')
    else
      with variable[car] do if ( xgroup = 0 ) then
        erreur_eval('variable unrelated to group in function focalf');
end;

procedure pre_eval;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_lis ) then pre_eval1(exp);
  for x := 1 to rel_nb do with rel[x] do
    if ( exp_type = type_lis ) then pre_eval1(exp);
end;

procedure eval_variable_00;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    begin
      if ( exp_type = type_inconnu ) then
        erreur_eval('variable ' + s_ecri_var(x) + ' not initialized');
      if ( exp_type = type_ree ) then
        begin
          val0 := ree[exp].val;
          val  := val0; 
        end;
    end;
end;

procedure eval_rel_00; 
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do val0 := variable[xvar].val0;
end;

procedure eval_group_00;
var x,i : integer;
    s,v : extended;
begin
  for x := 1 to group_nb do with group[x] do
    with lis2[x_deb0].atom do
      begin
        xgroup := x;
        s := 0.0;
        for i := 1 to size do
          begin
            v := rel[xrel[i]].val0;
            s := s + v;
            lval[i] := v;
          end;
        nb := round(s);
        pop0 := nb;
        for i := 1 to sizei do
          lvali[i] := rel[xreli[i]].val0;
        for i := 1 to sizemut do
          lmut[i] := variable[mut[xvarmut[i]].xvar].val0;
        for i := 1 to sizeloc do
          lloc[i] := variable[xvarloc[i]].val0; {iiiii constantes !}
      end;
end;

procedure eval_modele_00; 
var x,i : integer; 
    s : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      s := 0.0;
      for i := 1 to size do s := s + rel[xrel[i]].val0;
      pop0 := s;
    end;
end;

procedure graph_variable;
var  x,y,z,e,niv,nivmax1,nivmax2 : integer;
     tab1,tab2,tab3 : array[1..variable_nb_max] of integer;
     
procedure parc0(x : integer);
var i,j,y : integer;
begin
  if ( tab1[x] = 1 ) then exit;
  niv := niv + 1;
  tab1[x] := 1;
  tab2[niv] := x;
  with ggg[x] do
    for i := 1 to succ_nb do
      begin
        y := succ[i];
        for j := 1 to niv do
          if ( tab2[j] = y ) then
            begin
              erreur_eval('cycling definition of variable ' + s_ecri_var(x));
              exit;
            end;
        parc0(y);
      end;
  niv := niv - 1;
end;

procedure parc1(x : integer);
var i,y : integer;
begin
  niv := niv + 1;
  if ( niv > nivmax1 ) then nivmax1 := niv;
  tab1[x] := niv;
  with ggg[x] do
    for i := 1 to succ_nb do
      begin
        y := succ[i];
        if ( tab1[y] < niv ) then parc1(y);
      end;
  niv := niv - 1;
end;

procedure parc2(x : integer);
var i : integer;
begin
  niv := niv + 1;
  if ( niv > nivmax2 ) then nivmax2 := niv;
  tab2[x] := niv;
  with ggg[x] do
    for i := 1 to succ_nb do parc2(succ[i]);
  niv := niv - 1;
end;

begin

{ graphe des variables }

  for x := 1 to variable_nb do with ggg[x] do succ_nb := 0;
  for x := 1 to variable_nb do with variable[x] do
    for y := 1 to variable_nb do
      if occur_var(exp,exp_type,y) then
        with ggg[x] do
          begin
            succ_nb := succ_nb + 1;
            if (succ_nb > succ_nb_max) then
              begin
                erreur_eval('graph of variables too large');
                halt;
              end;
            succ[succ_nb] := y;
          end;{ 1. detection des cycles }

  for x := 1 to variable_nb do tab1[x] := 0;
  for x := 1 to variable_nb do
    begin
      niv := 0;
      parc0(x);
      if err_eval then exit;
    end;

{ 2. evaluer les variables selon leur hierarchie }
{ les variables des relations sont reelles, donc evaluees avant }

  nivmax1 := 0;
  for x := 1 to variable_nb do tab1[x] := 0;
  for x := 1 to variable_nb do
    begin
      niv := tab1[x];
      parc1(x);
    end;

  {for niv := nivmax1 downto 1 do
    for x := 1 to variable_nb do
      if ( tab1[x] = niv ) then
        iwriteln(s_ecri_val(niv) + ' -> ' + s_ecri_var(x)); }

{ 3. reorganisation de la hierarchie selon declenchement des mutations }

  for x := 1 to variable_nb do tab3[x] := 0;
  e := 0;
  for z := 1 to variable_nb do with variable[z] do
    if ( mut_declench <> 0 ) then 
      begin
        for x := 1 to variable_nb do tab2[x] := 0;
        nivmax2 := 0;
        niv := 0;
        parc2(z);
        for niv := nivmax2 downto 1 do 
          for x := 1 to variable_nb do with variable[x] do
            if ( exp_type <> type_ree ) then 
              if ( tab3[x] = 0 ) and ( tab2[x] = niv ) then 
                begin 
                  e := e + 1;
                  ordre_eval[e] := x;
                end;
         for x := 1 to variable_nb do tab3[x] := tab3[x] + tab2[x];
      end;
  for niv := nivmax1 downto 1 do
    for x := 1 to variable_nb do with variable[x] do
      if ( exp_type <> type_ree ) then
        if ( tab3[x] = 0 ) and ( tab1[x] = niv ) then
          begin
            e := e + 1;
            ordre_eval[e] := x;
          end;
  ordre_eval_nb := e;
end;

procedure eval_variable_0;
var e,y,u : integer;
begin
  for e := 1 to ordre_eval_nb do
    begin
      u := ordre_eval[e];
      with variable[u] do
        begin
          var_focal := u;
          if ( xgroup = 0 ) then
            begin
              val0 := eval(exp,exp_type);
              val  := val0;
            end
          else
            with group[xgroup] do
              begin
                y := x_deb;
                while ( y <> 0 ) do with lis2[y] do
                  begin
                    pheno_env   := y;
                    pheno_focal := y;
{iwriteln(s_ecri_var(ordre_eval[e]));
iwriteln(' irel  ' + s_ecri_val(irel));
iwriteln(' ireli ' + s_ecri_val(ireli));
iwriteln(' imut  ' + s_ecri_val(imut));
iwriteln(' iloc  ' + s_ecri_val(iloc)); }
                    if ( iloc <> 0 ) then with atom do
                      lloc[iloc] := eval(exp,exp_type)
                    else
                      iwriteln('eval_variable_0?');
                    y := suiv;
                  end;
                pheno_env   := 0;
                pheno_focal := 0;
              end;
          var_focal := 0;
        end;
    end;
end;

procedure eval_variable_1;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_ree ) then val := val0;
  eval_variable_0;
end;

procedure eval_rel_0; 
var x : integer; 
begin 
  for x := 1 to rel_nb do with rel[x] do val := val0; 
end; 

procedure eval_group_0;
var x,y : integer;
begin
  lis2_num := 0;
  for x := 1 to group_nb do with group[x] do
    begin
      tvie := 0;
      pop  := pop0;
      des  := 0;
      cre  := 0;
      destruc_lis2(x_deb,x_fin);
      cre_lis2(x_deb,x_fin);
      y := x_deb0; { il n'y a actuellement qu'un element dans la liste }
      while ( y <> 0 ) do with lis2[y] do
        begin
          pheno0 := lis2[y].atom;
          pheno0.anc := lis2[y].atom.num;
          cre_atom(x_deb,x_fin);
          { ce phenotype porte le num i = x, et son ancetre aussi }
          cre := cre + 1;
          y := suiv;
        end;
      longlis2 := cre;
    end;
end;

procedure eval_modele_0;
var x : integer;
begin
  for x := 1 to modele_nb do with modele[x] do pop := pop0;
end;

procedure eval_mut_0;
var x,y : integer;
begin
  for x := 1 to mut_nb do with mut[x] do
    with variable[xvar] do with group[xgroup] do
      begin
        var_focal := xvar;
        y := x_deb;
        while ( y <> 0 ) do with lis2[y] do
          begin
            pheno_focal := y;
            pheno_env   := y;
            with atom do
              ldat[imut] := round(eval(occur_exp,occur_exp_type));
            y := suiv;
          end;
      end;
  pheno_env   := 0;
  pheno_focal := 0;
  var_focal   := 0;
end;

procedure init_eval;
begin 
  init_alea;
  err_eval := false;
  pre_eval;
  if err_eval then err_eval := false; {iiiii}
  with variable[xtime] do val := 0.0;
  t__ := 0;
  pheno_env   := 0;
  pheno_focal := 0;
  var_focal   := 0;
  eval_variable_00; 
  eval_rel_00;
  eval_group_00;
  eval_modele_00;
  eval_rel_0;
  eval_group_0;
  eval_modele_0;
  graph_variable;
  eval_variable_0;
  eval_mut_0;
end;

procedure init_eval1;
begin
  init_alea;
  variable[xtime].val := 0.0;
  t__ := 0;
  pheno_env   := 0;
  pheno_focal := 0;
  var_focal   := 0;
  eval_rel_0;
  eval_group_0;
  eval_modele_0;
  eval_variable_1;
  eval_mut_0;
end; 

procedure run_modele;
var x,i : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      for i := 1 to size  do with rel[xrel[i]]  do
        begin
          var_focal := xvar;
          val := eval(exp,exp_type);
        end;
      for i := 1 to sizei do with rel[xreli[i]] do
        begin
          var_focal := xvar;
          val := eval(exp,exp_type);
        end;
    end;
  var_focal := 0;
end;

procedure run_group;
var x,i,y : integer;
begin
  for x := 1 to group_nb do with group[x] do 
    begin
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do
        begin
          pheno_focal := y;
          pheno_env   := y;
          for i := 1 to size do with rel[xrel[i]] do
            begin
              var_focal := xvar;
              with atom do lval_sav[i] := eval(exp,exp_type);
            end;
          for i := 1 to sizei do with rel[xreli[i]] do
            begin
              var_focal := xvar;
              with atom do lvali_sav[i] := eval(exp,exp_type);
            end;
          y := suiv;
        end;
    end;
  pheno_env   := 0;
  pheno_focal := 0;
  var_focal   := 0;
end;

procedure maj_group;
var x,i,y,sum : integer;
    v,a : extended;
begin
  for x := 1 to group_nb do with group[x] do
    begin
      sum := 0;
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do
        begin
          with atom do
            begin
              a := 0.0;
              for i := 1 to size do
                begin
                  v := lval_sav[i];
                  lval[i] := v;
                  a := a + v;
                end;
              nb := round(a);
              sum := sum + nb;
              for i := 1 to sizei do lvali[i] := lvali_sav[i];
            end;
          y := suiv;
        end;
      pop := sum;
    end;
end;

procedure destruc_pheno;
var x,y,z : integer;
begin
  for x := 1 to group_nb do with group[x] do
    begin
      y := x_deb;
      while ( y <> 0 ) do with lis2[y] do
        begin
          z := suiv;
          with atom do
            if ( nb = 0 ) then
              begin
                tvie := tvie + t__ - datc;
                datd := t__;
                longlis2 := longlis2 - 1;
                des := des + 1;
                destruc_atom(x_deb,x_fin,y);
              end;
          y := z;
        end;
    end;
end;

procedure maj_modele;
var x,i : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      a := 0.0;
      for i := 1 to size do with rel[xrel[i]] do        begin
          variable[xvar].val := val;
          a := a + val;
        end;
      pop := a;
      for i := 1 to sizei do with rel[xreli[i]] do variable[xvar].val := val;
    end;
end;

procedure run_mut(lm : integer);
{ lm = (m1 m2 (m3 m4) m5 ) }
{ liste des mutations déclenchees au meme moment }
{ sequentielles, paralleles }
var x,y,z,lz : integer;
 
function pheno_exist(x,z,lz : integer) : integer;
var u,lz1 : integer;
    b : boolean;
begin
  with group[x] do
    begin
      u := x_deb;
      while ( u <> 0 ) do with lis2[u] do with atom do
        begin
          b := true;
          if ( lz <> 0 ) then
            begin
              lz1 := lz;
              while ( lz1 <> 0 ) do with lis[lz1] do
                begin
                  with mut[car] do with variable[xvar] do
                    b := b and ( lmut[imut] = pheno0.lmut[imut] );
                  lz1 := cdr;
                end;
              if b then
                begin
                  pheno_exist := u;
                  exit;
                end;
            end
          else
            with mut[z] do with variable[xvar] do
              if( lmut[imut] = pheno0.lmut[imut] ) then
                begin
                  pheno_exist := u;
                  exit;
                end;
          u := suiv;
        end;
      u := m_deb;
      while ( u <> 0 ) do with lis2[u] do with atom do
        begin
          b := true;
          if ( lz <> 0 ) then
            begin
              lz1 := lz;
              while ( lz1 <> 0 ) do with lis[lz1] do
                begin
                  with mut[car] do with variable[xvar] do
                    b := b and ( lmut[imut] = pheno0.lmut[imut] );
                  lz1 := cdr;
                end;
              if b then
                begin
                  pheno_exist := u;
                  exit;
                end;
            end
          else
            with mut[z] do with variable[xvar] do
              if ( lmut[imut] = pheno0.lmut[imut] ) then
                begin
                  pheno_exist := u;
                  exit;
                end;
          u := suiv;
        end;
    end;
  pheno_exist := 0;
end;

procedure run_mut1(x,y,z,lz : integer);
{ x groupe, y phenotype, z mutation, lz liste mutations }
{ z = premier element de lz si lz <> 0 ... voir}
{ mutations independantes si lz = 0  }
{ mutations simultanees   si lz <> 0 }
var j,k,nb_mut,n,n1,n2,lz1,u,typtyp : integer;
    locloc,bb : boolean;
begin
  with group[x],mut[z] do
    begin
      with variable[xvar],lis2[y].atom do
        if ( ldat[imut] > t__ ) then exit;
      var_focal := xvar;
      nb_mut := round(eval(nbmut_exp,nbmut_exp_type));
      {iwriteln('z = '+s_ecri_val(z)+' nb_mut = ' + IntToStr(nb_mut));}
      if ( nb_mut = 0 ) then exit;
      pheno0 := lis2[y].atom;
      {with variable[concern_var] do
      if ( typvargroup = typvargroup_rel ) then}
      with pheno0 do for j := 1 to size do lval[j] := 0.0;
      { voir ! iiiii cas d'un mouvement...}
      { muter sans bouger, bouger sans muter ... }
      with variable[xvar],lis2[y].atom do
        ldat[imut] := round(t__ + eval(occur_exp,occur_exp_type));
      with variable[concern_var] do
        begin
          typtyp := typvargroup;
          case typtyp of
            typvargroup_rel  : begin
                                 k := irel;
                                 pheno0.lval[k] := 1.0;
                               end;
            typvargroup_reli : begin
                                 k := ireli;
                                 pheno0.lvali[k] := 1.0;
                               end;
            typvargroup_loc  : begin
                                 k := iloc;
                                 pheno0.lloc[k] := 1.0;
                             end;
            else iwriteln('eval_mut1?');
          end;
        end;
      n1 := 0;
      n2 := 0;
      for n := 1 to nb_mut do
        begin
          var_focal := xvar;
          if ( lz <> 0 ) then
            begin
              lz1 := lz;
              while ( lz1 <> 0 ) do with lis[lz1] do
                begin
                  with mut[car] do with variable[xvar] do
                    pheno0.lmut[imut] := eval(distrib_exp,distrib_exp_type);
                  lz1 := cdr;
                end;
            end
          else
            begin
              with variable[xvar] do
                pheno0.lmut[imut] := eval(distrib_exp,distrib_exp_type);
            end;
          if ( replace <> 0 ) then
            u := pheno_exist(x,z,lz)
          else
            u := 0;
          if ( u <> 0 ) then
            begin
              n2 := n2 + 1;
              with lis2[u].atom do
                begin
                  nb := nb + 1;
                  case typtyp of
                    typvargroup_rel  : lval[k]  := lval[k]  + 1.0;
                    typvargroup_reli : lvali[k] := lvali[k] + 1.0;
                    typvargroup_loc  : lloc[k]  := lloc[k]  + 1.0;
                    else;
                  end;
                end;
            end
          else
            begin
              with pheno0 do
                begin
                  xgroup := x;
                  nb  := 1;
                  anc := lis2[y].atom.num;
                end;
              cre_atom(m_deb,m_fin); { mutants mis sur liste temporaire }
              n1 := n1 + 1;
            end;
        end;
      longlis2 := longlis2 + n1;
      cre := cre + n1;
      with lis2[y].atom do
        case typtyp of
          typvargroup_rel  : begin
                               lval[k] := max(lval[k] - n1 - n2,0);
                               nb := imax(nb - n1 - n2,0);
                             end;
          typvargroup_reli : begin
                               lvali[k] := max(lvali[k] - n1 - n2,0);
                               {nb non mis a jour!}{iiiii}
                             end;
          typvargroup_loc  : begin
                               lloc[k] := max(lloc[k] - n1 - n2,0);
                               {nb non mis a jour!}{iiiii}
                             end;
          else;
        end;
    end;
end;

begin
  {iwriteln(s_ecri(lm,type_lis));}
  while ( lm <> 0 ) do with lis[lm] do
    begin
      if ( car_type = type_lis ) then 
        begin
          lz := car;
          z := lis[lz].car;
        end
      else
        begin
          z  := car;
          lz := 0;
        end;
      with mut[z] do
        begin
          x := variable[xvar].xgroup;
          with group[x] do
            begin
              y := x_deb;
              while ( y <> 0 ) do with lis2[y] do
                begin
                  pheno_focal := y;
                  pheno_env   := y;
                  run_mut1(x,y,z,lz);
                  y := suiv;
                end;
            end;
        end;
      lm := cdr;
    end;
  pheno_env   := 0;
  pheno_focal := 0;
  var_focal   := 0;
  { mise à jour des listes de phenotypes dans les groupes }
  for x := 1 to group_nb do with group[x] do
    if ( m_deb <> 0 ) then
      begin
        {s_ecri_lis2(m_deb);}
        concat_lis2(m_deb,m_fin,x_deb,x_fin);
        x_deb := m_deb;
        cre_lis2(m_deb,m_fin);
      end;
end;

procedure maj_variable;
var e,y,u : integer;
begin
  t__ := t__ + 1;
  with variable[xtime] do val := val + 1.0;
  for e := 1 to ordre_eval_nb do
    begin
      u := ordre_eval[e];
      with variable[u] do
        begin
          var_focal := u;
          if ( xgroup = 0 ) then
            val := eval(exp,exp_type)
          else
            with group[xgroup] do
              begin
                y := x_deb;
                while ( y <> 0 ) do with lis2[y] do
                  begin
                    pheno_focal := y;
                    pheno_env   := y;
                    with atom do lloc[iloc] := eval(exp,exp_type);
                    y := suiv;
                  end;
                pheno_env   := 0;
                pheno_focal := 0;
              end;
          if ( mut_declench <> 0 ) then run_mut(mut_declench);
        end;
      var_focal := 0;
    end;
end;

procedure run_t;
begin

{ 1.a -  evaluation des relations dans les groupes,
         d'abord les relations d'etat, en parallele,
         puis les relations independantes, en parallele }

  run_group;

{ 1.b -  evaluation les relations dans les modeles,
         d'abord les relations d'etat, en parallele,
         puis les relations independantes, en parallele }

  run_modele;

{ 2.a -  mise a jour les variables des relations liees aux groupes,
         plus destruction des phenotypes eteints }

  maj_group;
  destruc_pheno;

{ 2.b -  mise a jour les variables des relations liees aux modeles }

  maj_modele;

{ 3 -    mise a jour des autres variables,
         en premier t := t + 1
         plus declenchement des mutations }

  maj_variable;

end;

end.
