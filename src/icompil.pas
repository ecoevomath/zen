unit icompil;

{$MODE Delphi}


{  @@@@@@   compilation   @@@@@@  }

interface

uses   iglobvar,
       isymb,
       isyntax,
       Classes;

var    compiled : boolean;
       lines_compil : TStrings;

procedure compilation;
procedure init_compilation;

implementation

uses SysUtils,iutil;

var  xobj,size,truc,truc_fin : integer;
     modele_compil : integer;
     group_compil,mut_compil : integer;
     etat : integer;
     compil_init : boolean;
     nb_defvar,nb_deffun,nb_defrel,nb_defmut,nb_defgroup : integer;
     nb_defmut_group,nb_defrel_group,nb_mut : integer;
     xobj_mut : array[1..group_varmut_nb_max] of integer;{iiiii}
     line_num : integer;

     err_compil : boolean;

procedure init_compilation;
begin
  lines_compil := TStringList.Create;
end;

{procedure fin_compilation;
begin
  lines_compil.Free;
end; }

procedure erreur_compil(s,st : string);
begin
  s := 'Compilation error: '''+ s + ''' '#13#10 + st + #13#10 +
       '-> line number ' + IntToStr(line_num);
  erreur_(s);
  err_compil := true;
end;

procedure compil_variable(s : string);
{ defvar xxx = exp | defvar x1 x2 ... xn = exp }
var pos,nom,x,tx,i : integer;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        pos := position(s,'=');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''='' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        s2 := propre(tronque(s2));
        lirexp(s2,x,tx);
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        separe(s1,' ');
        for i := 0 to lines_separe.Count - 1 do
          begin
            s1  := tronque(lines_separe[i]);
            nom := trouve_dic(s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s1,'unproper name');
                    exit;
                  end;
                if est_reserve(s1) then
                  begin
                    erreur_compil(s1,'reserved word');
                    exit;
                  end;
                nom  := cre_dic(s1);
                xobj := cre_variable(nom);
              end
            else
              begin
                xobj := trouve_variable(nom);
                if ( xobj = 0  ) then
                  begin
                    erreur_compil(s,'name already exists');
                    exit;
                  end;
                if ( xobj = xtime ) then
                  begin
                    erreur_compil(s,'t = time is predefined');
                    exit;
                  end;
                if ( variable[xobj].exp_type <> type_inconnu ) then
                  begin
                    erreur_compil(s,'variable already declared');
                    exit;
                  end;
                if ( variable[xobj].xrel <> 0 ) and ( tx <> type_ree ) then
                  begin
                    erreur_compil(s2,'relation-variable must be set to a real value');
                    exit;
                  end;
              end;
            set_variable(xobj,x,tx);
            variable[xobj].xgroup := group_compil;
          end;
        nb_defvar := nb_defvar + lines_separe.Count;
        etat := 10;
      end;
  else;
  end;
end;

procedure compil_fun(s : string);
{ deffun fff(x1, ...,xn) = exp }
var pos,ls,nom,x,tx,nb_arg1,i : integer;
    s1,s2 : string;
    xarg1 : larg_type;
begin
  case etat of
  1 : begin
        pos := position(s,'=');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''='' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        ls := length(s1);
        if ( s1[ls] <> ')' ) then
          begin
            erreur_compil(s,''')'' expected');
            exit;
          end;
        pos := position(s1,'(');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''('' expected');
            exit;
          end;
        s := s1;
        coupe(s,pos,s1,s);
        s1 := tronque(s1);
        if est_reserve(s1) then
          begin
            erreur_compil(s1,'reserved word');
            exit;
          end;
        ls := length(s);
        s  := sous_chaine(s,1,ls-1);
        if ( s = '' ) then
          begin
            erreur_compil(s,'function argument(s) missing');
            exit;
          end;
        nom := trouve_dic(s1);
        if ( nom = 0 ) then 
          begin
            if not est_nom(s1) then
              begin
                erreur_compil(s1,'unproper name');
                exit;
              end;
            nom  := cre_dic(s1);
            xobj := cre_fun(nom);
          end
        else
          begin
            xobj := trouve_fun(nom);
            if ( xobj = 0 ) then
              begin
                erreur_compil(s,'name already exists');
                exit;
              end;
          end;
        separe(s,',');
        for i := 0 to lines_separe.Count - 1 do
          begin
            s1 := tronque(lines_separe[i]);
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            nom := trouve_dic('_'+s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s1,'unproper name');
                    exit;
                  end;
                nom  := cre_dic('_'+s1);
              end;
            xarg1[i+1] := cre_arg(nom);
          end;
        nb_arg1 := lines_separe.Count;
        set_fun(xobj,nb_arg1,xarg1);
        fun_compil := xobj;
        s2 := propre(s2);
        lirexp(s2,x,tx); 
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        nb_deffun := nb_deffun + 1;
        fun[xobj].exp := x;
        fun[xobj].exp_type := tx;
        fun_compil := 0; 
        etat := 10;
      end;
   else;
   end;
end;

procedure compil_rel(s : string);
{ defrel rrr }
{ var = exp  }
var pos,nom,var1,exp1,exp_type1,i : integer;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        nom := trouve_dic(s);
        if ( nom = 0 ) then 
          begin
            if not est_nom(s) then
              begin
                erreur_compil(s,'unproper name');
                exit;
              end;
            nom  := cre_dic(s);
            xobj := cre_rel(nom);
            rel[xobj].indep := 1;
          end
        else
          begin
            xobj := trouve_rel(nom);
            if ( xobj = 0 ) then
              begin
                erreur_compil(s,'name already exists');
                exit;
              end
            else
              if ( rel[xobj].xmodele = 0 ) and ( rel[xobj].xgroup = 0 ) then { cas relation non liee a un modele }
                begin
                  erreur_compil(s,'name already exists');
                  exit;
                end;
          end;
        etat := 2;
      end;
  2 : begin
        pos := position(s,'=');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''='' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1  := tronque(s1);
        s2  := propre(s2);
        nom := trouve_dic(s1);
        if ( nom = 0 ) then 
          begin
            if not est_nom(s1) then
              begin
                erreur_compil(s1,'unproper name');
                exit;
              end;
            nom  := cre_dic(s1);
            var1 := cre_variable(nom);
          end
        else
          begin
            var1 := trouve_variable(nom);
            with variable[var1] do
              if ( xmut <> 0 ) then
                begin
                  erreur_compil(s1,'mutation can not be a relation-variable');
                  exit;
                end;
          end;
        lirexp(s2,exp1,exp_type1);
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( exp1 = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        set_rel(xobj,exp1,exp_type1,var1);
        variable[var1].xrel := xobj;
        if ( variable[var1].xgroup <> 0 ) and ( group_compil = 0 ) then
          begin
            erreur_compil(s1,'group-variable outside of group scope');
            exit;
          end;
        variable[var1].xgroup := group_compil; { utile ? iiiii }
        rel[xobj].xgroup := group_compil;
        if ( group_compil <> 0 ) then with group[group_compil] do
          begin
            if ( rel[xobj].indep = 0 ) then
              begin
                for i := 1 to size do if ( xrel[i] = xobj ) then
                  begin
                    variable[var1].irel := i;
                    variable[var1].typvargroup := typvargroup_rel;
                  end;
              end
            else
              begin
                sizei := sizei + 1;
                if ( sizei > group_rel_nb_max ) then
                  begin
                    erreur_compil(s1,'too many relations in group');
                    exit;
                  end;
                xreli[sizei] := xobj;
                variable[var1].ireli := sizei;
                variable[var1].typvargroup := typvargroup_reli;
              end;
            nb_defrel_group := nb_defrel_group + 1;
          end
        else { relation independante d'un modele }
          with modele[modele_compil] do
            if ( rel[xobj].indep = 1 ) then
              begin
                sizei := sizei + 1;
                if ( sizei > mod_rel_nb_max ) then
                  begin
                    erreur_compil(s1,'too many relations in model');
                    exit;
                  end;
                xreli[sizei] := xobj;
              end;
        nb_defrel := nb_defrel + 1;
        etat := 10;
      end;
   else;
   end;
end;

procedure compil_modele(s : string);
{ defmod mmm(size)  }
{ rel : r1, ..., rsize }
var pos1,pos2,nom,i,x : integer;
    relat : mod_rel_type;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        pos1 := position(s,'(');
        pos2 := position(s,')');
        if ( ( pos2 <> length(s) ) or ( pos2 <= pos1 ) ) then
          begin
            erreur_compil(s,'syntax');
            exit;
          end;
        s := sous_chaine(s,1,pos2-1);
        coupe(s,pos1,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if not est_entier(s2,size) then
          begin
            erreur_compil(s2,'integer expected');
            exit;
          end;
        if ( size > mod_rel_nb_max ) then
          begin
            erreur_compil(s2,'size too big');
            exit;
          end;
        nom := trouve_dic(s1);
        if ( nom = 0 ) then 
          nom := cre_dic(s1)
        else
          begin
            erreur_compil(s,'name already exists');
            exit;
          end;
        xobj := cre_modele(nom);
        modele[xobj].size := size;
        modele_compil := xobj;
        etat := 2;
      end;
  2 : begin
        pos1 := position(s,':');
        if ( pos1 = 0 ) then
          begin
            erreur_compil(s,'''rel:'' expected');
            exit;
          end;
        coupe(s,pos1,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if ( s1 <> 'rel' ) then
          begin
            erreur_compil(s,'keyword ''rel'' expected');
            exit;
          end;
        separe(s2,',');
        if ( lines_separe.Count <> size ) then
          begin
            erreur_compil(s,'mismatch number of relations/model size');
            exit;
          end;
        for i := 0 to lines_separe.Count - 1 do
          begin
            s1 := tronque(lines_separe[i]);
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            nom := trouve_dic(s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s,'unproper name');
                    exit;
                  end;
                nom := cre_dic(s1);
                relat[i+1] := cre_rel(nom);
              end
            else
              begin
                erreur_compil(s,'relation declared before related model');
                exit;
              end;
          end;
        for i := 1 to size do with modele[xobj] do
          begin
            x := relat[i];
            xrel[i] := x;
            rel[x].xmodele := xobj;
            rel[x].indep   := 0;
            rel[x].xgroup  := 0;
          end;
        etat := 10;
        exit; 
     end;
   else;
   end;
end;

procedure compil_group(s : string); 
{ defgroup ggg(size)                  }
{ rel : r1, ..., rsize                }
{ mut : m1, ..., mk                   } 
{   declarations relations            }
{   declarations variables d'etat     }
{   declarations variables locales    }
{   declarations variables mutations  }
{ endgroup                            }
var pos1,pos2,nom,i,x,y : integer;
    relat : group_rel_type; 
    mut1  : group_varmut_type;
    s1,s2 : string;
begin 
  case etat of 
  1 : begin 
        pos1 := position(s,'('); 
        pos2 := position(s,')'); 
        if ( ( pos2 <> length(s) ) or ( pos2 <= pos1 ) ) then
          begin
            erreur_compil(s,'syntax');
            exit;
          end;
        s := sous_chaine(s,1,pos2-1); 
        coupe(s,pos1,s1,s2); 
        s1 := tronque(s1); 
        s2 := tronque(s2);
        if not est_entier(s2,size) then
          begin
            erreur_compil(s2,'integer expected');
            exit;
          end;
        if ( size > group_rel_nb_max ) then
          begin
            erreur_compil(s2,'size too big');
            exit;
          end;
        nom := trouve_dic(s1); 
        if ( nom = 0 ) then
          begin 
            nom := cre_dic(s1);
            xobj := cre_group(nom); 
          end
        else
          begin 
            xobj := trouve_group(nom);
            if ( xobj = 0 ) then
              begin
                erreur_compil(s,'name already exists');
                exit;
              end;
          end;
        group[xobj].size := size;
        group_compil := xobj;
        etat := 2; 
      end; 
  2 : begin 
        pos1 := position(s,':');
        if ( pos1 = 0 ) then
          begin
            erreur_compil(s,'''rel:'' expected');
            exit;
          end;
        coupe(s,pos1,s1,s2); 
        s1 := tronque(s1); 
        s2 := tronque(s2); 
        if ( s1 <> 'rel' ) then
          begin
            erreur_compil(s,'keyword ''rel'' expected');
            exit;
          end;
        separe(s2,',');
        if ( lines_separe.Count <> size ) then
          begin
            erreur_compil(s,'mismatch number of relations/group size');
            exit;
          end;
        for i := 0 to lines_separe.Count - 1 do
          begin
            s1 := tronque(lines_separe[i]);
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            nom := trouve_dic(s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s,'unproper name');
                    exit;
                  end;
                nom := cre_dic(s1);
                relat[i+1] := cre_rel(nom);
              end
            else
              begin
                erreur_compil(s,'relation declared before related model');
                exit;
              end;
          end;
        for i := 1 to size do with group[group_compil] do
          begin
            x := relat[i];
            xrel[i] := x; 
            rel[x].xgroup  := group_compil;
            rel[x].xmodele := 0;
            rel[x].indep   := 0;
          end; 
        etat := 3; 
        exit;  
      end; 
  3 : begin
        pos1 := position(s,':');
        if ( pos1 = 0 ) then
          begin
            erreur_compil(s,'''mut:'' expected');
            exit;
          end;
        coupe(s,pos1,s1,s2); 
        s1 := tronque(s1); 
        s2 := tronque(s2); 
        if ( s1 <> 'mut' ) then
          begin
            erreur_compil(s,'keyword ''mut'' expected');
            exit;
          end;
        separe(s2,',');
        if ( lines_separe.Count > group_varmut_nb_max ) then
          begin
            erreur_compil(s,'too many mutations');
            exit;
          end;
        for i := 0 to lines_separe.Count - 1 do
          begin
            s1 := tronque(lines_separe[i]);
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            nom := trouve_dic(s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s1,'unproper name');
                    exit;
                  end;
                nom := cre_dic(s1);
                x := cre_variable(nom);
                mut1[i+1] := cre_mut(x);
                mut[mut1[i+1]].xvar := x;
              end
            else
              begin
                x := trouve_variable(nom);
                { ne doit pas avoir ete declaree par defvar }
                if ( variable[x].exp_type <> type_inconnu ) then
                  begin
                    erreur_compil(s,'mutation already declared as variable');
                    exit;
                  end;
                mut1[i+1] := cre_mut(x);
                mut[mut1[i+1]].xvar := x;
              end;
          end;
        with group[group_compil] do
          begin
            sizemut := lines_separe.Count;
            for i := 1 to sizemut do 
              begin 
                x := mut1[i];
                y := mut[x].xvar;
                xvarmut[i] := x; 
                variable[y].xgroup := group_compil;
                variable[y].xmut := x;
                variable[y].imut := i;
                variable[y].typvargroup := typvargroup_mut;
              end;
          end;
        etat := 10;
      end;
  else; 
  end; 
end; 

procedure compil_group_fin(s : string);
{ endgroup }
var x,group_iloc,nb_rel_indep,nb_rel_group : integer;
begin
  if ( group_compil = 0 ) then
    begin
      erreur_compil(s,'group ?');
      exit;
    end;
  with group[group_compil] do
    begin
      nb_rel_indep := sizei;
      nb_rel_group := nb_defrel_group - nb_rel_indep;
      if ( nb_rel_group > size ) then
        begin
          erreur_compil(s_ecri_group(group_compil),
          'number of group-relations > group-size');
          exit;
        end;
      if ( nb_rel_group < size ) then
        begin
          erreur_compil(s_ecri_group(group_compil),
          'number of group-relations < group-size');
          exit;
        end;
      if ( nb_defmut_group > sizemut ) then
        begin
          erreur_compil(s_ecri_group(group_compil),
          'more mutations declared than expected');
          exit;
        end;
      if ( nb_defmut_group < sizemut ) then
        begin
          erreur_compil(s_ecri_group(group_compil),
          'less mutations declared than expected');
          exit;
        end;
  { determination des variables locales au groupe : }
      group_iloc := 0;
      for x := 1 to variable_nb do with variable[x] do
        if ( xgroup = group_compil ) then
          if ( irel = 0 ) and ( ireli = 0 ) and ( imut = 0 ) then
            begin
              group_iloc := group_iloc + 1;
              if ( group_iloc > group_varloc_nb_max ) then
                begin
                  erreur_compil(s_ecri_group(group_compil),
                  'too many local variables in group');
                  exit;
                end;
              iloc := group_iloc;
              xvarloc[group_iloc] := x;
              typvargroup := typvargroup_loc;
            end;
      sizeloc := group_iloc;
    end;
  nb_defgroup := nb_defgroup + 1;
  group_compil := 0;
  nb_defrel_group := 0;
  nb_defmut_group := 0;
end;

procedure compil_mut(s : string); 
{ defmut m1 m2 ... mk = val01 val02 ... val0k          }
{ occur   : occur_exp                                  }
{ trigger : declench_var                               }
{ number  : nbmut_exp                                  }
{ distrib : distrib_exp1; distrib_exp2; ... distrib_expk }
{ replace : 0/1
{ concern : concern_var                                }
var pos,nom,x,tx,y,i,lz : integer;
    v : extended;
    s1,s1a,s2,s2a : string;
begin 
  case etat of 
  1 : begin 
        if ( group_compil = 0 ) then
          begin
            erreur_compil(s,'mutation must be declared within a group');
            exit;
          end;
        pos := position(s,'='); 
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''='' expected');
            exit;
          end;
        coupe(s,pos,s1,s2); 
        s1 := tronque(s1); 
        s2 := tronque(s2);
        separe(s1,' ');
        if ( lines_separe.Count < 1 ) then
          begin
            erreur_compil(s1,'syntax');
            exit;
          end;
        nb_mut := lines_separe.Count;
        for i := 1 to nb_mut do
          begin
            s1a := tronque(lines_separe[i-1]);
            nom := trouve_dic(s1a);
            if ( nom = 0 ) then
              begin
                erreur_compil(s1,'unknown mutation name');
                exit;
              end;
            xobj_mut[i] := trouve_mut(nom);
            if ( xobj_mut[i] = 0 ) then
              begin
                erreur_compil(s1,'unknown mutation');
                exit;
              end;
            mut_compil := xobj_mut[i];
          end;
        separe(s2,' ');
        if ( lines_separe.Count <> nb_mut ) then
          begin
            erreur_compil(s2,'mismatch number of murations/number of values');
            exit;
          end;
        for i := 1 to nb_mut do
          begin
            s2a := tronque(lines_separe[i-1]);
            lirexp(s2a,x,tx);
            if err_syntax then
              begin
                err_syntax := false;
                erreur_compil(s2,'');
                exit;
              end;
            if ( x = 0 ) then
              begin
                erreur_compil(s2,'');
                exit;
              end;
            if ( tx <> type_ree ) then
              begin
                erreur_compil(s2,'real value expected');
                exit;
              end;
            y := mut[xobj_mut[i]].xvar;
            with variable[y] do
              begin
                exp := x;
                exp_type := tx;
              end;
          end;
        etat := 2;
      end;
  2 : begin
        if ( group_compil = 0 ) then erreur_compil(s,'group ?');
        if ( mut_compil   = 0 ) then erreur_compil(s,'mutation ?');
        pos := position(s,':'); 
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''trigger:'' expected');
            exit;
          end;
        coupe(s,pos,s1,s2); 
        s1 := tronque(s1); 
        s2 := tronque(s2); 
        if ( s1 <> 'trigger' ) then
          begin
            erreur_compil(s,'keyword ''trigger'' expected');
            exit;
          end;
        lirexp(s2,x,tx); 
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        if ( tx <> type_variable ) then
          begin
            erreur_compil(s2,'variable name expected');
            exit;
          end;
        if ( nb_mut = 1 ) then with mut[xobj_mut[1]] do
          begin
            with variable[x] do
              mut_declench := cons(xobj_mut[1],type_mut,mut_declench); 
            declench_var := x;
          end
        else
          begin
            lz := 0;
            for i := 1 to nb_mut do with mut[xobj_mut[i]] do
              begin
                declench_var := x;
                lz := cons(xobj_mut[i],type_mut,lz);
              end; 
            with variable[x] do
              mut_declench := cons(lz,type_lis,mut_declench);
            { remarque: ordre inverse jjjjj }
          end;
        etat := 3; 
      end; 
  3 : begin 
        if ( group_compil = 0 ) then erreur_compil(s,'group ?');
        if ( mut_compil   = 0 ) then erreur_compil(s,'mutation ?');
        pos := position(s,':'); 
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''occur:'' expected');
            exit;
          end;
        coupe(s,pos,s1,s2); 
        s1 := tronque(s1); 
        s2 := propre(s2);
        if ( s1 <> 'occur' ) then
          begin
            erreur_compil(s,'keyword ''occur'' expected');
            exit;
          end;
        lirexp(s2,x,tx); 
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        for i := 1 to nb_mut do with mut[xobj_mut[i]] do
          begin
            occur_exp := x;
            occur_exp_type := tx;
          end;
        etat := 4; 
      end; 
 4  : begin 
        if ( group_compil = 0 ) then erreur_compil(s,'group ?');
        if ( mut_compil   = 0 ) then erreur_compil(s,'mutation ?');
        pos := position(s,':'); 
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''number:'' expected');
            exit;
          end;
        coupe(s,pos,s1,s2); 
        s1 := tronque(s1); 
        s2 := propre(s2); 
        if ( s1 <> 'number' ) then
          begin
            erreur_compil(s,'keyword ''number'' expected');
            exit;
          end;
        lirexp(s2,x,tx); 
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        for i := 1 to nb_mut do with mut[xobj_mut[i]] do
          begin
            nbmut_exp := x;
            nbmut_exp_type := tx;
          end;
        etat := 5; 
      end;
  5 : begin 
        if ( group_compil = 0 ) then erreur_compil(s,'group ?');
        if ( mut_compil   = 0 ) then erreur_compil(s,'mutation ?');
        pos := position(s,':'); 
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''distrib:'' expected');
            exit;
          end;
        coupe(s,pos,s1,s2); 
        s1 := tronque(s1); 
        s2 := tronque(s2);
        if ( s1 <> 'distrib' ) then
          begin
            erreur_compil(s,'keyword ''distrib'' expected');
            exit;
          end;
        separe(s2,';'); { separateur ';' } {iiiii}
        if ( lines_separe.Count <> nb_mut ) then
          begin
            erreur_compil(s2,
            'mismatch number of mutations/number of distributions');
            exit;
          end;
        for i := 1 to nb_mut do
          begin
            s2a := propre(lines_separe[i-1]);
            lirexp(s2a,x,tx);
            if err_syntax then
              begin
                err_syntax := false;
                erreur_compil(s2,'');
                exit;
              end;
            if ( x = 0 ) then
              begin
                erreur_compil(s2,'');
                exit;
              end;
            with mut[xobj_mut[i]] do
              begin
                distrib_exp := x;
                distrib_exp_type := tx;
              end;
          end;
        etat := 6; 
      end;
  6 : begin
        if ( group_compil = 0 ) then erreur_compil(s,'group ?');
        if ( mut_compil   = 0 ) then erreur_compil(s,'mutation ?');
        pos := position(s,':');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''replace:'' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if ( s1 <> 'replace' ) then
          begin
            erreur_compil(s,'keyword ''replace'' expected');
            exit;
          end;
        lirexp(s2,x,tx);
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        if ( tx <> type_ree ) then
          begin
            erreur_compil(s2,'0 or 1 expected');
            exit;
          end;
        v := ree[x].val;
        if ( v <> 0.0 ) and ( v <> 1.0 ) then
          begin
            erreur_compil(s2,'0 or 1 expected');
            exit;
          end;
        for i := 1 to nb_mut do with mut[xobj_mut[i]] do replace := trunc(v);
        etat := 7;
      end;
  7 : begin
        if ( group_compil = 0 ) then erreur_compil(s,'group ?');
        if ( mut_compil   = 0 ) then erreur_compil(s,'mutation ?');
        pos := position(s,':');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''concern:'' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if ( s1 <> 'concern' ) then
          begin
            erreur_compil(s,'keyword ''concern'' expected');
            exit;
          end;
        lirexp(s2,x,tx);
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        if ( tx <> type_variable ) then
          begin
            erreur_compil(s2,'variable name expected');
            exit;
          end;
        with variable[x] do
          if ( xmut <> 0 ) then
            begin
            erreur_compil(s,'mutation can not be used here');
            exit;
            end;
        for i := 1 to nb_mut do with mut[xobj_mut[i]] do concern_var := x;
        nb_defmut_group := nb_defmut_group + nb_mut; 
        nb_defmut := nb_defmut + nb_mut;
        mut_compil := 0;
        etat := 10;
      end;
  else;
  end;
end;

function  token(s : string) : integer;
begin
  if ( s = 'defvar' ) then
    token := type_variable
  else
  if ( s = 'deffun' ) then
    token := type_fun
  else
  if ( s = 'defrel' ) then
    token := type_rel
  else
  if ( s = 'defmod'  ) then 
    token := type_modele 
  else 
  if ( s = 'defgroup'  ) then 
    token := type_group 
  else 
  if ( s = 'defmut'  ) then 
    token := type_mut 
  else 
    token := type_inconnu;
end;

function  token_fin(s : string) : integer; 
begin 
  if ( s = 'endgroup' ) then 
    token_fin := type_group 
  else 
    token_fin := type_inconnu; 
end; 

procedure compile(s : string);
var pos,ls : integer;
    s1,s2  : string;
begin
  s := tronque(minuscule(s));
  ls := length(s);
  if ( ls > 0 ) and ( s[1] = '{' ) then exit;
  if ( etat = 0 ) then
    if ( ls = 0 ) then exit else etat := 1;
  case etat of
  1 : begin
        truc_fin := token_fin(s); 
        case truc_fin of 
          type_group    : begin
                            compil_group_fin(s);
                            etat := 0;
                            exit; 
                          end;
          else;
        end;
        pos := position(s,' ');
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if compil_init and ( s1 <> 'defmod' ) then
          begin
            erreur_compil(s,'model must be declared first');
            exit;
          end
        else
          compil_init := false;
       truc := token(s1);
       case truc of
          type_variable : compil_variable(s2);
          type_fun      : compil_fun(s2);
          type_rel      : compil_rel(s2);
          type_modele   : compil_modele(s2);
          type_group    : compil_group(s2);
          type_mut      : compil_mut(s2);
          else
            begin
              erreur_compil(s,'unknown keyword');
              exit;
            end;
        end; 
      end;
  2 : begin
        case truc of
          type_variable : compil_variable(s);
          type_fun      : compil_fun(s);
          type_rel      : compil_rel(s);
          type_modele   : compil_modele(s);
          type_group    : compil_group(s);
          type_mut      : compil_mut(s);
          else erreur_compil(s,'lost 2!');
        end
      end;
  3 : begin 
        case truc of 
          type_group    : compil_group(s);
          type_mut      : compil_mut(s);
          else erreur_compil(s,'lost 3!'); 
        end 
      end; 
  4 : begin 
        case truc of 
          type_mut      : compil_mut(s);
          else erreur_compil(s,'lost 4!'); 
        end 
      end; 
  5 : begin 
        case truc of 
          type_mut      : compil_mut(s);
          else erreur_compil(s,'lost 5!'); 
        end 
      end;
  6 : begin
        case truc of
          type_mut      : compil_mut(s);
          else erreur_compil(s,'lost 6!');
        end
      end;
  7 : begin
        case truc of
          type_mut      : compil_mut(s);
          else erreur_compil(s,'lost 7!');
        end
      end;
  10 : begin
        if ( ls <> 0 ) then
          begin
            erreur_compil(s,'blank line expected');
            exit;
          end;
        etat := 0;
      end;
  else;
  end;
end;

procedure coherence_compil; 
var x,i,irel : integer;
begin
  if ( modele_nb = 0 ) then
    begin
      erreur_compil('','no model declared');
      exit;
    end;
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_inconnu ) then
      begin
        erreur_compil(s_ecri_var(x),'variable not declared');
        exit;
      end;
  for x := 1 to variable_nb do with variable[x] do 
    if ( exp_type = type_ree) and ( mut_declench <> 0 ) then 
      begin
        erreur_compil(s_ecri_var(x),
        'variable with real value cannot trigger mutation');
        exit;
      end;
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type <> type_ree) and ( xrel <> 0 ) then
      begin
        erreur_compil(s_ecri_var(x),
        'relation-variable must be set to a real value');
        exit;
      end;
  for x := 1 to fun_nb do with fun[x] do
    if ( exp_type = type_inconnu ) then
      begin
        erreur_compil(s_ecri_fun(x),'function not declared');
        exit;
      end;
  if ( nb_defgroup < group_nb ) then
    begin
      erreur_compil('','group not declared');
      exit;
    end;

  irel := 0;
  for x := 1 to modele_nb do with modele[x] do irel := irel + size;
  if ( irel > nb_defrel )  then
    begin
      erreur_compil('','missing declaration of model relation');
      exit;
    end;
  for x := 1 to modele_nb do with modele[x] do
    for i := 1 to size do
      if ( rel[xrel[i]].exp_type = type_inconnu ) then
        begin
          erreur_compil(s_ecri_rel(xrel[i]),
          'undefined relation in model ' + s_ecri_model(x));
          exit;
        end;
  if ( nb_defvar <> variable_nb - variable_nb_predef - nb_defmut ) then
    begin
      erreur_compil('','missing declaration of variable');
      exit;
    end;
  if ( nb_deffun <> fun_nb - fun_nb_predef ) then
    begin
      erreur_compil('','missing declaration of function');
      exit;
    end;
  crea_var   := false;
  crea_fun   := false;
  crea_group := false;

end;

procedure init_compil;
begin
  compil_init  := true;
  etat         := 0; 
  nb_defvar    := 0;
  nb_deffun    := 0;
  nb_defrel    := 0;
  nb_defgroup  := 0;
  nb_defrel_group := 0; 
  nb_defmut    := 0;
  nb_defmut_group := 0;
  modele_compil := 0;
  fun_compil   := 0; 
  crea_var     := true;
  crea_fun     := true;
  crea_group   := true;
  group_compil := 0;
  mut_compil   := 0;
end;

procedure compilation;
var i : integer;
begin
  init_compil;
  line_num := 0;
  err_compil := false;
  compiled := false;
  for i := 0 to lines_compil.Count - 1 do
    begin
      line_num := line_num + 1;
      {iwriteln(lines_compil[i]);}
      compile(lines_compil[i]); {voir symbole ! de continuation}
      if err_compil then exit;
    end;
  coherence_compil;
  if err_compil then exit;
  compiled := true;
end;

end.
