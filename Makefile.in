# A simple makefile for ZEN.
# This file is part of the ZEN program,
# (c) 2017 The Zen Development team / License GPLv3+
# This is just aliases to build ULM without firing the whole lazarus IDE.

INSTALL = install

.PHONY: all
all: zen zenc

zen: ./src/zen.lpr
	$(lazbuild) $(widgetset) --bm=$(buildmode) ./src/zen.lpr
	mv src/zen .
zenc: ./src/console/zenc.lpr
	$(lazbuild) src/console/zenc.lpr
	mv src/console/zenc .

.PHONY: clean
clean:
	$(RM) src/*.ppu src/*.o src/zen.compiled zen src/zen.or src/console/*.ppu src/console/*.o zenc

.PHONY: dist
dist: zen zenc
	mkdir -p dist
	# Create the distribuable tar.gz file
	tar -C dist --transform 's,^,zen/,' -zcvf dist/zen.tar.gz ../zen ../zenc ../LICENSE.txt ../CHANGELOG.md ../README.md

.PHONY: distclean
distclean: clean
	$(RM) Makefile ./icons/zen.desktop

.PHONY: install
install: zen zenc
	$(INSTALL) ./zen $(DESTDIR)$(prefix)/bin
	$(INSTALL) ./zenc $(DESTDIR)$(prefix)/bin
	$(INSTALL) -d $(DESTDIR)$(prefix)/share/zen
	$(INSTALL) -d $(DESTDIR)$(prefix)/share/zen/icons
	$(INSTALL) ./icons/zen.svg $(DESTDIR)$(prefix)/share/zen/icons
	$(INSTALL) -d $(DESTDIR)$(prefix)/share/applications/
	$(INSTALL) ./icons/zen.desktop $(DESTDIR)$(prefix)/share/applications/
	$(INSTALL) -d $(DESTDIR)$(prefix)/man
	$(INSTALL) -d $(DESTDIR)$(prefix)/man/man1
	$(INSTALL) ./docs/man/zen.1.gz $(DESTDIR)$(prefix)/man/man1

.PHONY: uninstall
uninstall:
	$(RM) $(prefix)/bin/zen
	$(RM) $(prefix)/bin/zenc
	$(RM) $(prefix)/share/zen/icons/zen.svg
	$(RM) -r $(prefix)/share/zen/icons
	$(RM) -r $(prefix)/share/zen
	$(RM) $(prefix)/share/applications/zen.desktop
	$(RM) $(prefix)/man/man1/zen.1.gz
	$(RM) $(prefix)/man/man1/zenc.1.gz