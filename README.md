# Zen - Eco-evolutionary Software

Ecological models with an evolutionary component allow to study
several biological phenomena: host-pathogens interactions, coevolution
of plants and pollinisators, mimicry, development of the immune
system, evolution of cooperation, evolution of life history traits,
and more generally biodiversity and speciation.

## Compile ZEN from source

You can compile ZEN from source using Free Pascal and the libraries included
with the [Lazarus IDE](http://www.lazarus-ide.org/):

``` bash
# You will need the lazbuild tool included with lazarus (`sudo apt-get install lazarus` should be enough on debian/ubuntu)
cd zen/
./configure # (Optional: Use '--prefix=~/.local' for user install)
make
sudo make install
```

## License

Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team

ZEN is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.

ZEN is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with ZEN.  If not, see <http://www.gnu.org/licenses/>.
